/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author asmaasallam
 */
public class QueryInStateMachine {
    
    String queryStr;
    long maxDiffToSelf;
    long maxDiffToPrev;
        
    int numAllowedRepetitions;
    
    int numCurrentRepetitions;
    
    ArrayList<QueryInStateMachine> nextQueries;
    
    public QueryInStateMachine(String queryStr) {
        this.queryStr = queryStr;
        this.maxDiffToPrev = Long.MIN_VALUE;
        this.maxDiffToSelf = Long.MIN_VALUE;
        nextQueries = new ArrayList<>();
    }
    
    public ArrayList<QueryInStateMachine> getNextQueries() {
        return nextQueries;
    }
    
    public String getQueryStr() {
        return this.queryStr;
    }
    
    public QueryInStateMachine addNextQuery(String queryStr) {
        
        if(this.queryStr.equals(queryStr)) {
            this.numAllowedRepetitions++;
            return this;
        } else {
            int index = nextQueries.indexOf(new QueryInStateMachine(queryStr));
            QueryInStateMachine nextQuery;
            if(index == -1) {
                nextQuery = new QueryInStateMachine(queryStr);
                nextQueries.add(nextQuery);
            } else {
                nextQuery = nextQueries.get(index);
            }
            return nextQuery;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof String) {
            String str = (String)obj;
            return str.equals(this.queryStr);
        } else if(obj instanceof QueryInStateMachine) {
            return this.queryStr.equals(((QueryInStateMachine)obj).getQueryStr());
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.queryStr);
        return hash;
    }

    void addPrevTimeDiff(long timeDiff) {
        if(maxDiffToPrev < timeDiff) {
            maxDiffToPrev = timeDiff;
        }
    }
    
    void addTimeDiffToSelf(long timeDiff) {
        if(maxDiffToSelf < timeDiff) {
            maxDiffToSelf = timeDiff;
        }
    }

    long getMaxtimeDiff() {
        return maxDiffToPrev;
    }

    int getNumRepetitions() {
        return numAllowedRepetitions;
    }
    
    void clearNumCurrRepetitions() {
        numCurrentRepetitions = 0;
    }
    
    void incrementNumCurrRepetitions() {
        numCurrentRepetitions++;
    }

    long getMaxDiffToSelf() {
        return this.maxDiffToSelf;
    }

    boolean canBeRepeated() {
        return this.numAllowedRepetitions > this.numCurrentRepetitions;
    }
    
}