/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;

/**
 *
 * @author asmaasallam
 */
public class StateMachine {

    
    ArrayList<QueryInStateMachine> nextAnticipatedQueries;

    QueryInStateMachine currentNode;
    
    long nextAnticipatedTime;

    long lastBeginQueryTime;
    
    boolean beforeDetection;

    public StateMachine(ArrayList<QueryInStateMachine> nextAnticipatedQueries, long lastBeginQueryTime) {
        
        // TODO uncomment
        //nextAnticipatedTime = lastBeginQueryTime + periodInfo.getPeriodLength() * periodInfo.getResolution();
        this.lastBeginQueryTime = lastBeginQueryTime;
        this.nextAnticipatedQueries = nextAnticipatedQueries;

        currentNode = nextAnticipatedQueries.get(0);
        
        beforeDetection = true;
    }

    public StateMachine(ArrayList<QueryInStateMachine> nextAnticipatedQueries) {
       
        
        this.nextAnticipatedQueries = nextAnticipatedQueries;

        currentNode = nextAnticipatedQueries.get(0);
        
        beforeDetection = true;
    }

    public int checkQuery(String queryStr, long beginTime, long queryTime) {
        
        if(beforeDetection) {
//            if(checkBeginTime(beginTime)) {
//                return true;
//            }
            
            if(checkFirstQueryTime(queryTime)) {
                return 1;
            }
            lastBeginQueryTime = beginTime;            
            beforeDetection = false;
            // TODO uncomment
            //nextAnticipatedTime += (periodInfo.getPeriodLength() * periodInfo.getResolution());
            System.out.println("next: " + nextAnticipatedTime);
            return 0;
        }
        
        if(lastBeginQueryTime != beginTime) {
            int numAnomalies = 0;
            if(currentNode.getNextQueries() != null && !currentNode.getNextQueries().isEmpty()) {
                numAnomalies++;
            }
//            if(checkBeginTime(beginTime)) {
//                return true;
//            }
            if(checkFirstQueryTime(queryTime)) {
                numAnomalies++;
            }
            currentNode = nextAnticipatedQueries.get(0);
            lastBeginQueryTime = beginTime;
            // TODO uncomment
            //nextAnticipatedTime += (periodInfo.getPeriodLength() * periodInfo.getResolution());
            System.out.println("next: " + nextAnticipatedTime);
            return numAnomalies;
        }
        long timeDiff = queryTime - lastBeginQueryTime;
        if(currentNode.getQueryStr().equals(queryStr)) {
            if(!currentNode.canBeRepeated()) {
                return 1;
            } else if(timeDiff <= currentNode.getMaxDiffToSelf()) {
                updateState(beginTime, true, null);
                return 0;
            } else {
                return 1;
            }
        }
        for (QueryInStateMachine qinm : currentNode.getNextQueries()) {
            if (qinm.getQueryStr().equals(queryStr)) {
                if (timeDiff <= qinm.getMaxtimeDiff()) {
                    updateState(beginTime, false, qinm);
                    return 0;
                } else {
                    return 1;
                }
            }
        }
        return 1;
    }

//    public boolean checkBeginTime(long beginTime) {
//        // TODO check next
//        while (beginTime >= nextAnticipatedTime + periodInfo.getIntEnd()) {
//            nextAnticipatedTime += (periodInfo.getPeriodLength() * periodInfo.getResolution());
//        }
//        return !(beginTime >= nextAnticipatedTime - periodInfo.getIntStart() && beginTime <= nextAnticipatedTime + periodInfo.getIntEnd());
//    }
    
    public boolean checkFirstQueryTime(long firstQueryTime) {
        return currentNode.getMaxtimeDiff() < firstQueryTime;
    }
    
    public void updateState(long beginTime, boolean updateCurrNode, QueryInStateMachine nextNode) {
        lastBeginQueryTime = beginTime;
        
        if(updateCurrNode) {
            currentNode.incrementNumCurrRepetitions();
        } else {
            currentNode.clearNumCurrRepetitions();
            currentNode = nextNode;
        }
    }
    
}