/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author asmaasallam
 */
public class Definitions {
    public static final String TRACES_DB_NAME = "DangerTrace";
    public static final String TRAINING_DB_NAME = "DangerTemplate";
    public static final String TRACKING_DB_NAME = "trainingDb";
    public static final String DETECTION_DB_NAME = "detectionDb";
    
    public static final float PORTION_TO_RECONSIDER = 0.1f;
    public static final int NUM_SLOTS = 10;
    public static final int SLOT_LENGTH = 1;
    public static final int[] CAPTURE_FREQUENCIES = {4, 6, 8};
    //public static final float PORTION_TO_RECONSIER = 0.5f;
    
    public static final int ONE_HOUR = 60 * 60 * 1000;
}
