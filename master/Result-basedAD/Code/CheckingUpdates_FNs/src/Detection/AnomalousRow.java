/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Detection;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author asmaasallam
 */
public class AnomalousRow {
    String rowKey;
    Timestamp generatingQueryTime;
    boolean isTrueAnomaly;
    
    ArrayList<Integer> flaggedIntervals;

    AnomalousRow(String attsValsStr, Timestamp queryTime, boolean isTrueAnomaly) {
        this.rowKey = attsValsStr;
        this.generatingQueryTime = queryTime;
        this.isTrueAnomaly = isTrueAnomaly;
        flaggedIntervals = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof String) {
            return rowKey.equals((String)obj);
        } else if(obj instanceof AnomalousRow) {
            AnomalousRow input = (AnomalousRow)obj;
            return rowKey.equals(input.rowKey) 
                    && generatingQueryTime.equals(input.generatingQueryTime);
        }
        return false;
    }
    
    public void addInterval(int l) {
        flaggedIntervals.add(l);
    }

    public int nIntervals() {
        return flaggedIntervals.size();
    }
    
}
