/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Detection;

/**
 *
 * @author asmaasallam
 */
public class TableAnomalies {
    public String tableName;
    public int total;
    public int undetected;
    
    public void addToTotal(int total, int undetected) {
        this.total += total;
        this.undetected += undetected;
    }
}
