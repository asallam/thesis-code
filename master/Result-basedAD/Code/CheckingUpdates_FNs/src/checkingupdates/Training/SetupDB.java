/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkingupdates.Training;

import Detection.A_Detector;
import Utils.DbUtils;
import Utils.Attribute;
import Utils.Definitions;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class SetupDB {

    Connection conn_target = null;
    Connection conn_tracking = null;
    Connection conn_detection = null;
    Connection conn_traces = null;

    ArrayList<String> allTables;
    HashMap<String, ArrayList<Attribute>> tablesPKs;

    private static final String TARGET_DB_NAME = "DangerTemplate";
    private static final String TRACKING_DB_NAME = "trainingDb";
    private static final String DETECTION_DB_NAME = "detectionDb";

    public SetupDB() {
        connectToTarget();
        connectToTracking();
        connectToDetection();
        connectToTraces();
    }

    public void connectToTarget() {
        try {
            conn_target = DbUtils.connectToDb(TARGET_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void connectToTracking() {
        try {
            conn_tracking = DbUtils.connectToDb(TRACKING_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void connectToTraces() {
        try {
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void connectToDetection() {
        try {
            conn_detection = DbUtils.connectToDb(DETECTION_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn_tracking() {
        try {
            DbUtils.closeConn(conn_tracking);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn_target() {
        try {
            DbUtils.closeConn(conn_target);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn_detection() {
        try {
            DbUtils.closeConn(conn_detection);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeConn_traces() {
        try {
            DbUtils.closeConn(conn_traces);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    public void dropDBs() throws ClassNotFoundException, SQLException {
//        DbUtils.dropDb(Definitions.DETECTION_DB_NAME);
//        DbUtils.dropDb(Definitions.TRAINING_DB_NAME);
//    }
//    
//    public void createDBs() throws ClassNotFoundException, SQLException {
//        DbUtils.createDb(Definitions.DETECTION_DB_NAME);
//        DbUtils.createDb(Definitions.TRAINING_DB_NAME);
//    }
    public void createDbTables() throws SQLException {
        allTables = DbUtils.listTables(conn_target, TARGET_DB_NAME);
        tablesPKs = DbUtils.findPKs(conn_target, allTables);
        Set<Map.Entry<String, ArrayList<Attribute>>> entrySet = tablesPKs.entrySet();
        for (Map.Entry<String, ArrayList<Attribute>> entry : entrySet) {
            String tableName = entry.getKey();
            ArrayList<Attribute> atts = entry.getValue();
            Statement stmt;
            String query = "CREATE TABLE " + tableName + "(";
            for (int i = 0; i < atts.size(); i++) {
                Attribute att = atts.get(i);
                if (i != 0) {
                    query = query + ", ";
                }
                query = query + att.getName() + " " + att.getType();
            }
            String query1 = query + ", cnts text, start_time datetime, remanining_shift text, prev_total text)";
            stmt = conn_tracking.createStatement();
            stmt.executeUpdate(query1);
            stmt.close();

            // remanining_shift text, prev_total text
            String query2 = query + ", cnts text, start_time datetime, sums text)";
            stmt = conn_detection.createStatement();
            stmt.executeUpdate(query2);
            stmt.close();

            insertPKsValuesIntoDb(tableName, atts);

        }
    }

    public void insertPKsValuesIntoDb(String tableName, ArrayList<Attribute> atts) {
        try {
            ResultSet rs1;
            Statement stmt1;
            String queryGetValues = "SELECT ";
            for (int i = 0; i < atts.size(); i++) {
                String attName = atts.get(i).getName();
                if (i != 0) {
                    queryGetValues = queryGetValues + ", ";
                }
                queryGetValues = queryGetValues + attName;
            }
            queryGetValues = queryGetValues + " FROM " + tableName;
            stmt1 = conn_target.createStatement();
            rs1 = stmt1.executeQuery(queryGetValues);

            String attsStr = "";
            for (int i = 0; i < atts.size(); i++) {
                if (i != 0) {
                    attsStr = attsStr + ", ";
                }
                attsStr = attsStr + atts.get(i).getName();
            }
            while (rs1.next()) {
                String values = "";
                for (int i = 1; i <= atts.size(); i++) {
                    if (i != 1) {
                        values = values + ", ";
                    }
                    // TODO had to check if the type of the variable and get it accordingly 
                    values = values + rs1.getInt(i);
                }
                //String queryInsert = "INSERT INTO " + tableName + "(" + attsStr + ", cnt" + ")" + " VALUES (" + values + ", 0)";
                String queryInsert1 = "INSERT INTO " + tableName + "(" + attsStr
                        + ", cnts, start_time, remanining_shift, prev_total"
                        + ")" + " VALUES (" + values + ", null, null, '"
                        + Trainer.composeInitailFrequencies() + "', '"
                        + Trainer.composeInitialTotals() + "')";
                Statement stmt2;
                stmt2 = conn_tracking.createStatement();
                stmt2.executeUpdate(queryInsert1);
                stmt2.close();

                String queryInsert2 = "INSERT INTO " + tableName + "(" + attsStr
                        + ", cnts, start_time, sums"
                        + ")" + " VALUES (" + values + ", null, null, '"
                        + A_Detector.composeInitialTotals() + "')";
                stmt2 = conn_detection.createStatement();
                stmt2.executeUpdate(queryInsert2);
                stmt2.close();
            }

            stmt1.close();
            rs1.close();

        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnections() {
        closeConn_target();
        closeConn_tracking();
        closeConn_detection();
        closeConn_traces();
    }
    
    public void resetTables() throws SQLException {
        allTables = DbUtils.listTables(conn_target, TARGET_DB_NAME);
        for(String tableName : allTables) {
            String query1 = "UPDATE " + tableName 
                    + " SET cnts = null, start_time = null, "
                    + "remanining_shift = '" + Trainer.composeInitailFrequencies() 
                    + "', prev_total = '" + Trainer.composeInitialTotals() + "'";
            Statement stmt1 = conn_tracking.createStatement();
            stmt1.executeUpdate(query1);
            stmt1.close();
            
            String query2 = "UPDATE " + tableName 
                    + " SET cnts = null, start_time = null, sums = '"
                        + A_Detector.composeInitialTotals() + "'";
            Statement stmt2 = conn_detection.createStatement();
            stmt2.executeUpdate(query2);
            stmt2.close();
        }
        
        String query3 = "DELETE FROM Profiles";
        Statement stmt3 = conn_traces.createStatement();
        stmt3.executeUpdate(query3);
        stmt3.close();
        
        String query4 = "DELETE FROM counts";
        Statement stmt4 = conn_traces.createStatement();
        stmt4.executeUpdate(query4);
        stmt4.close();
        
    }

    public static void main(String[] args) {
        try {
            SetupDB setupDb = new SetupDB();
           // setupDb.createDbTables();
            
            setupDb.resetTables();
            setupDb.closeConnections();
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
