package checkingupdates.Training;

import Utils.MathUtil;
import Utils.Attribute;
import Utils.DbUtils;
import Utils.Definitions;
import Utils.ParsedQuery;
import Utils.Parser;
import Utils.Query;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class Trainer {

    Connection conn_traces;
    Connection conn_training;
    Connection conn_tracking;

    public Trainer() {
        startConnections();
    }

    public void startConnections() {
        try {
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);
            conn_training = DbUtils.connectToDb(Definitions.TRAINING_DB_NAME);
            conn_tracking = DbUtils.connectToDb(Definitions.TRACKING_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnections() {
        try {
            DbUtils.closeConn(conn_traces);
            DbUtils.closeConn(conn_training);
            DbUtils.closeConn(conn_tracking);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void recordCount(String tableName, int count, Timestamp startTime,
            int intervalLength) throws SQLException {
        if(count > 0) {
            String query = "INSERT INTO counts"
                    + "(table_name, count, start_time, interval_length) VALUES ('"
                    + tableName + "', " + count + ", '" + startTime
                    + "', " + intervalLength + ")";
            DbUtils.executeUpdateOnDb(conn_traces, query);
        }
    }

    private void updateTrackingTable(String table1Name,
            CountsArr ca1, String attsValsStr) throws SQLException {
        String query12 = "UPDATE " + table1Name
                + " SET cnts = '" + ca1.counts + "', "
                + "start_time = '" + ca1.startTime + "' WHERE " + attsValsStr;
        DbUtils.executeUpdateOnDb(conn_tracking, query12);
    }

    public void constructProfiles() {
        ArrayList<String> tablesNames;
        try {
            tablesNames = DbUtils.listTables(conn_tracking, Definitions.TRACKING_DB_NAME);
            for (String tableName : tablesNames) {
                HashMap<Integer, ArrayList> vals = new HashMap<>();

                String query = "SELECT interval_length, count FROM dbo.counts WHERE table_name = '"
                        + tableName + "'";
                Statement stmt;
                stmt = conn_traces.createStatement();
                ResultSet rs = stmt.executeQuery(query);
                

                while (rs.next()) {
                    int intervalLength = rs.getInt(1);
                    int count = rs.getInt(2);

                    if (count != 0) {
                        ArrayList<Integer> countsForInterval = vals.get(intervalLength);
                        if (countsForInterval == null) {
                            countsForInterval = new ArrayList<>();
                            vals.put(intervalLength, countsForInterval);
                        }
                        countsForInterval.add(count);
                    }

                }

                for (Map.Entry<Integer, ArrayList> countsForInterval : vals.entrySet()) {
                    MathUtil mathUtil = new MathUtil();
                    ArrayList<Integer> counts = countsForInterval.getValue();
                    Integer outlier;
                    // TODO uncomment
//                    do {
//                        outlier = mathUtil.getOutlier(counts);
//                        if(outlier != null) counts.remove(outlier);
//                    } while (outlier != null);

                    int maxCount = 0;
                    for (int count : counts) {
                        if (count > maxCount) {
                            maxCount = count;
                        }
                    }

                    addProfile(tableName, countsForInterval.getKey(), maxCount);
                }

            }
        } catch (SQLException ex) {
            Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void addProfile(String tableName, int intervalLength, int maxCount) throws SQLException {
        String query = "INSERT INTO Profiles(table_name, interval_length, count) VALUES ('"
                + tableName + "', " + intervalLength + ", " + maxCount + ")";
        DbUtils.executeUpdateOnDb(conn_traces, query);
    }

    static String composeInitailFrequencies() {
        String initialStr = "";
        for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
            if (i != 0) {
                initialStr = initialStr + ",";
            }
            initialStr = initialStr + Definitions.CAPTURE_FREQUENCIES[i];
        }
        return initialStr;
    }

    static String composeInitialTotals() {
        String initialStr = "";
        for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
            if (i != 0) {
                initialStr = initialStr + ",";
            }
            initialStr = initialStr + "0";
        }
        return initialStr;
    }

    

    private CountsArr recordCountsInTrackingDb(String tableName,
            Timestamp accessTime, String attsValsStr) throws SQLException {
        
        Statement stmt;
        stmt = conn_tracking.createStatement();
        String query = "SELECT cnts, start_time FROM "
                + tableName + " WHERE " + attsValsStr;
        ResultSet rs = stmt.executeQuery(query);

        ArrayList<Integer> countsArr = new ArrayList();

        if (rs.next()) {
            String counts = rs.getString(1);
            Timestamp startTime = rs.getTimestamp(2);

            String[] oldCounts = null;
            if (counts != null) {
                oldCounts = counts.split(",");
            } else {
                startTime = new Timestamp(accessTime.getTime()
                        - (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR) * (Definitions.NUM_SLOTS - 1));
            }

            for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
                if (counts == null) {
                    countsArr.add(0);
                } else {
                    countsArr.add(new Integer(oldCounts[i]));
                }
            }

            long diff = (accessTime.getTime() - startTime.getTime()) / (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR);
            Timestamp newTime = startTime;
            if (diff >= Definitions.NUM_SLOTS) {
                for (int i = 0; i <= diff - Definitions.NUM_SLOTS; i++) {
                    for (int j = 0; j < Definitions.CAPTURE_FREQUENCIES.length; j++) {
                        int sum = 0;
                        for (int k = 0; k < Definitions.CAPTURE_FREQUENCIES[j]; k++) {
                            if(countsArr.get(k) > 0) {
                                sum++;
                            }
                        }recordCount(tableName, sum, newTime, Definitions.CAPTURE_FREQUENCIES[j]);
                    }
                    newTime = new Timestamp(newTime.getTime()
                        + (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR));
                    countsArr.remove(0);
                    countsArr.add(0);
                }
            } 

            int index = (int) ((accessTime.getTime() - newTime.getTime()) / (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR));
            countsArr.set((int) index, countsArr.get((int) index) + 1);

            String newCounts = "";
            for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
                if (i > 0) {
                    newCounts = newCounts + ",";
                }
                newCounts = newCounts + countsArr.get(i);
            }

            String newStart = newTime.toString();

            rs.close();
            stmt.close();

            return new CountsArr(newCounts, newStart);
        }

        return null;

    }

    private void recordQuery(ParsedQuery pq, Timestamp queryTime) throws SQLException {
        if (!pq.newQuery.isEmpty()) {
            // execute newQuery on training DB
            // loop on the result-set rows 
            // mark the corresponding rows in trackingDB

            if (!DbUtils.tableExists(conn_training, pq.table1Name)
                    || (!pq.table2Name.isEmpty() && !DbUtils.tableExists(conn_training, pq.table2Name))) {
                return;
            }

            System.out.println(pq.newQuery);

//            Statement stmt_c;
//            stmt_c = conn_training.createStatement();
//            ResultSet rs_c;
//            String subQuery = pq.newQuery.substring(pq.newQuery.indexOf(" FROM"));
//            String q_c = "SELECT count(*) " + subQuery;
//            if(q_c.toLowerCase().contains("order by")) {
//                q_c = q_c.substring(0, q_c.toLowerCase().indexOf("order by"));
//            }
//            rs_c = stmt_c.executeQuery(q_c);
//            rs_c.next();
//            int count = rs_c.getInt(1);
//            rs_c.close();
//            stmt_c.close();
//            if(count > 5) {
//                return;
//            }
            
            

            Statement stmt;
            stmt = conn_training.createStatement();
            ResultSet rs;
            rs = stmt.executeQuery(pq.newQuery);

            ArrayList<Attribute> atts1 = DbUtils.findTablePKs(conn_training, pq.table1Name);
            ArrayList<Attribute> atts2 = DbUtils.findTablePKs(conn_training, pq.table2Name);

            int rowCount = 0;
            while (rs.next()) {
                rowCount++;
                
                String attsValsStr = "";
                int i;
                for (i = 0; i < atts1.size(); i++) {
                    if (i != 0) {
                        attsValsStr = attsValsStr + " AND ";
                    }
                    attsValsStr = attsValsStr + atts1.get(i).getName() + " = ";
                    int attVal = rs.getInt(i + 1);
                    attsValsStr = attsValsStr + attVal;
                }

                // send query
                CountsArr ca1 = recordCountsInTrackingDb(pq.table1Name,
                        queryTime, attsValsStr);
                if (ca1 != null) {
                    updateTrackingTable(pq.table1Name, ca1, attsValsStr);
                }

                if (!pq.table2Name.isEmpty()) {
                    attsValsStr = "";
                    for (; i < atts1.size() + atts2.size(); i++) {
                        if (i - atts1.size() > 0) {
                            attsValsStr = attsValsStr + " AND ";
                        }
                        attsValsStr = attsValsStr
                                + atts2.get(i - atts1.size()).getName()
                                + " = ";
                        int attVal = rs.getInt(i + 1);
                        attsValsStr = attsValsStr + attVal;
                    }
                    CountsArr ca2 = recordCountsInTrackingDb(pq.table2Name,
                            queryTime, attsValsStr);
                    if (ca2 != null) {
                        updateTrackingTable(pq.table2Name, ca2, attsValsStr);
                    }
                }
            }

            System.out.println(pq.origQuery);
            System.out.println(pq.newQuery);
            System.out.println(rowCount);
            
            stmt.close();
            rs.close();
        } 
    }

    public void addToTrainingQueries(String queryStr, Timestamp queryTime) throws SQLException {
        ParsedQuery pq = Parser.parseQuery(conn_training, queryStr, queryTime);
        if(pq != null && !pq.isAgg) {
            pq.origQuery = queryStr;
            recordQuery(pq, queryTime);
        }
    }

    public Stats computeStats() throws SQLException {
        ArrayList<String> allTables = 
                DbUtils.listTables(conn_training, Definitions.TRAINING_DB_NAME);
        
        Stats stats = new Stats();
        
        int total = 0;
        int totalRowsAccessed = 0;
        float totalAverageTimesPerRow = 0;
        float totalMaxTimesPerRow = 0;
        for(String tableName : allTables) {
            total += DbUtils.numRows(tableName, conn_tracking);
            totalRowsAccessed += DbUtils.countAccessed(conn_tracking, tableName);
            totalAverageTimesPerRow += DbUtils.avgRowAccesses(conn_traces, tableName);
            totalMaxTimesPerRow += DbUtils.maxRowAccesses(conn_traces, tableName);
        }
        stats.avgNumRowsAccessed = (float)totalRowsAccessed / total;
        stats.avgRowAccess = (float)totalAverageTimesPerRow / allTables.size();
        stats.maxRowAccess = (float)totalMaxTimesPerRow / allTables.size();
        
        return stats;
    }

    public void addQueries(ArrayList<Query> trainingQueries) throws SQLException {
        for(Query query : trainingQueries) {
            addToTrainingQueries(query.str, query.time);
        }
        constructProfiles();
        closeConnections();
    }

}
