/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package checkingupdates.Training;

import Utils.Definitions;
import Utils.Query;
import Utils.QueryInStateMachine;
import java.sql.Timestamp;
import java.util.HashMap;

/**
 *
 * @author asmaasallam
 */
public class Trainer_SM {

    Timestamp lastQueryTime;
    QueryInStateMachine QIM;
    
    HashMap<String, QueryInStateMachine> allSMs;
    
    private static final int T_DIFF = Definitions.ONE_HOUR / 10;
    
    public Trainer_SM() {
        allSMs = new HashMap<>();
    }
    
    public void addQuery(Query q) {
        if(lastQueryTime != null && q.getTime().getTime() - lastQueryTime.getTime() < T_DIFF) {
            QIM = QIM.addNextQuery(q.template);
            lastQueryTime = q.time;
        } else {
            QIM = allSMs.get(q.template);
            if(QIM == null) {
                QIM = new QueryInStateMachine(q.template);
                allSMs.put(q.template, QIM);
            }
            lastQueryTime = q.time;
        }
    }
    
}
