package Main;

import Detection.A_Detector;
import Detection.AnomalousRow;
import Detection.TableAnomalies;
import Utils.DbUtils;
import Utils.Definitions;
import Utils.Query;
import checkingupdates.Training.SetupDB;
import checkingupdates.Training.Trainer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class Main {

    public static ArrayList<Query> readQueries(Connection conn, String tableName) throws SQLException {
        Statement stmt;
        String query = "SELECT sql, starttime from "
                + tableName + " WHERE (lower(sql) LIKE 'select%') order by starttime ";
        ResultSet rs;

        stmt = conn.createStatement();
        rs = stmt.executeQuery(query);

        ArrayList<Query> allQueries = new ArrayList<>();

        while (rs.next()) {
            String queryStr = rs.getString(1);
            Timestamp queryTime = rs.getTimestamp(2);
            Query traceQuery = new Query(queryStr, queryTime);

            allQueries.add(traceQuery);
        }
        rs.close();
        stmt.close();
        return allQueries;
    }

    static ArrayList<Integer> timeToAD = new ArrayList<>();
    static int totalUndetected = 0;
    static int total = 0;

    public static void checkQueries(String trainingTableName, String anomalousQueriesTableName) {
        try {
            float timeToAnomalousQuery = 0.3f;
            int numReps = 3;

            Connection conn_traces;
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);

            ArrayList<Query> trainingQueries = readQueries(conn_traces, trainingTableName);

            Trainer trainer = new Trainer();
            trainer.addQueries(trainingQueries);
            System.out.println("Done training");
            
            ArrayList<Query> detectionQueries = readQueries(conn_traces, anomalousQueriesTableName);
            ArrayList<Query> tempDetectionQueries = new ArrayList<>();
            ArrayList<Query> newDetectionQueries = new ArrayList<>();
            ArrayList<Boolean> anomalousQueries = new ArrayList<>();

            for (int i = 0; i < detectionQueries.size(); i++) {
                Query detectionQuery = detectionQueries.get(i);
                for (int k = 0; k < numReps; k++) {
                    Timestamp ts = new Timestamp((long) (detectionQuery.time.getTime()
                            + (k + 1) * timeToAnomalousQuery * Definitions.SLOT_LENGTH * Definitions.ONE_HOUR * Definitions.CAPTURE_FREQUENCIES[0]));
                    Query newQuery = new Query(detectionQuery.str, ts);
                    tempDetectionQueries.add(newQuery);
                }

                Iterator<Query> queriesIter = tempDetectionQueries.iterator();
                while (queriesIter.hasNext()) {
                    Query tempQuery = queriesIter.next();
                    if (tempQuery.time.before(detectionQuery.time)) {
                        newDetectionQueries.add(tempQuery);
                        anomalousQueries.add(Boolean.TRUE);
                        queriesIter.remove();
                    } else {
                        break;
                    }
                }

                newDetectionQueries.add(detectionQuery);
                anomalousQueries.add(Boolean.FALSE);
            }
            for (int j = 0; j < tempDetectionQueries.size(); j++) {
                Query tempQuery = tempDetectionQueries.get(j);
                newDetectionQueries.add(tempQuery);
                anomalousQueries.add(Boolean.TRUE);
            }

            A_Detector detector = new A_Detector();
            HashMap<String, HashMap<String, ArrayList<AnomalousRow>>> expectedAnomalies = new HashMap<>();

            HashMap<String, HashMap<String, AnomalousRow>> anomalousRows = new HashMap<>();

            HashMap<String, ArrayList<String>> previousAnomalies = new HashMap<>();
            HashMap<String, HashMap<String, ArrayList<Integer>>> allAnomalies = new HashMap<>();

            int totalParsedQueries = 0;
            for (int i = 0; i < newDetectionQueries.size(); i++) {
                Query q = newDetectionQueries.get(i);
                //System.out.println(i + "/" + newDetectionQueries.size());
                int result[] = detector.checkQuery(q.str, q.time, expectedAnomalies,
                        anomalousRows, anomalousQueries.get(i), previousAnomalies, allAnomalies);
                if (result != null) {
                    totalParsedQueries++;
                }
            }

//            int total = 0;
//            int anomalousRows2 = 0;
//            for (Map.Entry<String, HashMap<String, ArrayList<Integer>>> anomaly : allAnomalies.entrySet()) {
//                for (Map.Entry<String, ArrayList<Integer>> e : anomaly.getValue().entrySet()) {
//                    ArrayList<Integer> anomalies = e.getValue();
//                    if(anomalies != null && anomalies.size() > 0) {
//                        total++;
//                    }
//                    if (anomalies != null && anomalies.size() > 1) {
//                        anomalousRows2++;
//                    }
//                }
//            }
//            System.out.println("AnomalousRows2 = " + anomalousRows2
//                    + ", total = " + total);

            int totalAnomalousRows = 0;
            int totalDetectedAnomalous = 0;
            int totalDetectedAnomalous2 = 0;
            
            for (Map.Entry<String, HashMap<String, AnomalousRow>> anomaly : anomalousRows.entrySet()) {
                for (Map.Entry<String, AnomalousRow> e : anomaly.getValue().entrySet()) {
                    totalAnomalousRows++;
                    if (e.getValue().nIntervals() > 0) {
                        totalDetectedAnomalous++;
                    }
                    if (e.getValue().nIntervals() > 1) {
                        totalDetectedAnomalous2++;
                    }
                }
            }
            System.out.println(totalAnomalousRows + "," + totalDetectedAnomalous + "," + totalDetectedAnomalous2);

//            for (Map.Entry<String, HashMap<String, ArrayList<AnomalousRow>>> anomaly : expectedAnomalies.entrySet()) {
//                int tableUndetected = 0;
//                int tableTotal = 0;
//                String tableName = anomaly.getKey();
//                for (Map.Entry<String, ArrayList<AnomalousRow>> e : anomaly.getValue().entrySet()) {
//                    total++;
//                    tableTotal++;
//                    if (e.getValue() != null && e.getValue().size() > 0) {
//                        totalUndetected++;
//                        tableUndetected++;
//                    }
//                }
//                addTableInfo(tableName, tableUndetected, tableTotal);
//            }

            detector.closeConnections();
            detector.printTimeToAnomalyDetection();

            timeToAD.addAll(detector.getTimeToAnomalyDetection());

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] strs) {

//        Main.checkQueries("danger", "danger");
        SetupDB setupDB = new SetupDB();
        try {
            setupDB.resetTables();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        setupDB.closeConnections();

        Main.checkQueries("danger", "danger");

//        Main.checkQueries("danger", "danger_0128_10");
//        Main.checkQueries("danger", "danger_0128_11");
//        Main.checkQueries("danger", "danger_0128_13");
//
//        Main.checkQueries("danger", "danger_0304_10");
//        Main.checkQueries("danger", "danger_0304_12");
//        Main.checkQueries("danger", "danger_0304_13");
//        Main.checkQueries("danger", "danger_0304_14");
        System.out.println("==================================================");
        System.out.println("total: " + total + ", Undetected: " + totalUndetected);
        System.out.println("Average time to AD: " + getAvgTimeToAD());
        printAllTablesTotals();
    }

    private static float getAvgTimeToAD() {
        int totalTime = 0;
        for (int time : timeToAD) {
            totalTime += time;
        }
        return ((float) totalTime / timeToAD.size());
    }

    static HashMap<String, TableAnomalies> allTablesAnomalies = new HashMap<>();

    private static void addTableInfo(String tableName, int tableUndetected, int tableTotal) {
        TableAnomalies tableAnomalies = allTablesAnomalies.get(tableName);
        if (tableAnomalies == null) {
            tableAnomalies = new TableAnomalies();
            allTablesAnomalies.put(tableName, tableAnomalies);
        }
        tableAnomalies.addToTotal(tableTotal, tableUndetected);
    }

    private static void printAllTablesTotals() {
        for (Map.Entry<String, TableAnomalies> entry : allTablesAnomalies.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().undetected + "/" + entry.getValue().total);
        }
    }
}
