/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Detection;

import Utils.DbUtils;
import Utils.Definitions;
import Utils.ParsedQuery;
import Utils.Parser;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class A_Detector {

    static Connection conn_traces;
    static Connection conn_target;

    public void startConnections() {
        try {
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);
            conn_target = DbUtils.connectToDb(Definitions.TRAINING_DB_NAME);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(A_Detector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public A_Detector() {
        startConnections();
    }

    public void closeConnections() {
        try {
            DbUtils.closeConn(conn_traces);
            DbUtils.closeConn(conn_target);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    HashMap<String, TableAnomalies> allAnomalies = new HashMap<String, TableAnomalies>();

    public int[] checkQuery(String queryStr, Timestamp queryTime, ArrayList<String> anomalousTables) throws SQLException {
        ParsedQuery pq = Parser.parseQuery(conn_target, queryStr, queryTime);
        if (pq != null) {
            return recordQuery(pq, queryTime, anomalousTables);
        } else {
            return null;
        }
    }

    private int[] recordQuery(ParsedQuery pq, Timestamp queryTime, ArrayList<String> anomalousTables)
            throws SQLException {
        if (pq != null && !pq.newQuery.isEmpty()) {

            if (!DbUtils.tableExists(conn_target, pq.table1Name)
                    || (!pq.table2Name.isEmpty() && !DbUtils.tableExists(conn_target, pq.table2Name))) {
                return null;
            }

            int nAnomalies[] = new int[Definitions.CAPTURE_FREQUENCIES.length];

            DetectionSums result1 = computeCounts(pq.table1Name, queryTime);
            if (result1 != null) {
                for (int k = 0; k < nAnomalies.length; k++) {
                    if (result1.nAnomalies[k] > 0) {
                        nAnomalies[k]++;
                        // TODO doesn't work well for multiple tracking intervals
                        anomalousTables.add(pq.table1Name);
                    }
                }
                
                recordCounts(pq.table1Name, result1);
                addTableAnomalies(pq.table1Name, queryTime, result1);
                
            }

            if (!pq.table2Name.isEmpty()) {
                DetectionSums result2 = computeCounts(pq.table2Name, queryTime);
                if (result2 != null) {
                    for (int k = 0; k < nAnomalies.length; k++) {
                        if (result2.nAnomalies[k] > 0) {
                            // TODO doesn't work well for multiple tracking intervals
                            anomalousTables.add(pq.table2Name);
                            if (result1 != null && result1.nAnomalies[k] == 0) {
                                nAnomalies[k]++;
                            }
                        }
                    }
                    recordCounts(pq.table2Name, result2);
                    addTableAnomalies(pq.table2Name, queryTime, result2);
                }
            }

            return nAnomalies;
        }
        return null;
    }

    // TODO check conn_detection 
    // TODO check sel
    private void recordCounts(String tableName, DetectionSums result)
            throws SQLException {
        
        String query;
        if(result.addRow) {
            query = "INSERT INTO tables_access_detection_counts(table_name,start_time, counts, sel, user_name ,sums) VALUES"
                    + "('" + tableName + "', '" + result.startTime 
                    + "', '"  + result.counts + "', 1, '" + Definitions.ONE_USER 
                    + "', '" + result.sums + "')";
        } else {
            query = "UPDATE tables_access_detection_counts"
                    + " SET counts = '" + result.counts + "', "
                    + "start_time = '" + result.startTime + "', sums = '"
                    + result.sums + "' WHERE table_name = '" + tableName + "' and user_name = '"
                    + Definitions.ONE_USER + "'";
        }
        DbUtils.executeUpdateOnDb(conn_traces, query);
    }

    private DetectionSums computeCounts(String tableName, Timestamp accessTime) throws SQLException {
//        if(tableName.equals("Treatment_Schedule") || tableName.equals("InvoiceNo")) {
//            return null;
//        }
        // TODO check sel
        Statement stmt;
        stmt = conn_traces.createStatement();
        String query = "SELECT counts, start_time, sums FROM tables_access_detection_counts"
                + " WHERE table_name = '" + tableName
                + "' and user_name = '" + Definitions.ONE_USER + "'";
        ResultSet rs = stmt.executeQuery(query);

        ArrayList<Integer> countsArr = new ArrayList();
        ArrayList<Integer> sumsArr = new ArrayList();

        int nAnomalies[] = new int[Definitions.CAPTURE_FREQUENCIES.length];
        // TODO this has to be done once
        int thresholds[] = readThresholds(tableName);

        String counts = null;
        Timestamp startTime = null;
        String sums;
        String[] sumsParts = null;
        
        boolean addRow = false;

        if (rs.next()) {
            counts = rs.getString(1);
            startTime = rs.getTimestamp(2);
            sums = rs.getString(3);
            sumsParts = sums.split(",");
        } else {
            addRow = true;
        }

        String[] oldCounts = null;
        if (counts != null) {
            oldCounts = counts.split(",");
        } else {
            startTime = new Timestamp(accessTime.getTime()
                    - (1000 * 60 * 60) * (Definitions.NUM_SLOTS - 1));
        }

        for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
            if (counts == null) {
                countsArr.add(0);
            } else {
                countsArr.add(new Integer(oldCounts[i].trim()));
            }
        }

        for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
            if (sumsParts != null) {
                sumsArr.add(new Integer(sumsParts[i].trim()));
            } else {
                sumsArr.add(0);
            }
        }

        long diff = (accessTime.getTime() - startTime.getTime()) / (1000 * 60 * 60);
        Timestamp newTime;

        int nShifts = (int) (diff - Definitions.NUM_SLOTS + 1);
        if (nShifts > 0) {
            for (int i = 0; i < nShifts; i++) {
                for (int j = 0; j < Definitions.CAPTURE_FREQUENCIES.length; j++) {
                    int firstEntryVal = countsArr.get(Definitions.NUM_SLOTS - Definitions.CAPTURE_FREQUENCIES[j]);
                    sumsArr.set(j, sumsArr.get(j) - firstEntryVal);
                }
                countsArr.remove(0);
                countsArr.add(0);
            }

            newTime = new Timestamp(startTime.getTime()
                    + (diff - Definitions.NUM_SLOTS + 1) * (1000 * 60 * 60));
        } else {
            newTime = startTime;
        }

        // diff % NUM_SLOTS
        int index = (int) ((accessTime.getTime() - newTime.getTime()) / (1000 * 60 * 60));
        countsArr.set((int) index, countsArr.get((int) index) + 1);

        String newCounts = "";
        String newSums = "";
        for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
            if (index >= Definitions.NUM_SLOTS - Definitions.CAPTURE_FREQUENCIES[i]) {
                sumsArr.set(i, sumsArr.get(i) + 1);
            }

            int s = sumsArr.get(i);

            if (i > 0) {
                newSums = newSums + ",";
            }
            newSums = newSums + s;

            if (sumsArr.get(i) > thresholds[i]) {
                nAnomalies[i]++;
            }
        }

        for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
            int c = countsArr.get(i);
            if (i > 0) {
                newCounts = newCounts + ",";
            }
            newCounts = newCounts + c;
        }

        String newStart = newTime.toString();

        rs.close();
        stmt.close();

        return new DetectionSums(newCounts, newSums, newStart, nAnomalies, addRow);
    }

    public static int[] readThresholds(String tableName) throws SQLException {
        Statement stmt;
        String query = "SELECT threshold FROM tables_access_profiles"
                + " WHERE table_name = '" + tableName + "' and role_name = '"
                + Definitions.ONE_ROLE + "' order by interval_length";

        stmt = conn_traces.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(query);
        int[] thresholds = new int[Definitions.CAPTURE_FREQUENCIES.length];
        for (int i = 0; rs.next(); i++) {
            thresholds[i] = rs.getInt(1);
        }
        return thresholds;
    }

    private void addAnomaly(String tableName,
            HashMap<String, Integer> allAnomalies,
            int[] nAnomalies) {
        Integer tableAnomalies = allAnomalies.get(tableName);
        if (tableAnomalies == null) {
            tableAnomalies = 0;
            allAnomalies.put(tableName, tableAnomalies);
        }

        for (int i = 0; i < nAnomalies.length; i++) {
            if (nAnomalies[i] > 0) {
                tableAnomalies++;
            }
        }
    }

    private void addTableAnomalies(String table1Name, Timestamp queryTime, DetectionSums result1) {
        TableAnomalies ta = allAnomalies.get(table1Name);
        if(ta == null) {
            ta = new TableAnomalies();
            allAnomalies.put(table1Name, ta);
        }
        ta.addAnomaly(queryTime, result1.nAnomalies);
    }
    
    public void printN_Anomalies() {
        int nMatched = 0;
        int total = 0;
        for(Map.Entry<String, TableAnomalies> e : allAnomalies.entrySet()) {
            nMatched += e.getValue().findReps();
            total += e.getValue().findTotal();
        }
        System.out.println(nMatched + "/" + total);
    }
}
