/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Detection;

import Utils.Definitions;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author asmaasallam
 */
class TableAnomalies {
    public ArrayList<Timestamp> anomalyTimes;
    public ArrayList<Integer> intervalsLengths;
    
    public TableAnomalies() {
        anomalyTimes = new ArrayList<>();
        intervalsLengths = new ArrayList<>();
    }
    
    public void addAnomaly(Timestamp anomalyTime, int intervalLength) {
        anomalyTimes.add(anomalyTime);
        intervalsLengths.add(intervalLength);
    }
    
    public void addAnomaly(Timestamp queryTime, int nAnomalies[]) {
        for(int i = 0; i < nAnomalies.length; i++) {
            if(nAnomalies[i] > 0) {
                addAnomaly(queryTime, Definitions.CAPTURE_FREQUENCIES[i]);
            }
        }
    }
    
    public int findTotal() {
        return anomalyTimes.size();
    }
    
    public int findReps() {
        int matched = 0;
        
        for(int i = 0; i < anomalyTimes.size(); i++) {
            Timestamp anomalyTime_i = anomalyTimes.get(i);
            int intervalLength_i = intervalsLengths.get(i);
            
            boolean match = false;
            for(int j = 0; j < anomalyTimes.size(); j++) {
                if(i == j) {
                    continue;
                }
                Timestamp anomalyTime_j = anomalyTimes.get(j);
                int intervalLength_j = intervalsLengths.get(j);
                
                if(intervalLength_j == intervalLength_i) {
                    continue;
                }
                
                long diff = 0;
                if(j > i) {
                    diff = anomalyTime_j.getTime() - anomalyTime_i.getTime();
                } else {
                    diff = anomalyTime_i.getTime() - anomalyTime_j.getTime();
                }
                
                float diff_f = (float)diff / Definitions.ONE_HOUR;
                if(diff_f <= Math.abs(intervalLength_j - intervalLength_i)) {
                    match = true;
                }
                
            }
            if(match) {
                matched++;
            }
        }
        
        return matched;
    } 
}
