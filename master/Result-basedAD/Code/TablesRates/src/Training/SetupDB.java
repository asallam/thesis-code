/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Training;

import Detection.A_Detector;
import Utils.DbUtils;
import Utils.Attribute;
import Utils.Definitions;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class SetupDB {

    Connection conn_target = null;
    Connection conn_tracking = null;
    Connection conn_detection = null;
    Connection conn_traces = null;

    ArrayList<String> allTables;
    HashMap<String, ArrayList<Attribute>> tablesPKs;

    private static final String TARGET_DB_NAME = "DangerTemplate";
    private static final String TRACKING_DB_NAME = "trainingDb";
    private static final String DETECTION_DB_NAME = "detectionDb";

    public SetupDB() {
        connectToTarget();
        connectToTracking();
        connectToDetection();
        connectToTraces();
    }

    public void connectToTarget() {
        try {
            conn_target = DbUtils.connectToDb(TARGET_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void connectToTracking() {
        try {
            conn_tracking = DbUtils.connectToDb(TRACKING_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void connectToTraces() {
        try {
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void connectToDetection() {
        try {
            conn_detection = DbUtils.connectToDb(DETECTION_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn_tracking() {
        try {
            DbUtils.closeConn(conn_tracking);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn_target() {
        try {
            DbUtils.closeConn(conn_target);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConn_detection() {
        try {
            DbUtils.closeConn(conn_detection);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void closeConn_traces() {
        try {
            DbUtils.closeConn(conn_traces);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnections() {
        closeConn_target();
        closeConn_tracking();
        closeConn_detection();
        closeConn_traces();
    }
    
    public void resetTables() throws SQLException {
        String query1 = "DELETE FROM users_tables_access_counts";
        Statement stmt1 = conn_traces.createStatement();
        stmt1.executeUpdate(query1);
        stmt1.close();
        
        String query2 = "DELETE FROM tables_access_profiles";
        Statement stmt2 = conn_traces.createStatement();
        stmt2.executeUpdate(query2);
        stmt2.close();
        
        String query3 = "DELETE FROM tables_access_training_counts";
        Statement stmt3 = conn_traces.createStatement();
        stmt3.executeUpdate(query3);
        stmt3.close();
        
        String query4 = "DELETE FROM tables_access_detection_counts";
        Statement stmt4 = conn_traces.createStatement();
        stmt4.executeUpdate(query4);
        stmt4.close();
        
    }

    public static void main(String[] args) {
        try {
            
            SetupDB setupDb = new SetupDB();
            setupDb.resetTables();
            setupDb.closeConnections();
            
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
