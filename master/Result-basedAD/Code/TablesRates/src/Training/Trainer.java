/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Training;

import Utils.DbUtils;
import Utils.Definitions;
import Utils.MathUtil;
import Utils.ParsedQuery;
import Utils.Parser;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class Trainer {

    Connection conn_traces;
    Connection conn_training;

    public Trainer() {
        startConnections();
    }

    public void startConnections() {
        try {
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);
            conn_training = DbUtils.connectToDb(Definitions.TRAINING_DB_NAME);
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnections() {
        try {
            DbUtils.closeConn(conn_traces);
            DbUtils.closeConn(conn_training);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // called whenever an interval ends during training
    private void recordCount(String roleName, String tableName, int count,
            int intervalLength, int sel, String bucketTime) throws SQLException {
        if (count > 0) {
            String query = "INSERT INTO tables_access_training_counts"
                    + "(role_name, table_name, count, interval_length, sel, bucket_time) VALUES ("
                    + "'" + roleName + "', '" + tableName + "', "
                    + count + ", " + intervalLength
                    + ", " + sel + ", '" + bucketTime + "')";
            DbUtils.executeUpdateOnDb(conn_traces, query);
        }
    }

    private void updateUserTableAccess(String userName, String tableName,
            CountsArr ca1) throws SQLException {
        String query12;
        if(ca1.addRow) { 
            // TODO add role info
            query12 = "INSERT INTO users_tables_access_counts(counts, table_name, user_name, start_time) values ("
                    + "'" + ca1.counts.trim() + "', '" + tableName + "', '" + Definitions.ONE_USER
                    + "', '" + ca1.startTime + "')";
        } else {
            query12 = "UPDATE users_tables_access_counts"
                    + " SET counts = '" + ca1.counts.trim() + "', "
                    + "start_time = '" + ca1.startTime + "' WHERE table_name = '"
                    + tableName + "' and user_name = '" + userName + "'";
        }
        DbUtils.executeUpdateOnDb(conn_traces, query12);
    }

    public float constructProfiles() {
        int total = 0;
        ArrayList<String> tablesNames;
        try {
            tablesNames = DbUtils.listTables(conn_training, Definitions.TRAINING_DB_NAME);
            for (String tableName : tablesNames) {

                // TODO add role and sel
                String query = "SELECT count, interval_length FROM tables_access_training_counts WHERE table_name = '"
                        + tableName + "'";
                Statement stmt;
                stmt = conn_traces.createStatement();
                ResultSet rs = stmt.executeQuery(query);

                HashMap<Integer, ArrayList> vals = new HashMap<>();
                while (rs.next()) {
                    int intervalLength = rs.getInt(2);
                    int count = rs.getInt(1);
                    
                    ArrayList<Integer> countsForInterval = vals.get(intervalLength);
                        if (countsForInterval == null) {
                            countsForInterval = new ArrayList<>();
                            vals.put(intervalLength, countsForInterval);
                        }
                        countsForInterval.add(count);
                }

                for (Map.Entry<Integer, ArrayList> countsForInterval : vals.entrySet()) {
                    MathUtil mathUtil = new MathUtil();
                    ArrayList<Integer> counts = countsForInterval.getValue();
                    Integer outlier;
                    
//                    do {
//                        outlier = mathUtil.getOutlier(counts);
//                        if(outlier != null) counts.remove(outlier);
//                    } while (outlier != null);

                    int maxCount = 0;
                    for (int count : counts) {
                        if (count > maxCount) {
                            maxCount = count;
                        }
                    }

                    addToProfile(Definitions.ONE_ROLE, tableName, countsForInterval.getKey(), maxCount);
                    total += maxCount;
                }
            }
            return (float)total/tablesNames.size();
        } catch (SQLException ex) {
            Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    private void addToProfile(String roleName, String tableName, int intervalLength, int threshold) throws SQLException {
        String query = "INSERT INTO tables_access_profiles(role_name, table_name, threshold, interval_length) VALUES ("
                + "'" + roleName + "', '" + tableName + "', " + threshold + ", " + intervalLength + ")";
        DbUtils.executeUpdateOnDb(conn_traces, query);
    }

    static String composeInitialTotals() {
        String initialStr = "";
        for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
            if (i != 0) {
                initialStr = initialStr + ",";
            }
            initialStr = initialStr + "0";
        }
        return initialStr;
    }

    private void recordQuery(ParsedQuery pq, Timestamp queryTime) throws SQLException {
        if (!pq.newQuery.isEmpty()) {
            // execute newQuery on training DB
            // loop on the result-set rows 
            // mark the corresponding rows in tracesDB

            if (!DbUtils.tableExists(conn_training, pq.table1Name)
                    || (!pq.table2Name.isEmpty() && !DbUtils.tableExists(conn_training, pq.table2Name))) {
                return;
            }

            CountsArr ca1 = recordCountsInDb(pq.table1Name, queryTime);
            if (ca1 != null) {
                updateUserTableAccess(Definitions.ONE_USER, pq.table1Name, ca1);
            }

            if (!pq.table2Name.isEmpty()) {
                CountsArr ca2 = recordCountsInDb(pq.table2Name,
                        queryTime);
                if (ca2 != null) {
                    updateUserTableAccess(Definitions.ONE_USER, pq.table2Name, ca2);
                }
            }
        }

    }

    private CountsArr recordCountsInDb(String tableName,
            Timestamp accessTime) throws SQLException {
        boolean addRow = false;
        Statement stmt;
        stmt = conn_traces.createStatement();
        String query = "SELECT counts, start_time FROM users_tables_access_counts WHERE "
                + "user_name = '" + Definitions.ONE_USER + "' and table_name = '" + tableName + "'";
        ResultSet rs = stmt.executeQuery(query);

        ArrayList<Integer> countsArr = new ArrayList();

        String counts = null;
        Timestamp startTime = null;

        if (rs.next()) {
            counts = rs.getString(1);
            startTime = rs.getTimestamp(2);
        }

        String[] oldCounts = null;
        if (counts != null) {
            oldCounts = counts.split(",");
        } else {
            startTime = new Timestamp(accessTime.getTime()
                    - (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR) * (Definitions.NUM_SLOTS - 1));
            addRow = true;
        }

        for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
            if (counts == null) {
                countsArr.add(0);
            } else {
                countsArr.add(new Integer(oldCounts[i].trim()));
            }
        }

        long diff = (accessTime.getTime() - startTime.getTime()) / (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR);
        Timestamp newTime;
        if (diff >= Definitions.NUM_SLOTS) {
            for (int i = 0; i <= diff - Definitions.NUM_SLOTS; i++) {
                for (int j = 0; j < Definitions.CAPTURE_FREQUENCIES.length; j++) {
                    int sum = 0;
                    for (int k = 0; k < Definitions.CAPTURE_FREQUENCIES[j]; k++) {
                        sum += countsArr.get(k);
                    }
                    // TODO set sel
                    Timestamp bucketTime = new Timestamp(startTime.getTime()
                    + i * (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR));
                    recordCount(Definitions.ONE_ROLE, tableName, sum, Definitions.CAPTURE_FREQUENCIES[j], 1, bucketTime.toString());
                }
                countsArr.remove(0);
                countsArr.add(0);
            }

            newTime = new Timestamp(startTime.getTime()
                    + (diff - Definitions.NUM_SLOTS + 1) * (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR));

        } else {
            newTime = startTime;
        }

        int index = (int) ((accessTime.getTime() - newTime.getTime()) / (Definitions.SLOT_LENGTH * Definitions.ONE_HOUR));
        countsArr.set((int) index, countsArr.get((int) index) + 1);

        String newCounts = "";
        for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
            if (i > 0) {
                newCounts = newCounts + ",";
            }
            newCounts = newCounts + countsArr.get(i);
        }

        String newStart = newTime.toString();

        rs.close();
        stmt.close();

        return new CountsArr(newCounts, newStart, addRow);
    }
    
    public void addToTrainingQueries(String queryStr, Timestamp queryTime) throws SQLException {
        ParsedQuery pq = Parser.parseQuery(conn_training, queryStr, queryTime);
        
        if(pq != null) {
            pq.origQuery = queryStr;
            recordQuery(pq, queryTime);
        }
    }

    
    
}
