package Main;

import Detection.A_Detector;
import Training.SetupDB;
import Training.Trainer;
import Utils.DbUtils;
import Utils.Definitions;
import Utils.ParsedQuery;
import Utils.Parser;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class Main {
    static HashMap<String, ArrayList<Integer>> timeToADPerTable;
    static int totalAnomalies;

    // false-positives
    
    public static void readQueriesInTable(String tableName) {
        try {
            int totalNumRows = 6105;
            float[] portionTraining = {0.8f, 0.7f, 0.6f, 0.5f, 0.4f, 0.3f, 0.2f, 0.1f};

            for (int t = 0; t < portionTraining.length; t++) {
                SetupDB setupDB = new SetupDB();
                setupDB.resetTables();
                setupDB.closeConnections();

                Connection conn_traces;
                conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);

                Statement stmt;
                String query = "SELECT sql, starttime from "
                        + tableName + " WHERE (lower(sql) LIKE 'select%') order by starttime ";
                ResultSet rs;

                stmt = conn_traces.createStatement();
                rs = stmt.executeQuery(query);

                Trainer trainer = new Trainer();
                A_Detector detector = new A_Detector();
                int nAnomalousQueries[] = new int[Definitions.CAPTURE_FREQUENCIES.length];

                Timestamp firstQueryTime = null;
                for (int i = 0; rs.next(); i++) {
                    String queryStr = rs.getString(1);
                    Timestamp queryTime = rs.getTimestamp(2);
                    
//                    if(i == 0) {
//                        firstQueryTime = queryTime;
//                    } else {
//                        long diff = queryTime.getTime() - firstQueryTime.getTime();
////                        System.out.println(diff);
//                    }
                    
                    if (i <= (int) (portionTraining[t] * totalNumRows)) {
                        trainer.addToTrainingQueries(queryStr, queryTime);
                        if (i == (int) (portionTraining[t] * totalNumRows)) {
                            float avgReadsPerHour = trainer.constructProfiles();
                            trainer.closeConnections();
                        }
                    } else if (i > (int) (0.8f * totalNumRows)) {
                        ArrayList<String> anomalousTables = new ArrayList<>();
                        int anomalies[] = detector.checkQuery(queryStr, queryTime, anomalousTables);
                        if (anomalies != null) {
                            for (int k = 0; k < anomalies.length; k++) {
                                if (anomalies[k] > 0) {
                                    nAnomalousQueries[k]++;
                                }
                            }
                        }
                    }
                }
                detector.closeConnections();

                BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt", true));
                PrintWriter out = new PrintWriter(bw);

                out.println("portionTraining: " + portionTraining[t]);

                for (int i = 0; i < nAnomalousQueries.length; i++) {
                    out.println(nAnomalousQueries[i]);
                }
                out.println();

                out.close();
                stmt.close();
                rs.close();
                DbUtils.closeConn(conn_traces);

                System.out.println("=======================");
                detector.printN_Anomalies();
                System.out.println("=======================");
            }

        } catch (SQLException ex) {
            Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // to measure the false-negatives
/*    public static void readQueriesInTable(String tableName) {
        try {
            int totalNumRows = 6105;
            float[] portionTraining = {0.8f};//, 0.9f, 0.8f, 0.7f, 0.6f, 0.5f, 0.4f, 0.3f, 0.2f, 0.1f};

            for (int numReps = 3; numReps < 4; numReps++) {
                totalAnomalies = 0;
                timeToADPerTable = new HashMap<>();

                for (int t = 0; t < portionTraining.length; t++) {
                    ArrayList<String> detectionQueries = new ArrayList<>();
                    ArrayList<Timestamp> detectionQueriesTimeStamps = new ArrayList<>();

                    HashMap<String, ArrayList<Timestamp>> expectedAnomalies = new HashMap<>();

                    float delta = 0.1f;

                    SetupDB setupDB = new SetupDB();
                    setupDB.resetTables();
                    setupDB.closeConnections();

                    Connection conn_traces;
                    conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);

                    Connection conn_training;
                    conn_training = DbUtils.connectToDb(Definitions.TRAINING_DB_NAME);

                    Statement stmt;
                    String query = "SELECT sql, starttime from "
                            + tableName + " WHERE (lower(sql) LIKE 'select%') order by starttime ";
                    ResultSet rs;

                    stmt = conn_traces.createStatement();
                    rs = stmt.executeQuery(query);

                    Trainer trainer = new Trainer();
                    A_Detector detector = new A_Detector();

                    Timestamp firstQueryTime = null;
                    for (int i = 0; rs.next(); i++) {
                        String queryStr = rs.getString(1);
                        Timestamp queryTime = rs.getTimestamp(2);

                        if (i == 0) {
                            firstQueryTime = queryTime;
                        } else {
                            long diff = queryTime.getTime() - firstQueryTime.getTime();
                            System.out.println(diff);
                        }

                        if (i <= (int) (portionTraining[t] * totalNumRows)) {
                            trainer.addToTrainingQueries(queryStr, queryTime);
                            if (i == (int) (portionTraining[t] * totalNumRows)) {
                                trainer.constructProfiles();
                                trainer.closeConnections();
                            }
                        } else {
                            for (int r = 0; r <= numReps; r++) {
                                detectionQueries.add(queryStr);
                                Timestamp newQueryTime = new Timestamp((long) (queryTime.getTime() + r * delta * (Definitions.CAPTURE_FREQUENCIES[0] * Definitions.SLOT_LENGTH * Definitions.ONE_HOUR)));
                                detectionQueriesTimeStamps.add(newQueryTime);
                                if (r > 0) {
                                    ParsedQuery pq = Parser.parseQuery(conn_training, queryStr, newQueryTime);
                                    if (pq != null) {
                                        addExpectedAnomalies(pq, queryTime, expectedAnomalies);
                                    }
                                }
                            }
                        }
                    }

                    BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt", true));
                    PrintWriter out = new PrintWriter(bw);

                    out.println("Nr: " + numReps);
                    out.println("Total anomalies: " + totalAnomalies);

                    for (int k = 0; k < detectionQueries.size(); k++) {
                        ArrayList<String> anomalousTables = new ArrayList<>();
                        String queryStr = detectionQueries.get(k);
                        Timestamp queryTime = detectionQueriesTimeStamps.get(k);
                        int anomalies[] = detector.checkQuery(queryStr, queryTime, anomalousTables);
                        if (anomalies != null && anomalies[0] > 0) {
                            removeExpectedAnomalies(anomalousTables, queryTime, expectedAnomalies);
                        }
                    }
                    
                    out.println("Remaining anomalies: " + totalAnomalies);

                    float totalAvgs = 0;
                    for (Map.Entry<String, ArrayList<Integer>> e : timeToADPerTable.entrySet()) {
                        int total = 0;
                        for (Integer ee : e.getValue()) {
                            total += ee;
                        }
                        int[] threshold = A_Detector.readThresholds(e.getKey());
                        int numAnomalies = e.getValue().size();
                        if (threshold != null && threshold.length > 0 && threshold[0] != 0) {
                            float avg = (float)total / (threshold[0] * numAnomalies);
                            totalAvgs += avg;
                            out.println(e.getKey() + ": " + avg);
                        }
                    }

                    float avgTotalAvg = totalAvgs / timeToADPerTable.entrySet().size();
                    out.println("Avg of all tables: " + avgTotalAvg);
                    
                    out.println();

                    out.close();
                    stmt.close();
                    rs.close();
                    DbUtils.closeConn(conn_traces);
                    DbUtils.closeConn(conn_training);
                    
                    detector.closeConnections();

                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/

    public static void main(String[] strs) {
        Main.readQueriesInTable("danger");
    }

    private static void addExpectedAnomalies(ParsedQuery pq, Timestamp queryTime,
            HashMap<String, ArrayList<Timestamp>> expectedAnomalies) {
        addExpectedAnomaly(pq.table1Name, queryTime, expectedAnomalies);
        addExpectedAnomaly(pq.table2Name, queryTime, expectedAnomalies);
    }

    private static void removeExpectedAnomalies(ArrayList<String> anomalousTables, Timestamp queryTime,
            HashMap<String, ArrayList<Timestamp>> expectedAnomalies) {
        for (String tableName : anomalousTables) {
            removeExpectedAnomaly(tableName, queryTime, expectedAnomalies);
        }
    }

    private static void addExpectedAnomaly(String tableName, Timestamp queryTime,
            HashMap<String, ArrayList<Timestamp>> expectedAnomalies) {
        if (tableName != null && !tableName.isEmpty()) {
            ArrayList<Timestamp> tableAnomalies = expectedAnomalies.get(tableName);
            if (tableAnomalies == null) {
                tableAnomalies = new ArrayList<>();
                expectedAnomalies.put(tableName, tableAnomalies);
            }
            tableAnomalies.add(queryTime);
            totalAnomalies++;
        }
    }
    
    private static void removeExpectedAnomaly(String tableName, Timestamp queryTime, HashMap<String, ArrayList<Timestamp>> expectedAnomalies) {
        if (tableName != null && !tableName.isEmpty()) {
            ArrayList<Timestamp> tableAnomalies = expectedAnomalies.get(tableName);
            if (tableAnomalies != null) {
                Iterator<Timestamp> iter = tableAnomalies.iterator();
                int last;
                for (last = 0; iter.hasNext(); last++) {
                    Timestamp tableAnomalyTime = iter.next();
                    long diff = queryTime.getTime() - tableAnomalyTime.getTime();
                    if (diff >= 0 && diff < (Definitions.CAPTURE_FREQUENCIES[0] * Definitions.SLOT_LENGTH * Definitions.ONE_HOUR)) {
                        iter.remove();
                        totalAnomalies--;
                    }
                }
                ArrayList<Integer> tableTimeToAD = timeToADPerTable.get(tableName);
                if (tableTimeToAD == null) {
                    tableTimeToAD = new ArrayList<>();
                    timeToADPerTable.put(tableName, tableTimeToAD);
                }
                for (int i = last; i > 0; i--) {
                    tableTimeToAD.add(i);
                }
            }
        }
    }
}
