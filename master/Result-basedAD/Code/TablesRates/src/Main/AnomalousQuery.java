/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Main;

import java.sql.Timestamp;

/**
 *
 * @author asmaasallam
 */
class AnomalousQuery {
    String queryStr;
    Timestamp queryTime;
    
    public AnomalousQuery (String queryStr, Timestamp queryTime) {
        this.queryStr = queryStr;
        this.queryTime = queryTime;
    }
}
