/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Detection;

/**
 *
 * @author asmaasallam
 */
public class DetectionSums {

    String sums, counts, startTime;
    int nAnomalies[];
    
    public DetectionSums(String newCounts, String newSums, String newStart, int nAnomalies[]) {
        counts = newCounts;
        sums = newSums;
        startTime = newStart;
        this.nAnomalies = nAnomalies;
    }
    
}
