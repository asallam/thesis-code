package Detection;

import Utils.Attribute;
import Utils.DbUtils;
import Utils.Definitions;
import Utils.ParsedQuery;
import Utils.Parser;
import checkingupdates.Training.SetupDB;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class A_Detector {

    Connection conn_traces;
    Connection conn_target;
    Connection conn_detection;

    int[] rowAnomalies;
    int totalRowsInspected;
    
    ArrayList<Integer> timeToAnomalyDetection;

    public A_Detector() {
        startConnections();
        rowAnomalies = new int[Definitions.CAPTURE_FREQUENCIES.length];
        totalRowsInspected = 0;
        timeToAnomalyDetection = new ArrayList<>();
    }

    public void startConnections() {
        try {
            conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);
            conn_target = DbUtils.connectToDb(Definitions.TRAINING_DB_NAME);
            conn_detection = DbUtils.connectToDb(Definitions.DETECTION_DB_NAME);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(A_Detector.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void closeConnections() {
        try {
            DbUtils.closeConn(conn_traces);
            DbUtils.closeConn(conn_target);
            DbUtils.closeConn(conn_detection);
        } catch (SQLException ex) {
            Logger.getLogger(SetupDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int[] checkQuery(String queryStr, Timestamp queryTime, HashMap<String, HashMap<String, ArrayList<Integer>>> allAnomalies) throws SQLException {
        ParsedQuery pq = Parser.parseQuery(conn_target, queryStr, queryTime);
        if (pq != null && !pq.isAgg) {
            return recordQuery(pq, queryTime, allAnomalies);
        } else {
            return null;
        }
    }

    private int[] recordQuery(ParsedQuery pq, Timestamp queryTime,
            HashMap<String, HashMap<String, ArrayList<Integer>>> allAnomalies)
            throws SQLException {
        if (pq != null && !pq.newQuery.isEmpty()) {
            // execute newQuery on training DB
            // loop on the result-set rows 
            // mark the corresponding rows in trackingDB

            if (!DbUtils.tableExists(conn_target, pq.table1Name)
                    || (!pq.table2Name.isEmpty() && !DbUtils.tableExists(conn_target, pq.table2Name))) {
                return null;
            }

            int nAnomalies[] = new int[Definitions.CAPTURE_FREQUENCIES.length];

            Statement stmt;
            stmt = conn_target.createStatement();
            ResultSet rs;
            //System.out.println(pq.newQuery);
            rs = stmt.executeQuery(pq.newQuery);

            ArrayList<Attribute> atts1 = DbUtils.findTablePKs(conn_target, pq.table1Name);
            ArrayList<Attribute> atts2 = DbUtils.findTablePKs(conn_target, pq.table2Name);

            while (rs.next()) {
                String attsValsStr = "";
                int i;
                for (i = 0; i < atts1.size(); i++) {
                    if (i != 0) {
                        attsValsStr = attsValsStr + " AND ";
                    }
                    attsValsStr = attsValsStr + atts1.get(i).getName() + " = ";
                    int attVal = rs.getInt(i + 1);
                    attsValsStr = attsValsStr + attVal;
                }

                DetectionSums result1 = computeCounts(pq.table1Name, queryTime, attsValsStr);
                if (result1 != null) {
                    for (int k = 0; k < nAnomalies.length; k++) {
                        if (result1.nAnomalies[k] > 0) {
                            nAnomalies[k]++;
                        }
                    }
                    addAnomaly(pq.table1Name, attsValsStr, allAnomalies,
                                result1.nAnomalies);
                    recordCounts(pq.table1Name, result1, attsValsStr);
                }

                if (!pq.table2Name.isEmpty()) {
                    attsValsStr = "";
                    for (; i < atts1.size() + atts2.size(); i++) {
                        if (i - atts1.size() > 0) {
                            attsValsStr = attsValsStr + " AND ";
                        }
                        attsValsStr = attsValsStr
                                + atts2.get(i - atts1.size()).getName()
                                + " = ";
                        int attVal = rs.getInt(i + 1);
                        attsValsStr = attsValsStr + attVal;
                    }

                    DetectionSums result2 = computeCounts(pq.table2Name, queryTime, attsValsStr);
                    if (result2 != null) {
                        for (int k = 0; k < nAnomalies.length; k++) {
                            // TODO recheck
                            if (result2.nAnomalies[k] > 0
                                    && result1 != null && result1.nAnomalies[k] == 0) {
                                nAnomalies[k]++;
                            }
                        }
                        addAnomaly(pq.table2Name, attsValsStr, allAnomalies,
                                result2.nAnomalies);
                        recordCounts(pq.table2Name, result2, attsValsStr);
                    }
                }
            }

            stmt.close();
            rs.close();

            return nAnomalies;
        }
        return null;
    }

    private DetectionSums computeCounts(String tableName, Timestamp accessTime,
            String attsValsStr) throws SQLException {

//        if(tableName.equals("Treatment_Schedule") || tableName.equals("InvoiceNo")) {
//            return null;
//        }
        Statement stmt;
        stmt = conn_detection.createStatement();
        String query = "SELECT cnts, start_time, sums FROM "
                + tableName + " WHERE " + attsValsStr;
        ResultSet rs = stmt.executeQuery(query);

        // TODO rename counts to represent a timeseries
        ArrayList<Integer> countsArr = new ArrayList();
        ArrayList<Integer> sumsArr = new ArrayList();

        int nAnomalies[] = new int[Definitions.CAPTURE_FREQUENCIES.length];
        int thresholds[] = readThresholds(tableName);

        if (rs.next()) {
            String counts = rs.getString(1);
            Timestamp startTime = rs.getTimestamp(2);
            String sums = rs.getString(3);
            String[] sumsParts = sums.split(",");

            String[] oldCounts = null;
            if (counts != null) {
                oldCounts = counts.split(",");
            } else {
                startTime = new Timestamp(accessTime.getTime()
                        - (1000 * 60 * 60) * (Definitions.NUM_SLOTS - 1));
            }

            for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
                if (counts == null) {
                    countsArr.add(0);
                } else {
                    countsArr.add(new Integer(oldCounts[i]));
                }
            }

            for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
                sumsArr.add(new Integer(sumsParts[i]));
            }

            long diff = (accessTime.getTime() - startTime.getTime()) / (1000 * 60 * 60);
            Timestamp newTime;

            int nShifts = (int) (diff - Definitions.NUM_SLOTS + 1);
            if (nShifts > 0) {
                for (int i = 0; i < nShifts; i++) {
                    for (int j = 0; j < Definitions.CAPTURE_FREQUENCIES.length; j++) {
                        int firstEntryVal = countsArr.get(Definitions.NUM_SLOTS - Definitions.CAPTURE_FREQUENCIES[j]);
                        if (firstEntryVal > 0) {
                            sumsArr.set(j, sumsArr.get(j) - 1);
                        }
                    }
                    countsArr.remove(0);
                    countsArr.add(0);
                }

                newTime = new Timestamp(startTime.getTime()
                        + (diff - Definitions.NUM_SLOTS + 1) * (1000 * 60 * 60));
            } else {
                newTime = startTime;
            }

            // diff % NUM_SLOTS
            int index = (int) ((accessTime.getTime() - newTime.getTime()) / (1000 * 60 * 60));
            countsArr.set((int) index, countsArr.get((int) index) + 1);

            String newCounts = "";
            String newSums = "";
            for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
                if (index >= Definitions.NUM_SLOTS - Definitions.CAPTURE_FREQUENCIES[i]
                        && countsArr.get((int) index) == 1) {
                    sumsArr.set(i, sumsArr.get(i) + 1);
                }

                int s = sumsArr.get(i);

                if (i > 0) {
                    newSums = newSums + ",";
                }
                newSums = newSums + s;

                if (sumsArr.get(i) > thresholds[i]) {
                    nAnomalies[i]++;
//                    System.out.println(tableName + ", " 
//                            + Definitions.CAPTURE_FREQUENCIES[i] 
//                            + ": " + sumsArr.get(i) + " > " + thresholds[i]);
                }
            }

            for (int i = 0; i < Definitions.NUM_SLOTS; i++) {
                int c = countsArr.get(i);
                if (i > 0) {
                    newCounts = newCounts + ",";
                }
                newCounts = newCounts + c;
            }

            String newStart = newTime.toString();

            rs.close();
            stmt.close();

            return new DetectionSums(newCounts, newSums, newStart, nAnomalies);
        }

        return null;
    }

    public static String composeInitialTotals() {
        String initialStr = "";
        for (int i = 0; i < Definitions.CAPTURE_FREQUENCIES.length; i++) {
            if (i != 0) {
                initialStr = initialStr + ",";
            }
            initialStr = initialStr + "0";
        }
        return initialStr;
    }

    private int[] readThresholds(String tableName) throws SQLException {
        Statement stmt;
        String query = "SELECT interval_length, count FROM Profiles WHERE table_name = '"
                + tableName + "' order by interval_length";
        stmt = conn_traces.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(query);
        int[] thresholds = new int[Definitions.CAPTURE_FREQUENCIES.length];
        for (int i = 0; rs.next(); i++) {
            thresholds[i] = rs.getInt(2);
        }
        return thresholds;
    }

    private void recordCounts(String tableName, DetectionSums result, String attsValsStr) throws SQLException {
        String query = "UPDATE " + tableName
                + " SET cnts = '" + result.counts + "', "
                + "start_time = '" + result.startTime + "', sums = '"
                + result.sums + "' WHERE " + attsValsStr;
        DbUtils.executeUpdateOnDb(conn_detection, query);
    }

    private void addAnomaly(String tableName, String attsValsStr,
            HashMap<String, HashMap<String, ArrayList<Integer>>> allAnomalies,
            int[] nAnomalies) {
        HashMap<String, ArrayList<Integer>> tableAnomalies = allAnomalies.get(tableName);
        if (tableAnomalies == null) {
            tableAnomalies = new HashMap<>();
            allAnomalies.put(tableName, tableAnomalies);
        }
        ArrayList<Integer> rowAnomaly = tableAnomalies.get(attsValsStr);
        if (rowAnomaly == null) {
            rowAnomaly = new ArrayList<>();
            tableAnomalies.put(attsValsStr, rowAnomaly);
        } else {
            totalRowsInspected++;
        }
        for (int i = 0; i < nAnomalies.length; i++) {
            if (nAnomalies[i] > 0) {
                int intervalLength = Definitions.CAPTURE_FREQUENCIES[i];
                if (!rowAnomaly.contains(intervalLength)) {
                    rowAnomaly.add(intervalLength);
                    rowAnomalies[i]++;
                }
            }
        }
    }

    public void printRowAnomalies(PrintWriter out) {
        for (int i = 0; i < rowAnomalies.length; i++) {
            out.println(i + ": " + rowAnomalies[i]);
        }
        out.println("Total rows inspected: " + totalRowsInspected);
    }
    
    public void printTimeToAnomalyDetection() {
        for (int i : timeToAnomalyDetection) {
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println("Detected: " + timeToAnomalyDetection.size());
    }
}
