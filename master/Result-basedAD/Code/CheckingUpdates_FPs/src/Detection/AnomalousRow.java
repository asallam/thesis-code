/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Detection;

import java.sql.Timestamp;

/**
 *
 * @author asmaasallam
 */
public class AnomalousRow {
    String rowKey;
    Timestamp generatingQueryTime;
    boolean isTrueAnomaly;

    AnomalousRow(String attsValsStr, Timestamp queryTime, boolean isTrueAnomaly) {
        this.rowKey = attsValsStr;
        this.generatingQueryTime = queryTime;
        this.isTrueAnomaly = isTrueAnomaly;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof String) {
            return rowKey.equals((String)obj);
        } else if(obj instanceof AnomalousRow) {
            AnomalousRow input = (AnomalousRow)obj;
            return rowKey.equals(input.rowKey) 
                    && generatingQueryTime.equals(input.generatingQueryTime);
        }
        return false;
    }
    
}
