package Main;

import Detection.A_Detector;
import Utils.DbUtils;
import Utils.Definitions;
import checkingupdates.Training.SetupDB;
import checkingupdates.Training.Stats;
import checkingupdates.Training.Trainer;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asmaasallam
 */
public class FPs {

    public static void readQueriesInTable(String tableName) {
        try {
            int totalNumRows = 6105;
            float[] portionTraining = {0.95f, 0.9f, 0.8f, 0.7f, 0.6f, 0.5f, 0.4f, 0.3f, 0.2f, 0.1f};

            float[] portionToReconsider = {0.1f};
//{0.05f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f};
//{0.1f, 0.3f, 0.5f, 0.7f};
            //for (int s = 0; s < 5; s++) {
            for (int t = 0; t < portionTraining.length; t++) {

                for (int r = 0; r < portionToReconsider.length; r++) {
                    int nDetection = 0;

                    SetupDB setupDB = new SetupDB();
                    setupDB.resetTables();
                    setupDB.closeConnections();

                    Connection conn_traces;
                    conn_traces = DbUtils.connectToDb(Definitions.TRACES_DB_NAME);

                    Statement stmt;
                    String query = "SELECT sql, starttime from "
                            + tableName + " WHERE (lower(sql) LIKE 'select%') order by starttime ";
                    ResultSet rs;

                    stmt = conn_traces.createStatement();
                    rs = stmt.executeQuery(query);

                    Trainer trainer = new Trainer();
                    A_Detector detector = new A_Detector();
                    int nAnomalousQueries[] = new int[Definitions.CAPTURE_FREQUENCIES.length];

                    HashMap<String, HashMap<String, ArrayList<Integer>>> allAnomalies = new HashMap<>();
                    
                    Stats stats = null;
                    for (int i = 0; rs.next(); i++) {
                        String queryStr = rs.getString(1);
                        Timestamp queryTime = rs.getTimestamp(2);
                        if (i <= (int) (portionTraining[t] * totalNumRows)) {
                            trainer.addToTrainingQueries(queryStr, queryTime);
                            if (i == (int) (portionTraining[t] * totalNumRows)) {
                               // trainer.recordLast(queryTime, portionToReconsider[r]);
                                trainer.constructProfiles();
                                stats = trainer.computeStats();
                                trainer.closeConnections();
                            }
                        } else {
                            System.out.println("--" + queryStr);
                            int anomalies[] = detector.checkQuery(queryStr, queryTime, allAnomalies);
                            System.out.println("--------------------------------");
                            if (anomalies != null) {
                                nDetection++;
                                for (int k = 0; k < anomalies.length; k++) {
                                    if (anomalies[k] > 0) {
                                        nAnomalousQueries[k]++;
                                    }
                                }
                            }
                        }
                    }
                    detector.closeConnections();

                    int anomalousRows2 = 0;
                    int total = 0;
                    for (Map.Entry<String, HashMap<String, ArrayList<Integer>>> anomaly : allAnomalies.entrySet()) {
                        for (Map.Entry<String, ArrayList<Integer>> e : anomaly.getValue().entrySet()) {
                            ArrayList<Integer> anomalies = e.getValue();
                            if(anomalies != null) {
                                total++;
                                if(anomalies.size() > 1) {
                                    anomalousRows2++;
                                }
                            }
                        }
                    }
                    
                    
                    BufferedWriter bw = new BufferedWriter(new FileWriter("results.txt", true));
                    PrintWriter out = new PrintWriter(bw);

                    out.println("portionTraining: " + portionTraining[t]);
                    out.println("nDetection = " + nDetection);
                    out.println(stats.toString());
                    for (int i = 0; i < nAnomalousQueries.length; i++) {
                        out.println(nAnomalousQueries[i]);
                    }
                    
                    out.println("% = " + portionTraining[t] + 
                            " anomalousRows2 = " + anomalousRows2 + 
                            ", total = " + total);
                    
                    detector.printRowAnomalies(out);
                    out.println();
                    
                    out.close();
                    stmt.close();
                    rs.close();
                    DbUtils.closeConn(conn_traces);
                }
            }
            // }

        } catch (SQLException ex) {
            Logger.getLogger(Trainer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(FPs.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] strs) {
        FPs.readQueriesInTable("danger");
    }
}
