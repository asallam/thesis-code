/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 *
 * @author asmaasallam
 */
public class Parser {
    
    public static ParsedQuery parseQuery(Connection conn, String queryStr, 
            Timestamp queryTime) throws SQLException {
        String queryStrLower = queryStr.toLowerCase();

        boolean isAgg;
        isAgg = queryStrLower.contains("sum(") || queryStrLower.contains("count(")
                || queryStrLower.contains(" top ") || queryStrLower.contains("max(");

        if (queryStrLower.startsWith("select")) {

            if (queryStrLower.contains("master.dbo")) {
                // TODO
                return null;
            }
            
            ParsedQuery pq = new ParsedQuery();
            
            pq.isAgg = isAgg;

            String newQuery = "";
            String table1Name = "";
            String table2Name = "";

            int start = queryStrLower.indexOf(" from") + 6;
            int w = queryStrLower.indexOf("where");
            int o = queryStrLower.indexOf("order by");
            int end;
            if (w != -1) {
                end = w - 1;
            } else {
                if (o != -1) {
                    end = o - 1;
                } else {
                    end = queryStrLower.length() - 1;
                }
            }
            String tablesStr = queryStr.substring(start, end).trim();
            String[] tablesList = tablesStr.split(",");
            if (tablesList.length == 1) {
                // one table only
                if (tablesList[0].contains(" ")) {
                    // there is alias
//                    System.out.println("+ " + tablesList[0]);
                    String[] joinedParts = tablesList[0].split("Join");
                    switch (joinedParts.length) {
                        case 1:
                            // one table with an alias
                            // TODO
                            break;
                        case 2:
                            String firstTableParts[] = joinedParts[0].split(" ");
                            if (firstTableParts.length > 2) {
                                // there are aliases
                                String whereClause = "";
                                String alias1 = null;
                                String alias2 = null;

                                table1Name = firstTableParts[0];
                                alias1 = firstTableParts[1];
                                String secondParts[] = joinedParts[1].split("ON");
                                whereClause = whereClause + secondParts[1];

                                String secondTableParts[] = secondParts[0].split(" ");

                                int i;
                                for (i = 0; i < secondTableParts.length; i++) {
                                    if (!secondTableParts[i].isEmpty()) {
                                        table2Name = secondTableParts[i];
                                        break;
                                    }
                                }
                                for (i = i + 1; i < secondTableParts.length; i++) {
                                    if (!secondTableParts[i].isEmpty()) {
                                        alias2 = secondTableParts[i];
                                        break;
                                    }
                                }

                                String attsStr = "";

                                attsStr = attsStr + DbUtils.listPKs(conn, table1Name, alias1);

                                attsStr = attsStr + ", ";
                                attsStr = attsStr + DbUtils.listPKs(conn, table2Name, alias2);

                                newQuery = "Select ";
                                newQuery = newQuery + attsStr + " FROM "
                                        + table1Name + " " + alias1 + ", "
                                        + table2Name + " " + alias2 + " ";

                                if (w != -1) {
                                    whereClause = whereClause + " AND " + queryStr.substring(w + 6);
                                }
                                newQuery = newQuery + " WHERE " + whereClause;

                                //System.out.println(newQuery);
                            } else {
                                // TODO
                            }
                            break;

                        default:
                            // TODO
                            break;
                    }
                } else {
//                    System.out.println("- " + tablesList[0]);
                    newQuery = "SELECT ";
                    String attsStr;
                    table1Name = tablesList[0];
                    attsStr = DbUtils.listPKs(conn, table1Name, null);

                    newQuery = newQuery + attsStr + " FROM " + tablesStr + " ";
                    if (w != -1) {
                        String whereClause = queryStr.substring(w);
                        newQuery = newQuery + whereClause;
                    }
                    //System.out.println(newQuery);
                }
            } else {
//                System.out.println("2+ " + tablesStr);
                tablesList[0] = tablesList[0].trim();
                if (tablesList[0].contains(" ")) {
                    newQuery = "Select ";
                    String attsStr = "";

                    // there are aliases
                    for (int j = 0; j < tablesList.length; j++) {
                        tablesList[j] = tablesList[j].trim();
                        String alias = tablesList[j].substring(tablesList[j].indexOf(" "));
                        String tableName = tablesList[j].substring(0, tablesList[j].indexOf(" "));
                        if (j == 0) {
                            table1Name = tableName;
                        } else {
                            attsStr = attsStr + ", ";
                            table2Name = tableName;
                        }
                        attsStr = attsStr + DbUtils.listPKs(conn, tableName, alias);

                    }
                    newQuery = newQuery + attsStr + " FROM " + tablesStr + " ";
                    if (w != -1) {
                        String whereClause = queryStr.substring(w);
                        newQuery = newQuery + whereClause;
                    }
                    //System.out.println(newQuery);
                } else {
//                    System.out.println("-------------**");
                }
            }
            
            pq.newQuery = newQuery;
            pq.table1Name = table1Name;
            pq.table2Name = table2Name;
            
            return pq;
        }
        return null;
    }
}
