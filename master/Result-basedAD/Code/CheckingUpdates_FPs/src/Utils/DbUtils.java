package Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author asmaasallam
 */
public class DbUtils {

    public static Connection connectToDb(String DbName) throws ClassNotFoundException, SQLException {
        String connectionUrl = "jdbc:sqlserver://ASMA-PC\\SQLEXPRESS_2008:1433;"
                + "databaseName=" + DbName + ";integratedSecurity=true;";
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        return DriverManager.getConnection(connectionUrl);
    }
    
    public static void createDb(String DbName) throws ClassNotFoundException, SQLException {
        String connectionUrl = "jdbc:sqlserver://ASMA-PC\\SQLEXPRESS_2008:1433;integratedSecurity=true;";
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Connection conn = DriverManager.getConnection(connectionUrl);
        
        Statement stmt = conn.createStatement();
        String sql = "CREATE DATABASE '" + DbName + "'";
        stmt.executeUpdate(sql);
        stmt.close();
        conn.close();
    }
    
    public static void dropDb(String DbName) throws ClassNotFoundException, SQLException {
        String connectionUrl = "jdbc:sqlserver://ASMA-PC\\SQLEXPRESS_2008:1433;integratedSecurity=true;";
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Connection conn = DriverManager.getConnection(connectionUrl);
        
        Statement stmt = conn.createStatement();
        String sql = "DROP DATABASE '" + DbName + "'";
        stmt.executeUpdate(sql);
        stmt.close();
        conn.close();
    }

    public static void closeConn(Connection conn) throws SQLException {
        if (conn != null && !conn.isClosed()) {
            conn.close();
        }
    }

    public static ArrayList listTables(Connection conn, String DbName) throws SQLException {
        Statement stmt;
        String query = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' AND TABLE_CATALOG = '" + DbName + "' ";
        ArrayList<String> allTables;

        stmt = conn.createStatement();
        ResultSet rs;

        rs = stmt.executeQuery(query);
        allTables = new ArrayList<>();
        while (rs.next()) {
            String tableName = rs.getString("TABLE_NAME");
//            System.out.println(tableName);
            allTables.add(tableName);
        }
        stmt.close();

        return allTables;
    }

    public static HashMap findPKs(Connection conn, ArrayList<String> allTables) throws SQLException {
        HashMap tablesPKs;
        tablesPKs = new HashMap<>();
        ArrayList<Attribute> atts;
        for (String tableName : allTables) {
            atts = findTablePKs(conn, tableName);
            tablesPKs.put(tableName, atts);
        }
        return tablesPKs;
    }

    public static ArrayList<Attribute> findTablePKs(Connection conn, String tableName) throws SQLException {

        Statement stmt1;
        String queryForNames = "SELECT column_name "
                + "FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC "
                + "INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU "
                + "ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY' AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME AND "
                + "KU.table_name = '" + tableName + "'";
        stmt1 = conn.createStatement();
        ResultSet rs1;

        rs1 = stmt1.executeQuery(queryForNames);

        ArrayList<Attribute> PKs;
        PKs = new ArrayList<>();
        while (rs1.next()) {
            String attName = rs1.getString("column_name");

            String queryForType = "SELECT DATA_TYPE\n"
                    + "FROM INFORMATION_SCHEMA.COLUMNS\n"
                    + "WHERE TABLE_NAME = '" + tableName + "' AND COLUMN_NAME = '" + attName + "'";

            Statement stmt2;
            stmt2 = conn.createStatement();
            ResultSet rs2;
            rs2 = stmt2.executeQuery(queryForType);
            rs2.next();
            String attType = rs2.getString("DATA_TYPE");

            PKs.add(new Attribute(attName, attType));

            stmt2.close();
            rs2.close();
        }

        stmt1.close();
        rs1.close();

        return PKs;
    }
    
    public static int numRows(String tableName, Connection conn) throws SQLException {
        Statement stmt;
        String query = "SELECT count(*) FROM " + tableName;
        stmt = conn.createStatement();
        
        ResultSet rs;
        rs = stmt.executeQuery(query);
        
        rs.next();
        return rs.getInt(1);
        
    }
    
    public static int maxID(String tableName, Connection conn) throws SQLException {
        Statement stmt;
        String query = "SELECT max(ID) FROM " + tableName;
        stmt = conn.createStatement();
        
        ResultSet rs;
        rs = stmt.executeQuery(query);
        
        rs.next();
        return rs.getInt(1);
        
    }
    
    public static void executeUpdateOnDb(Connection conn, String query) throws SQLException {
//        System.out.println(query);
        Statement stmt;
        stmt = conn.createStatement();
        stmt.executeUpdate(query);
        stmt.close();
    }
    
    public static String listPKs(Connection conn, String tableName, String alias) throws SQLException {
        String attsStr = "";
        ArrayList<Attribute> atts = DbUtils.findTablePKs(conn, tableName);

        for (int i = 0; i < atts.size(); i++) {
            if (i != 0) {
                attsStr = attsStr + ", ";
            }
            if (alias != null && !alias.isEmpty()) {
                attsStr = attsStr + alias + "." + atts.get(i).getName();
            } else {
                attsStr = attsStr + atts.get(i).getName();
            }
        }

        return attsStr;
    }
    
    public static boolean tableExists(Connection conn, String tableName) throws SQLException {
        String query = "SELECT * FROM INFORMATION_SCHEMA.TABLES "
                + "WHERE TABLE_NAME = '" + tableName + "'";

        Statement stmt;
        stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(query);
        boolean tableFound = rs.next();
        rs.close();
        stmt.close();
        return tableFound;
    }
    
    public static int countAccessed(Connection conn, String tableName) throws SQLException {
        String query = "SELECT COUNT(*) FROM " + tableName + " WHERE cnts IS NOT NULL";
        Statement stmt;
        stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(query);
        int count = 0;
        if(rs.next()) {
            count = rs.getInt(1);
        } 
        rs.close();
        stmt.close();
        return count;
    }
    
    public static int maxRowAccesses(Connection conn, String tableName) throws SQLException {
        String query = "SELECT count FROM Profiles WHERE table_name = '" + tableName + "' and interval_length = 1";
        Statement stmt;
        stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(query);
        int count = 0;
        if(rs.next()) {
            count = rs.getInt(1);
        } 
        rs.close();
        stmt.close();
        return count;
    }
    
    public static float avgRowAccesses(Connection conn, String tableName) throws SQLException {
        String query = "SELECT avg(count) FROM counts WHERE table_name = '" + tableName + "' and interval_length = 1";
        Statement stmt;
        stmt = conn.createStatement();
        ResultSet rs;
        rs = stmt.executeQuery(query);
        float avg = 0;
        if(rs.next()) {
            avg = rs.getFloat(1);
        } 
        rs.close();
        stmt.close();
        return avg;
    }
    
}
