/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.Timestamp;

/**
 *
 * @author asmaasallam
 */
public class Query {
    public String str;
    public Timestamp time;
    public String template;

    public void setStr(String str) {
        this.str = str;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getStr() {
        return str;
    }

    public Timestamp getTime() {
        return time;
    }
    
    public Query(String str, Timestamp time) {
        this.str = str;
        this.time = time;
    }
    
}
