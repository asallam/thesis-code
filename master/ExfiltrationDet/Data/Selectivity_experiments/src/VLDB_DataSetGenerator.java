import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.management.relation.RoleStatus;


public class VLDB_DataSetGenerator {

	double s = 0.5;
	int numTrainingRecsTotal = 2000;
	int numRoles = 9;
	
	Hashtable<String, ArrayList<Integer>> queriesRoles;
	public VLDB_DataSetGenerator() {
		queriesRoles = new Hashtable<String, ArrayList<Integer>>();
	}
	
	public void addQueryToRole(String query, int roleID) {
		ArrayList<Integer> roles = queriesRoles.get(query);
		if(roles == null) {
			roles = new ArrayList<Integer>();
			queriesRoles.put(query, roles);
		}
		if(!roles.contains(roleID)) {
			roles.add(roleID);
		}
	}
	
	public void findExclusiveQueries() {
		Iterator<Map.Entry<String, ArrayList<Integer>>> it = queriesRoles.entrySet().iterator();

		while (it.hasNext()) {
			Map.Entry<String, ArrayList<Integer>> entry = it.next();
			String query = entry.getKey();
			ArrayList<Integer> queryIssuers = entry.getValue();
			if (queryIssuers.size() == 1) {
				int roleID = queryIssuers.get(0);
				for(int i = 0; i < numRoles; i++) {
					if(roleID != i) {
						System.out.println(i + "," + query);
					}
				}
			}
		}
	}
	
	public void writeTrainingQueries(int roleID, ArrayList<String> trainingQueries) {
		try {
			String filePath = "VLDB/" + s + "/training/";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + ";\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// generateR0_3(0);
	// generateR0_3(25);
	// generateR0_3(50);
	// generateR0_3 (75);
	public void generateR0_3(int roleID, int start) {
		int numTables = 10;
		int numCols = 20;
		int numTrainingRecs = numTrainingRecsTotal/9;
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zt = new ZipfGenerator(numTables, s);
		ZipfGenerator zc = new ZipfGenerator(numCols, s);
		
		for (int i = 1; i <= numTables; i++) {
			double prob = zt.getProbability(i);
			int currNumTrainingRecs = (int) (numTrainingRecs * prob);
			int tableID = i - 1 + start;
			for(int j = 1; j <= numCols; j++) {
				int colID = j - 1;
				int currNumTrainingRecs_Col = (int) (currNumTrainingRecs * zc.getProbability(j));
				for(int k = 0; k < currNumTrainingRecs_Col; k++) {
					String query = "SELECT c" + colID + " FROM table" + tableID;
					String whereClause;
					if(prob > 0.66) {
						whereClause = "";
					} else if (prob > 0.33) {
						whereClause = " WHERE c0 <= 5";
					} else {
						whereClause = " WHERE c0 < 2";
					}
					query = query + whereClause;
//					System.out.println(roleID + "," + query);
					trainingQueries.add(query);
					addQueryToRole(query, roleID);
				}
			}
		}
		writeTrainingQueries(roleID, trainingQueries);
	}
	
	public void generateR4_5(int roleID, int start) {
		int numTables = 20;
		int numCols = 20;
		int numTrainingRecs = numTrainingRecsTotal/9;
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zt = new ZipfGenerator(numTables, s);
		ZipfGenerator zc = new ZipfGenerator(numCols, s);
		
		for (int i = 1; i <= numTables; i++) {
			double prob = zt.getProbability( numTables - i + 1);
			int currNumTrainingRecs = (int) (numTrainingRecs * prob);
			int tableID = i - 1 + start;
			for(int j = 1; j <= numCols; j++) {
				int colID = j - 1;
				int currNumTrainingRecs_Col = (int) (currNumTrainingRecs * zc.getProbability(numCols - j + 1));
				for(int k = 0; k < currNumTrainingRecs_Col; k++) {
					String query = "SELECT c" + colID + " FROM table" + tableID;
					String whereClause;
					if(prob > 0.66) {
						whereClause = "";
					} else if (prob > 0.33) {
						whereClause = " WHERE c0 <= 5";
					} else {
						whereClause = " WHERE c0 < 2";
					}
					query = query + whereClause;
//					System.out.println(roleID + "," + query);
					trainingQueries.add(query);
					addQueryToRole(query, roleID);
				}
			}
		}
		writeTrainingQueries(roleID, trainingQueries);
	}
	
	public void generateR6(int roleID, int start) {
		int numTables = 40;
		int numCols = 20;
		int numTrainingRecs = numTrainingRecsTotal/9;
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zt = new ZipfGenerator(numTables, s);
		ZipfGenerator zc = new ZipfGenerator(numCols, s);
		
		for (int i = 1; i <= numTables; i++) {
			double prob = zt.getProbability(i);
			int currNumTrainingRecs = (int) (numTrainingRecs * prob);
			int tableID = i - 1 + start;
			for(int j = 1; j <= numCols; j++) {
				int colID = j - 1;
				int currNumTrainingRecs_Col = (int) (currNumTrainingRecs * zc.getProbability(j));
				for(int k = 0; k < currNumTrainingRecs_Col; k++) {
					String query = "SELECT c" + colID + " FROM table" + tableID;
					String whereClause;
					if(prob > 0.66) {
						whereClause = "";
					} else if (prob > 0.33) {
						whereClause = " WHERE c0 <= 5";
					} else {
						whereClause = " WHERE c0 < 2";
					}
					query = query + whereClause;
//					System.out.println(roleID + "," + query);
					trainingQueries.add(query);
					addQueryToRole(query, roleID);
				}
			}
		}
		writeTrainingQueries(roleID, trainingQueries);
	}
	
	public void generateR7(int roleID, int start) {
		int numTables = 40;
		int numCols = 20;
		int numTrainingRecs = numTrainingRecsTotal/9;
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zt = new ZipfGenerator(numTables, s);
		ZipfGenerator zc = new ZipfGenerator(numCols, s);
		
		for (int i = 1; i <= numTables; i++) {
			double prob = zt.getProbability( numTables - i + 1);
			int currNumTrainingRecs = (int) (numTrainingRecs * prob);
			int tableID = i - 1 + start;
			for(int j = 1; j <= numCols; j++) {
				int colID = j - 1;
				int currNumTrainingRecs_Col = (int) (currNumTrainingRecs * zc.getProbability(numCols - j + 1));
				for(int k = 0; k < currNumTrainingRecs_Col; k++) {
					String query = "SELECT c" + colID + " FROM table" + tableID;
					String whereClause;
					if(prob > 0.66) {
						whereClause = "";
					} else if (prob > 0.33) {
						whereClause = " WHERE c0 <= 5";
					} else {
						whereClause = " WHERE c0 < 2";
					}
					query = query + whereClause;
//					System.out.println(roleID + "," + query);
					trainingQueries.add(query);
					addQueryToRole(query, roleID);
				}
			}
		}
		writeTrainingQueries(roleID, trainingQueries);
	}

	public void generateR8(int roleID) {
		int numTables = 40;
		int numCols = 20;
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		int numTrainingRecs = numTrainingRecsTotal/9;
		int trainingPerTable = numTrainingRecs / numTables;
		int trainingPerCol = trainingPerTable / numCols;
		
		for(int i = 0; i < numTables; i++) {
			for(int j = 0; j < numCols; j++) {
				for(int k = 0; k < trainingPerCol; k++) {
					String query = "SELECT c" + j + " FROM table" + i;
					String whereClause;
					if(i > 66) {
						whereClause = "";
					} else if (i > 33) {
						whereClause = " WHERE c0 <= 5";
					} else {
						whereClause = " WHERE c0 < 2";
					}
					query = query + whereClause;
//					System.out.println(roleID + "," + query);
					trainingQueries.add(query);
					addQueryToRole(query, roleID);
				}
			}
		}
		writeTrainingQueries(roleID, trainingQueries);
	}
	
	
	public static void main(String[] args) {
		VLDB_DataSetGenerator g = new VLDB_DataSetGenerator();
		
		g.generateR0_3(0, 0);
		g.generateR0_3(1, 10);
		g.generateR0_3(2, 20);
		g.generateR0_3 (3, 30);
		
		g.generateR4_5(4, 0);
		g.generateR4_5(5, 0);
		
		g.generateR6(6, 0);
		
		g.generateR7(7, 0);
		
		g.generateR8(8);
		
		g.findExclusiveQueries();
	}
	
}
