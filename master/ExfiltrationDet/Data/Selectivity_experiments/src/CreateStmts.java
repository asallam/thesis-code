
public class CreateStmts {
	public static void main(String[] args) {
		
		// 100 tables
		int numTables = 20;
		int numColsPerTable = 10;
		
		for(int i = 0; i < 100; i++) {
			System.out.println("DROP TABLE if exists table"+ i + ";");
		}
		
//		for(int i = 0; i < 100; i++) {
//			System.out.println("DROP ROLE if exists role"+ i + ";");
//		}
//		
//		for(int i = 0; i < 100; i++) {
//			System.out.println("DROP ROLE if exists user"+ i + ";");
//		}
//		
//		// create tables

//		
		for(int i = 0; i < numTables; i++) {
			String query = "CREATE TABLE table" + i + " (";			
			String colInfo = "";
			for(int j = 0; j < numColsPerTable; j++) {
				
				if (j != 0) {
					colInfo = colInfo + ", ";
				}
				colInfo = colInfo + "c" + j + " integer";
			}

			query = query + colInfo + ");";
			
			System.out.println(query);
			
			String analyzeQuery = "ANALYZE table" + i + "(";
			for(int y = 0; y < numColsPerTable; y++) {
				if(y != 0) {
					analyzeQuery = analyzeQuery + ", ";
				}
				analyzeQuery = analyzeQuery + "c" + y;
			}
			analyzeQuery = analyzeQuery + ");";
			
			for(int k = 0; k < 10; k++) {
				String insertQuery = "INSERT INTO table"+ i + " VALUES (";
				for(int c = 0; c < numColsPerTable; c++) {
					if(c != 0) {
						insertQuery = insertQuery + ", ";
					}
					insertQuery = insertQuery + k;
				}
				insertQuery = insertQuery + ");";
				System.out.println(insertQuery);
			}
			
			System.out.println(analyzeQuery);
			
		}
//		
		// create roles
		// 10 roles
		// 10 users
		int numRoles = 10;
		for(int k = 0; k < numRoles; k++) {
//			String createRoleQuery = "CREATE ROLE role" + k + ";";
//			String createUserQuery = "CREATE USER user" + k + ";";
//			String grantRoleToUserQuery  =  "GRANT role" + k + " TO user" + k + ";";
			
//			System.out.println(createRoleQuery);
//			System.out.println(createUserQuery);
//			System.out.println(grantRoleToUserQuery);
			
			for(int j = 0; j < numTables; j++) {
				String grantQuery = "GRANT select, insert, delete, update on table" + j + " TO role" + k + ";";
				System.out.println(grantQuery);
			}
		}
	}
}
