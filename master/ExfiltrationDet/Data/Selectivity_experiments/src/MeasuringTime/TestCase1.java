package MeasuringTime;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

// testing time vs number of atts

public class TestCase1 {

	int numTrainingTuples = 1500; 
	int numRoles = 30;
	// # of atts = 10, 50, 100, 500, 1000, 2000
	int[] numTables = {2, 10, 20, 50};
	
	
	
	public void generate() {
		for(int it = 0; it < numTables.length; it++) {
			String dir = "";
			ArrayList<String> trainingQueries = new ArrayList<String>();
			for(int t = 0; t < numTables[it]; t++) {
				trainingQueries.add("SELECT * from table" + t + ";");
			}
			
			for(int r = 0; r < numRoles; r++) {
				// create the training file
				try {
				
					String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\MeasuringTime\\atts\\nt" + numTables[it] + "\\";
					BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + r + ".txt"));
					out.write("C\n");
					out.write("user" + r + "\n");
					out.write("c\n");
					out.write("Q\n");
					for(int i = 0; i < trainingQueries.size(); i++) {
						out.write(trainingQueries.get(i) + "\n");
					}
					out.write("q");
				    out.close();
				} catch (IOException e) { e.printStackTrace();}
				
				}
		}
		
	}
	
	public static void main(String[] args) {
		TestCase1 tc1 = new TestCase1();
		tc1.generate();
	}
	
}
