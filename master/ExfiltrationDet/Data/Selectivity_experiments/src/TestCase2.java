import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class TestCase2 {
	
	//same tables, different cols
	public static void main(String[] args) {
		int numTrainingRecs = 2000;
		int numRoles = 10;
		int numTables = 20;
		int numColsPerTable = 10;
		
		int numTrainingRecsPerRole = numTrainingRecs / numRoles;
		
		int numColsPerTablePerRole = numColsPerTable / numRoles;
		
		int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / numTables;
		
		String detectionFilePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentCols\\" + numTrainingRecs;
		
		ArrayList<String> detectionQueries_0 = new ArrayList<String>();
		ArrayList<String> detectionQueries = new ArrayList<String>();
		
		for(int r = 0; r < numRoles; r++) {
			boolean detectionQueriesIDs[] = new boolean[numTrainingRecsPerRole];
			for (int i = 0; i < detectionQueriesIDs.length; i++) {
				detectionQueriesIDs[i] = false;
			}
			
			Random rGen = new Random();
			// now choose which queries in training and which in detection
			for(int i = 0; i < numTrainingRecsPerRole/4; i++) {
				int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsPerRole;
				detectionQueriesIDs[rNumber] = true;
			}
			
			ArrayList<String> trainingQueries = new ArrayList<String>();
			
			int currQueryID = 0;
			
			for(int t = 0; t < numTables; t++) {
				String query = "SELECT ";
				int startingCol = r * numColsPerTablePerRole;
				for(int c = startingCol; c < startingCol + numColsPerTablePerRole; c++) {
					if(c != startingCol) {
						query = query + ", ";
					}
					query = query + "c" + c;
				}
				query = query + " FROM table" + t;
				query = query + ";";
			
				for(int n = 0; n < numTrainingRecsPerRolePerTable; n++) {
					if(detectionQueriesIDs[currQueryID]) {
						detectionQueries.add(r + "," + query);
						for(int r0 = 0; r0 < numRoles; r0++) {
							if(r == r0) continue; 
							detectionQueries_0.add(r0 + "," + query);
						}
					} else {
						trainingQueries.add(query);
					}
					currQueryID++;
				}
			}
	
			try {
				String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentCols\\" + numTrainingRecs + "\\training\\";
				BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + r + ".txt"));
				out.write("C\n");
				out.write("user" + r + "\n");
				out.write("c\n");
				out.write("Q\n");
				for(int i = 0; i < trainingQueries.size(); i++) {
					out.write(trainingQueries.get(i) + "\n");
				}
				out.write("q");
			    out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentCols\\" + numTrainingRecs + "\\detection.txt";
			BufferedWriter outDetection = new BufferedWriter(new FileWriter(filePath));
//			outDetection.write("C\n");
//			outDetection.write("user" + r + "\n");
//			outDetection.write("c\n");
//			outDetection.write("Q\n");
			for(int i = 0; i < detectionQueries.size(); i++) {
				outDetection.write(detectionQueries.get(i) + "\n");
			}
		    outDetection.close();
		    
		    String filePath_0 = "C:\\Users\\asmaa\\Desktop\\testData\\differentCols\\" + numTrainingRecs + "\\detection_0.txt";
			BufferedWriter outDetection0 = new BufferedWriter(new FileWriter(filePath_0));
			for(int i = 0; i < detectionQueries_0.size(); i++) {
				outDetection0.write(detectionQueries_0.get(i) + "\n");
			}
		    outDetection0.close();
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
		

	}
}
