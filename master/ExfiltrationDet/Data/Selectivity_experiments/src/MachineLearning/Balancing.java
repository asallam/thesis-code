package MachineLearning;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Balancing {

	int numTables = 20;
	int numColsPerTable = 5;
	int numRoles = 5;
	int numTrainingRecs = 100; 
	int numTrainingRecsPerRole = numTrainingRecs / numRoles;
	int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / numTables;
	int numTrainingRecsPerRolePerTablePerCol = numTrainingRecsPerRolePerTable / numColsPerTable;
	
	float percentageOfMax = 0.7f;
	
	// roles access different columns
	int numColsPerTablePerRole = numColsPerTable / numRoles;
	
	public void generateRole(int roleID) {
		
		int currNumTrainingRecs = (int) (numTrainingRecsPerRole * percentageOfMax);
		
		// IMP: role0 has the maximum number of records
		if(roleID == 0) {
			return; 
			// the purpose is to check other roles now role0
			//currNumTrainingRecs = numTrainingRecsPerRole;
		}
		
		int numDetectionRecs = currNumTrainingRecs / 4;
		
		boolean detectionQueriesIDs[] = new boolean[currNumTrainingRecs];
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}
		
		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for(int i = 0; i < numDetectionRecs; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % currNumTrainingRecs;
			detectionQueriesIDs[rNumber] = true;
		}
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		ArrayList<String> detectionQueries = new ArrayList<String>();

		int currQueryID = 0;
		
		for(int t = 0; t < numTables; t++) {
			
			String query = "SELECT ";
			int startingCol = roleID * numColsPerTablePerRole;
			for(int c = startingCol; c < startingCol + numColsPerTablePerRole; c++) {
				if(currQueryID >= currNumTrainingRecs) {
					break;
				}
				if(c != startingCol) {
					query = query + ", ";
				}
				query = query + "c" + c;
			}
			
			if(currQueryID >= currNumTrainingRecs) {
				break;
			}
			
			query = query + " FROM table" + t;
			query = query + ";";
		
			for(int n = 0; n < numTrainingRecsPerRolePerTable; n++) {
				if(detectionQueriesIDs[currQueryID]) {
					detectionQueries.add(query);
				} else {
					trainingQueries.add(query);
				}
				currQueryID++;
			}
		}
		
		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\Balancing\\" + numTrainingRecs + "\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < detectionQueries.size(); i++) {
			System.out.println(roleID + "," + detectionQueries.get(i));
		}

	}
	
	public void generateAll() {
		for(int i = 0; i < numRoles; i++) {
			generateRole(i);
		}
	}
	
	public static void main(String[] args) {
		Balancing fs = new Balancing();
		fs.generateAll();
	}
	
}
