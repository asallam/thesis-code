import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


public class TestCase1 {
	public static void main(String[] args) {
		// roles are accessing different tables
		// 10 roles
		
		int numTrainingRecs = 200;
		int numRoles = 10;
		int numTables = 20;
		
		int numTrainingRecsPerRole = numTrainingRecs / numRoles;
		int numTabsPerRole = numTables / numRoles;
		int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / numTabsPerRole;
		
		int currQueryID = 0;
		ArrayList<String> detectionQueries = new ArrayList<String>();
		ArrayList<String> detectionQueries_0 = new ArrayList<String>();
		
		for(int r = 0; r < numRoles; r++) {
			
			boolean detectionQueriesIDs[] = new boolean[numTrainingRecsPerRole];
			for (int i = 0; i < detectionQueriesIDs.length; i++) {
				detectionQueriesIDs[i] = false;
			}
			
			Random rGen = new Random();
			// now choose which queries in training and which in detection
			for(int i = 0; i < numTrainingRecsPerRole/4; i++) {
				int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsPerRole;
				detectionQueriesIDs[rNumber] = true;
			}
			
			ArrayList<String> trainingQueries = new ArrayList<String>();
			
			currQueryID = 0;
			
			for(int t = 0; t < numTabsPerRole; t++) {
				int tabID = r * numTabsPerRole + t;
				for (int i = 0; i < numTrainingRecsPerRolePerTable; i++) {
					String query = "SELECT * from table" + tabID + ";"; 
					
					//System.out.println(query);
					if(detectionQueriesIDs[currQueryID]) {
						detectionQueries.add(r + "," + query);
						for(int r0 = 0; r0 < numRoles; r0++) {
							if(r == r0) continue; 
							detectionQueries_0.add(r0 + "," + query);
						}
					} else {
						trainingQueries.add(query);
					}
					currQueryID++;
				}
			}
			try {
				String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentTables\\" + numTrainingRecs + "\\training\\";
				BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + r + ".txt"));
				out.write("C\n");
				out.write("user" + r + "\n");
				out.write("c\n");
				out.write("Q\n");
				for(int i = 0; i < trainingQueries.size(); i++) {
					out.write(trainingQueries.get(i) + "\n");
				}
				out.write("q");
			    out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentTables\\" + numTrainingRecs + "\\detection.txt";
			BufferedWriter outDetection = new BufferedWriter(new FileWriter(filePath));
			for(int i = 0; i < detectionQueries.size(); i++) {
				outDetection.write(detectionQueries.get(i) + "\n");
			}
		    outDetection.close();
		    
		    String filePath_0 = "C:\\Users\\asmaa\\Desktop\\testData\\differentTables\\" + numTrainingRecs + "\\detection_0.txt";
			BufferedWriter outDetection0 = new BufferedWriter(new FileWriter(filePath_0));
			for(int i = 0; i < detectionQueries_0.size(); i++) {
				outDetection0.write(detectionQueries_0.get(i) + "\n");
			}
		    outDetection0.close();
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}

		





/*			try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentTables\\" + numTrainingRecs + "\\detection\\";
			BufferedWriter out1 = new BufferedWriter(new FileWriter(filePath + "role" + r + ".txt"));
			out1.write("C\n");
			out1.write("user" + r + "\n");
			out1.write("c\n");
			out1.write("Q\n");
			for(int i = 0; i < detectionQueries.size(); i++) {
				out1.write(detectionQueries.get(i) + "\n");
			}
			out1.write("q");
		    out1.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			for(int ar = 0; ar < numRoles; ar++) {
				if(ar == r) continue;
				String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentTables\\" + numTrainingRecs + "\\detection_-\\";
				BufferedWriter out2 = new BufferedWriter(new FileWriter(filePath + "role" + r + "_" + ar + ".txt"));
				out2.write("C\n");
				out2.write("user" + ar + "\n");
				out2.write("c\n");
				out2.write("Q\n");
				for(int i = 0; i < detectionQueries.size(); i++) {
					out2.write(detectionQueries.get(i) + "\n");
				}
				out2.write("q");
			    out2.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
*/