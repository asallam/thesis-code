import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class TestCase3 {

	// same tables and same columns with different selectivity per table

	public static void main(String[] args) {

		int numTrainingRecs = 1000;
		int numRoles = 10;
		int numTables = 20;

		int numTrainingRecsPerRole = numTrainingRecs / numRoles;
		
		ArrayList<String> detectionQueries = new ArrayList<String>();
		ArrayList<String> detectionQueries_0 = new ArrayList<String>();
		
		for (int r = 0; r < numRoles; r++) {
			getRoleQueries(r, numRoles, numTrainingRecsPerRole, numTables,
					numTrainingRecs, detectionQueries, detectionQueries_0);
		}
//		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentSels\\" + numTrainingRecs + "\\detection.txt";
			BufferedWriter outDetection = new BufferedWriter(new FileWriter(filePath));
			for(int i = 0; i < detectionQueries.size(); i++) {
				outDetection.write(detectionQueries.get(i) + "\n");
			}
		    outDetection.close();
		    
		    String filePath_0 = "C:\\Users\\asmaa\\Desktop\\testData\\differentSels\\" + numTrainingRecs + "\\detection_0.txt";
			BufferedWriter outDetection0 = new BufferedWriter(new FileWriter(filePath_0));
			for(int i = 0; i < detectionQueries_0.size(); i++) {
				outDetection0.write(detectionQueries_0.get(i) + "\n");
			}
		    outDetection0.close();
		    
		} catch (IOException e) {
			e.printStackTrace();
		}
//		

	}

	private static void getRoleQueries(int r, int numRoles, int numTrainingRecsPerRole,
			int numTables, int numTrainingRecs, 
			ArrayList<String> detectionQueries, ArrayList<String> detectionQueries_0) {
		boolean detectionQueriesIDs[] = new boolean[numTrainingRecsPerRole];
		int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / 3;
		
		//detectionQueries.clear();
		
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}

		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for (int i = 0; i < numTrainingRecsPerRole / 4; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsPerRole;
			detectionQueriesIDs[rNumber] = true;
		}

		ArrayList<String> trainingQueries = new ArrayList<String>();

		int start = r - r % 3;
		int queryID = 0;
		for (int i = start; i < start + 3; i++) {
			for(int t = 0; t < numTrainingRecsPerRolePerTable; t++) {
				String query = "";
				int tableID = i;
				if (r % 3 == 0) {
					query = "SELECT * from table" + tableID + " WHERE c1 < 2;";
				} else if (r % 3 == 1) {
					query = "SELECT * from table" + tableID + " WHERE c1 < 5;";
				} else if (r % 3 == 2) {
					query = "SELECT * from table" + tableID + " WHERE c1 < 10;";
				}

				if (detectionQueriesIDs[queryID]) {
					detectionQueries.add(r + "," + query);
					for(int r0 = 0; r0 < numRoles; r0++) {
						if(r == r0) continue; 
						detectionQueries_0.add(r0 + "," + query);
					}
				} else {
					trainingQueries.add(query);
				}
				queryID++;
			}
		}

		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentSels\\"
					+ numTrainingRecs + "\\training\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath
					+ "role" + r + ".txt"));
			out.write("C\n");
			out.write("user" + r + "\n");
			out.write("c\n");
			out.write("Q\n");
			for (int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + ";\n");
			}
			out.write("q");
			out.close();
			
//			filePath = "C:\\Users\\asmaa\\Desktop\\testData\\differentSels\\" + numTrainingRecs + "\\detection" + r + ".txt";
//			BufferedWriter outDetection = new BufferedWriter(new FileWriter(filePath));
//			outDetection.write("C\n");
//			outDetection.write("user" + r + "\n");
//			outDetection.write("c\n");
//			outDetection.write("Q\n");
//			for(int i = 0; i < detectionQueries.size(); i++) {
//				outDetection.write(detectionQueries.get(i) + "\n");
//			}
//			outDetection.write("q");
//		    outDetection.close();
			
		} catch (IOException e) {
		}

		

	}

}
