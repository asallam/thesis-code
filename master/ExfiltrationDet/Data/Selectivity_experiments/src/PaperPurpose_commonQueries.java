import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class PaperPurpose_commonQueries {

	int numRoles = 5;
	int numColsPerTable = 5;
	int numTables = 10;

	float portion;

	ArrayList<String> allDetectionQueries = new ArrayList<String>();
	ArrayList<String> detection0 = new ArrayList<String>();

	public void generateRole(int roleID, int numTrainingRecsForRole, float s) {

		boolean detectionQueriesIDs[] = new boolean[numTrainingRecsForRole];
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}

		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for (int i = 0; i < numTrainingRecsForRole / 4; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsForRole;
			detectionQueriesIDs[rNumber] = true;
		}

		ArrayList<String> trainingQueries = new ArrayList<String>();

		int currQueryID = 0;
		int start = (int) (roleID * numTables * (1 - portion) / numRoles);
		int nT = (int) (numTables * (1 - portion) / numRoles);

		int numTablesPerRole = (int) (numTables * (1 - portion) / numRoles);
		String[] cols = { "c0"};
		
		// common queries among the roles
		int start2 = (int) (numTables * (1 - portion));
		int nTimesPerTable = numTrainingRecsForRole / (numTablesPerRole + numTables - start2);
		
		int nTimesPerCol = nTimesPerTable / cols.length;
		
		for (int i = start; i < start + nT; i++) {
			for (int c = 0; c < cols.length; c++) {
				for (int t = 0; t < nTimesPerCol; t++) {
					String query = "SELECT " + cols[c] + " FROM table" + i
							+ ";";
					if (detectionQueriesIDs[currQueryID
							% numTrainingRecsForRole]) {
						allDetectionQueries.add(roleID + "," + query);
						for (int r0 = 0; r0 < numRoles; r0++) {
							if (r0 == roleID)
								continue;
							else {
								detection0.add(r0 + "," + query);
							}
						}
					} else {
						trainingQueries.add(query);
					}
					currQueryID++;
				}
			}
		}
		
		for (int i = start2; i < numTables; i++) {
			for (int j = 0; j < nTimesPerTable; j++) {
				String query = "SELECT * from table" + i + ";";
				trainingQueries.add(query);
			}
		}

		try {
			String currDirectory = "/home/asma/pp/" + portion + "/" + s + "/";
			BufferedWriter out = new BufferedWriter(new FileWriter(
					currDirectory + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for (int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void generateAll() {

		float s = 2.0f;
		portion = 0.75f;
		generateRole(0, 702, s);
		generateRole(1, 176, s);
		generateRole(2, 78, s);
		generateRole(3, 44, s);
		generateRole(4, 28, s);

		// 0.01: 252,250,249,249,248
		// 0.5: 359,254,207,180,161
		// 2.0: 702,176,78,44,28

		try {
			String currDirectory = "/home/asma/pp/" + portion + "/" + s
					+ "/detection.txt";
			BufferedWriter out = new BufferedWriter(new FileWriter(
					currDirectory));
			for (int i = 0; i < allDetectionQueries.size(); i++) {
				out.write(allDetectionQueries.get(i) + "\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			String currDirectory = "/home/asma/pp/" + portion + "/" + s
					+ "/detection0.txt";
			BufferedWriter out = new BufferedWriter(new FileWriter(
					currDirectory));
			for (int i = 0; i < detection0.size(); i++) {
				out.write(detection0.get(i) + "\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) {
		PaperPurpose_commonQueries pp = new PaperPurpose_commonQueries();
		pp.generateAll();

	}

}
