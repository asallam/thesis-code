import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class PaperPurpose {
	
	int numRoles = 5;
	int numColsPerTable = 5;
	int numTables = 10;
	int numTuples = 1000;

	public void generateRi(int roleID) {
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		String[] cols = {"c0", "c1", "c2", "c3", "c4"};
		
		int numTablesPerRole = numTables / numRoles;
		int nTimesPerCol = numTuples / numTablesPerRole / cols.length;
		
		int startTableID = roleID * 2;
		
		for(int k = 0; k < numTablesPerRole; k++) {
			int tableID = startTableID + k;
			for(int j = 0; j < cols.length; j++) {
				for(int i = 0; i < nTimesPerCol; i++) {
					String query = "SELECT " + cols[j] + " FROM table" + tableID + ";";
					trainingQueries.add(query);
				}
			}
		}
		
		try {
			String filePath = "/home/asma/pp/0/";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) {
		PaperPurpose pp = new PaperPurpose();
		for(int i = 0; i < 5; i++) {
			pp.generateRi(i);
		}
	}
	
}
