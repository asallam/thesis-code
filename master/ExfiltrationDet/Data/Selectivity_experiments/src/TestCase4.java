import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;


public class TestCase4 {

	int numRecords = 1000;
	int numRoles = 4;
	int numRecordsPerRole = numRecords / numRoles; 
	int numTables = 20;
	int numColsPerTable = 10;
	
	double st; // = 1;
	double sc; // = 1;
	double ss; // = 0.5;
	
	boolean detectionQueriesIDs[];
	
	String currDirectory;
	String trainingDirectory;
	
	ArrayList<String> allDetectionQueries;
	ArrayList<String> allDetectionQueries_0;
	
	public TestCase4() {
		detectionQueriesIDs = new boolean[numRecordsPerRole];
	}
	
	public void generate() {
		formTrainingIDs();
		
		for(st = 0.5; st <= 5; st += 1) {
			for(sc = 0.5; sc <= 5; sc += 1) {
				for(ss = 100; ss >= 0; ss -= 20) {
					allDetectionQueries = new ArrayList<String>();
					allDetectionQueries_0 = new ArrayList<String>();
					currDirectory = "/home/asma/Desktop/different_s/" + st + "," + sc + "," + ss;
					// create the directory
					boolean success = (new File(currDirectory)).mkdir();
					// now create the directory of training
					trainingDirectory = currDirectory + "/training";
					success = (new File(trainingDirectory)).mkdir();
					generateR0();
					generateR1();
					generateR2();
					generateR3();
					// now write the training queries
					writeDetectionQueries();
				}
			}
		}
	}
	
	public void formTrainingIDs() {
		
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}
		
		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for(int i = 0; i < numRecordsPerRole / 4; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % numRecordsPerRole;
			detectionQueriesIDs[rNumber] = true;
		}
	}
	
	public void writeDetectionQueries() {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory + "/detection.txt"));
			
			for(int i = 0; i < allDetectionQueries.size(); i++) {
				out.write(allDetectionQueries.get(i) + "\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory + "/detection_0.txt"));
			
			for(int i = 0; i < allDetectionQueries_0.size(); i++) {
				out.write(allDetectionQueries_0.get(i) + "\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void writeTrainingQueries(int userID, ArrayList<String> trainingQueries) {
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(trainingDirectory + "/role" + userID + ".txt"));
			out.write("C\n");
			out.write("user" + userID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void generateR0() {
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zTables = new ZipfGenerator(numTables, st);		
		ZipfGenerator zColumns = new ZipfGenerator(numColsPerTable, sc);
		ZipfGenerator zSels = new ZipfGenerator(numTables, ss);
		int queryID = 0;
		for(int i = 1; i <= numTables; i++) {
			int numCurrTuples = (int)(numRecordsPerRole * zTables.getProbability(i));
			int tableID = i - 1;
			for(int k = 1; k <= numColsPerTable; k++) {
				int numColRecs = (int)(numCurrTuples * zColumns.getProbability(k));
				int colID = k - 1;
				for(int j = 0; j < numColRecs; j++) {
					String queryStr = "SELECT c" + colID + " FROM table" + tableID;
				
					if((float)i*100/numTables <= (100 - ss)) {
						queryStr = queryStr + ";";
					} else {
						queryStr = queryStr + " WHERE c0 < 5;"; // medium
					}
					
					if(detectionQueriesIDs[queryID % numRecordsPerRole]) {
						allDetectionQueries.add(0 + "," + queryStr);
						allDetectionQueries_0.add(1 + "," + queryStr);
						allDetectionQueries_0.add(2 + "," + queryStr);
						allDetectionQueries_0.add(3 + "," + queryStr);
					} else {
						trainingQueries.add(queryStr);
					}
					queryID++;
				}
			}
		}
		writeTrainingQueries(0, trainingQueries);
	}
	
	public void generateR1() {
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zTables = new ZipfGenerator(numTables, st);		
		ZipfGenerator zColumns = new ZipfGenerator(numColsPerTable, sc);
		int queryID = 0;
		
		for(int i = 1; i <= numTables; i++) {
			int numCurrTuples = (int)(numRecordsPerRole * zTables.getProbability(numTables - i + 1));
			
			for(int k = 1; k <= numColsPerTable; k++) {
				int numColRecs = (int)(numCurrTuples * zColumns.getProbability(numColsPerTable - k + 1));
				int colID = k - 1;
				int tableID = i - 1;
				for(int j = 0; j < numColRecs; j++) {
					String queryStr = "SELECT c" + colID + " FROM table" + tableID;
					
					if((float)i*100/numTables <= ss) {
						queryStr = queryStr + " WHERE c0 < 5;"; // medium in the overlap part
					} else {
						queryStr = queryStr + " WHERE c0 < 1;"; // small is the rest
					}
					
					if(detectionQueriesIDs[queryID % numRecordsPerRole]) {
						allDetectionQueries.add(1 + "," + queryStr);
						allDetectionQueries_0.add(0 + "," + queryStr);
						allDetectionQueries_0.add(2 + "," + queryStr);
						allDetectionQueries_0.add(3 + "," + queryStr);
					} else {
						trainingQueries.add(queryStr);
					}
					queryID++;
				}
			}
		}
		writeTrainingQueries(1, trainingQueries);
	}
	
	public void generateR2() {
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zTables = new ZipfGenerator(numTables, st);		
		ZipfGenerator zColumns = new ZipfGenerator(numColsPerTable, sc);
		
		int queryID = 0;
		
		for(int i = 1; i <= numTables; i++) {
			int numCurrTuples = (int)(numRecordsPerRole * zTables.getProbability(i));
			
			// starting from c1 since c0 is the one we use to select rows, it shouldn't be changed to avoid confusion. 
			for(int k = 2; k <= numColsPerTable; k++) {
				int numColRecs = (int)(numCurrTuples * zColumns.getProbability(k));
				int colID = k - 1;
				int tableID = i - 1;
				for(int j = 0; j < numColRecs; j++) {
					String queryStr = "UPDATE table" + tableID + " SET c" + colID + " = 1";
					
					if((float)i*100/numTables <= (100 - ss)) {
						queryStr = queryStr + ";";
					} else {
						queryStr = queryStr + " WHERE c0 < 5;"; // medium
					}
					
					if(detectionQueriesIDs[queryID % numRecordsPerRole]) {
						allDetectionQueries.add(2 + "," + queryStr);
						allDetectionQueries_0.add(1 + "," + queryStr);
						allDetectionQueries_0.add(0 + "," + queryStr);
						allDetectionQueries_0.add(3 + "," + queryStr);
					} else {
						trainingQueries.add(queryStr);
					}
					queryID++;
				}
			}
		}
		writeTrainingQueries(2, trainingQueries);
	}
	
	public void generateR3() {
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		ZipfGenerator zTables = new ZipfGenerator(numTables, st);		
		ZipfGenerator zColumns = new ZipfGenerator(numColsPerTable, sc);
		
		int queryID = 0;
		
		for(int i = 1; i <= numTables; i++) {
			int numCurrTuples = (int)(numRecordsPerRole * zTables.getProbability(numTables - i + 1));
			
			for(int k = 2; k <= numColsPerTable; k++) {
				int numColRecs = (int)(numCurrTuples * zColumns.getProbability(numColsPerTable - k + 1));
				int colID = k - 1;
				int tableID = i - 1;
				for(int j = 0; j < numColRecs; j++) {
					String queryStr = "UPDATE table" + tableID + " SET c" + colID + " = 1";
					
					if((float)i*100/numTables <= ss) {
						queryStr = queryStr + " WHERE c0 < 5;"; // medium in the overlap part
					} else {
						queryStr = queryStr + " WHERE c0 < 1;"; // small is the rest
					}
					
					if(detectionQueriesIDs[queryID % numRecordsPerRole]) {
						allDetectionQueries.add(3 + "," + queryStr);
						allDetectionQueries_0.add(1 + "," + queryStr);
						allDetectionQueries_0.add(2 + "," + queryStr);
						allDetectionQueries_0.add(0 + "," + queryStr);
					} else {
						trainingQueries.add(queryStr);
					}
					queryID++;
				}
			}
		}
		writeTrainingQueries(3, trainingQueries);
	}
	
	public static void main(String[] args) {
//		TestCase4 tc4 = new TestCase4();
//		// READ only, tables: zipf(100,st), columns: zipf(20, sc), selectivity: zipf(100, ss) 
//		tc4.generateR0();
//		//System.out.println("----------------------------");
//		// READ only: tables: R_zipf(100,st), columns: R_zipf(20, sc), selectivity: R_zipf(100, ss)
//		tc4.generateR1();
//		// UPDATE: tables: zipf(100,st), columns: zipf(20, sc), selectivity: zipf(100, ss) 
//		tc4.generateR2();
//		// UPDATE: tables: R_zipf(100,st), columns: R_zipf(20, sc), selectivity: R_zipf(100, ss)
//		tc4.generateR3();
		
		
		TestCase4 tc4 = new TestCase4();
		tc4.generate();
	}
}
