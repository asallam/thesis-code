DROP ROLE IF EXISTS tpcc_role_u;
DROP ROLE IF EXISTS tpcc_u;
DROP ROLE IF EXISTS tpcc_role_w;
DROP ROLE IF EXISTS tpcc_w;

CREATE ROLE tpcc_role_u;
CREATE ROLE tpcc_u WITH LOGIN;

CREATE ROLE tpcc_role_w;
CREATE ROLE tpcc_w WITH LOGIN;

GRANT tpcc_role_u to tpcc_u;
GRANT tpcc_role_w to tpcc_w;

GRANT INSERT, SELECT, UPDATE, DELETE ON order_line TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON new_order TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON stock TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON oorder TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON history TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON customer TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON district TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON item TO tpcc_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON warehouse TO tpcc_role_u;

GRANT INSERT, SELECT, UPDATE, DELETE ON order_line TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON new_order TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON stock TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON oorder TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON history TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON customer TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON district TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON item TO tpcc_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON warehouse TO tpcc_role_w;


