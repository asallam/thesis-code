ANALYZE users(u_id, name);
ANALYZE item (i_id, title);
ANALYZE review (a_id, u_id, i_id, rating);
ANALYZE review_rating (u_id, a_id, rating, status, creation_date, last_mod_date, type, vertical_id); 
ANALYZE trust (source_u_id, target_u_id, trust, creation_date);
