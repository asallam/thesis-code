/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Results;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class Histogram_Builder1 {

    ArrayList<Float> minTotalRows = new ArrayList<>();
    ArrayList<Float> minTotalSels = new ArrayList<>();
    
    ArrayList<Integer> outliers = new ArrayList<>();
    
    Histogram minTotalRows_outliers_hist;
    Histogram minTotalSels_outliers_hist;
    
    String dirName;
    
    public Histogram_Builder1(String dirName) {
        this.dirName = dirName;
    }

    public void parseFile(String fileName) {
        FileReader fileReader;
        try {
            fileReader = new FileReader(dirName + fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            line = bufferedReader.readLine();
            
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineParts = line.split(",");
                
                float minDiffRows_1 = Float.parseFloat(lineParts[4]) / 1000000;
                
                
                    
                    float minTotalSels_1 = Float.parseFloat(lineParts[6]) / 1000000;
                    float minTotalRows_1 = Float.parseFloat(lineParts[7]) / 1000000;
                    
                    minTotalSels.add(minTotalSels_1);
                    minTotalRows.add(minTotalRows_1);
                    
                    int isOutlier = Integer.parseInt(lineParts[8]);
                    outliers.add(isOutlier);
                
                
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void composeHistograms() {
        minTotalRows_outliers_hist = new Histogram(minTotalRows, outliers);
        minTotalSels_outliers_hist = new Histogram(minTotalSels, outliers);
    }
    
    public void printHistograms() {
        try {
            PrintWriter writer = new PrintWriter(dirName + "histograms.txt", "UTF-8");
            
            writer.println("--minTotalRows_outliers_hist--");
            writer.println(minTotalRows_outliers_hist.toString());
            
            writer.println("--minTotalSels_outliers_hist--");
            writer.println(minTotalSels_outliers_hist.toString());
            
            writer.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Histogram_Builder1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Histogram_Builder1.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        Histogram_Builder1 HB = new Histogram_Builder1("/home/asma/Desktop/Seats_logs/");
        HB.parseFile("output.txt");
        HB.composeHistograms();
        HB.printHistograms();
    }
}