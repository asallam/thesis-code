/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Results;

import java.util.ArrayList;

/**
 *
 * @author asma
 */
public class Histogram {
    
    ArrayList<Integer> bins;
     ArrayList<Integer> bins_count;
    ArrayList<Float> binsRanges;
    
    public Histogram(ArrayList<Float> values, ArrayList<Integer> classification) {
        
        bins = new ArrayList<>();
        binsRanges = new ArrayList<>();  
        bins_count = new ArrayList<>();
        
        float minValue = Float.MAX_VALUE;
        float maxValue = Float.MIN_VALUE;
        
        for(float value : values) {
            minValue = (value < minValue) ? value : minValue;
            maxValue = (value > maxValue) ? value: maxValue;
        }
       
        
        float range = (maxValue - minValue);
        float portion = range / 5;
        
        for(int i = 0; i < 5; i++) {
            bins.add(0);
            bins_count.add(0);
            binsRanges.add(minValue + i * portion);
        }
        
        for(int i = 0; i < values.size(); i++) {
            float value = values.get(i);
            int isOutlier = classification.get(i);
            
            if(value >= binsRanges.get(0) && value < binsRanges.get(1)) {
                
                bins.set(0, bins.get(0) + isOutlier);
                bins_count.set(0, bins_count.get(0) + 1);
            
            } else if(value >= binsRanges.get(1) && value < binsRanges.get(2)) {
                
                bins.set(1, bins.get(1) + isOutlier);
                bins_count.set(1, bins_count.get(1) + 1);
                
            } else if(value >= binsRanges.get(2) && value < binsRanges.get(3)) {
                
                bins.set(2, bins.get(2) + isOutlier);
                bins_count.set(2, bins_count.get(2) + 1);
                
            } else if(value >= binsRanges.get(3) && value < binsRanges.get(4)) {
                
                bins.set(3, bins.get(3) + isOutlier);
                bins_count.set(3, bins_count.get(3) + 1);
                
            } else if(value >= binsRanges.get(4)) {
                
                bins.set(4, bins.get(4) + isOutlier);
                bins_count.set(4, bins_count.get(4) + 1);
                
            }
        }
    }

    @Override
    public String toString() {
        String result = "";
        
        for(int i = 0; i < binsRanges.size(); i++) {
            String s = binsRanges.get(i) + "," + bins.get(i) + "," + bins_count.get(i) + "\n";
            result = result + s;
        }
        
        return result;
    }
    
    
}
