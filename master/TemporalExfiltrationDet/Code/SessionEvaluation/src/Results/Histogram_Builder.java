/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Results;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class Histogram_Builder {

    ArrayList<Float> maxAvgSels = new ArrayList<>();
    ArrayList<Float> maxDiffSels = new ArrayList<>();
    ArrayList<Float> maxAvgRows = new ArrayList<>();
    ArrayList<Float> maxDiffRows = new ArrayList<>();
    
    ArrayList<Float> minDiffSels = new ArrayList<>();
    ArrayList<Float> minDiffRows = new ArrayList<>();

    ArrayList<Integer> outliers = new ArrayList<>();
//    ArrayList<Integer> roleClusters = new ArrayList<>();

    Histogram maxAvgSels_outliers_hist;
//    Histogram maxAvgSels_clusters_hist;

    Histogram maxDiffSels_outliers_hist;
//    Histogram maxDiffSels_clusters_hist;

    Histogram maxAvgRows_outliers_hist;
//    Histogram maxAvgRows_clusters_hist;

    Histogram maxDiffRows_outliers_hist;
//    Histogram maxDiffRows_clusters_hist;
    
    Histogram minDiffRows_outliers_hist;
    Histogram minDiffSels_outliers_hist;
    
    String dirName;
    
    public Histogram_Builder(String dirName) {
        this.dirName = dirName;
    }

    public void parseFile(String fileName) {
        FileReader fileReader;
        try {
            fileReader = new FileReader(dirName + fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            line = bufferedReader.readLine();
            while ((line = bufferedReader.readLine()) != null) {
                String[] lineParts = line.split(",");
                
                float minDiffRows_1 = Float.parseFloat(lineParts[11]);
                
                if(minDiffRows_1 <= 1) {
                    minDiffRows.add(minDiffRows_1);

                    float maxAvgSel = Float.parseFloat(lineParts[6]);
                    maxAvgSels.add(maxAvgSel);

                    float maxDiffSel = Float.parseFloat(lineParts[7]);
                    maxDiffSels.add(maxDiffSel);

                    float maxAvgRows_1 = Float.parseFloat(lineParts[8]);
                    maxAvgRows.add(maxAvgRows_1);

                    float maxDiffRows_1 = Float.parseFloat(lineParts[9]);
                    maxDiffRows.add(maxDiffRows_1);

                    float minDiffSel = Float.parseFloat(lineParts[10]);
                    minDiffSels.add(minDiffSel);

                    int isOutlier = Integer.parseInt(lineParts[12]);
                    outliers.add(isOutlier);
                }
                
//                int isInRoleClusters = Integer.parseInt(lineParts[11]);
//                roleClusters.add(isInRoleClusters);

            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void composeHistograms() {
        maxAvgSels_outliers_hist = new Histogram(maxAvgSels, outliers);
//        maxAvgSels_clusters_hist = new Histogram(maxAvgSels, roleClusters);

        maxDiffSels_outliers_hist = new Histogram(maxDiffSels, outliers);
//        maxDiffSels_clusters_hist = new Histogram(maxDiffSels, roleClusters);

        maxAvgRows_outliers_hist = new Histogram(maxAvgRows, outliers);
//        maxAvgRows_clusters_hist = new Histogram(maxAvgRows, roleClusters);

        maxDiffRows_outliers_hist = new Histogram(maxDiffRows, outliers);
        
        minDiffRows_outliers_hist = new Histogram(minDiffRows, outliers);
        
        minDiffSels_outliers_hist = new Histogram(minDiffSels, outliers);
//        maxDiffRows_clusters_hist = new Histogram(maxDiffRows, roleClusters);
    }
    
    public void printHistograms() {
        try {
            PrintWriter writer = new PrintWriter(dirName + "histograms.txt", "UTF-8");
            
            writer.println("--maxAvgSels_outliers_hist--");
            writer.println(maxAvgSels_outliers_hist.toString());
            
//            writer.println("--maxAvgSels_clusters_hist--");
//            writer.println(maxAvgSels_clusters_hist.toString());

            writer.println("--maxDiffSels_outliers_hist--");
            writer.println(maxDiffSels_outliers_hist.toString());
            
//            writer.println("--maxDiffSels_clusters_hist--");
//            writer.println(maxDiffSels_clusters_hist.toString());
            
            writer.println("--maxAvgRows_outliers_hist--");
            writer.println(maxAvgRows_outliers_hist.toString());
            
//            writer.println("--maxAvgRows_clusters_hist--");
//            writer.println(maxAvgRows_clusters_hist.toString());

            writer.println("--maxDiffRows_outliers_hist--");
            writer.println(maxDiffRows_outliers_hist.toString());
            
            writer.println("--minDiffRows_outliers_hist--");
            writer.println(minDiffRows_outliers_hist.toString());
            
            writer.println("--minDiffSels_outliers_hist--");
            writer.println(minDiffSels_outliers_hist.toString());
            
//            writer.println("--maxDiffRows_clusters_hist--");
//            writer.println(maxDiffRows_clusters_hist.toString());
            
            writer.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Histogram_Builder.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Histogram_Builder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void main(String[] args) {
        Histogram_Builder HB = new Histogram_Builder("/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/");
        HB.parseFile("IsolationForest.csv");
        HB.composeHistograms();
        HB.printHistograms();
    }
}
