/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import weka.core.Instances;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import weka.clusterers.Clusterer;
import weka.clusterers.Cobweb;
import weka.clusterers.DensityBasedClusterer;

import weka.clusterers.EM;
import weka.clusterers.FarthestFirst;
import weka.core.Instance;

/**
 *
 * @author asma
 */
public class Training {

    String datasetName = "seats";
    String dirName = "/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/";

    String outputFileTraining = dirName + "clustering_training.arff";
    String outlierDetection_input_training = dirName + "outlierDetection_input_training.csv";
    String outlierDetection_output_FPs = dirName + "outlierDetection_output_FPs.txt";
    String outlierDetection_output_FNs = dirName + "outlierDetection_output_FNs.txt";
    String outlierDetection_input_FPs = dirName + "outlierDetection_input_FPs.csv";
    String outlierDetection_input_FNs = dirName + "outlierDetection_input_FNs.csv";
    String clustering_input_FPs = dirName + "clustering_detection_FPs.arff";
    String clustering_input_FNs = dirName + "clustering_detection_FNs.arff";

    HashMap<Integer, ArrayList<SFR>> rolesTrainingSessions = new HashMap<>();
    
    HashMap<Integer, ArrayList<SFR>> rolesDetectionSessions = new HashMap<>();

    String[] tablesNames = {"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};

    Clusterer clusterer;
    
    Instances trainingInstances;
    
    float portion = 0.8f;
    
    ArrayList<Integer> trueRoles_FPs;
    ArrayList<Integer> trueRoles_FNs;
    
    String clusterType;
    
    HashMap<Integer, ArrayList<Integer>> rolesMapping;
    
    ArrayList<String> detectionSFRs_FPs = new ArrayList<>();
    ArrayList<String> detectionSFRs_FNs = new ArrayList<>();
    
    ArrayList<Boolean> outlierDetResult_FPs = new ArrayList<>();
    ArrayList<Boolean> outlierDetResult_FNs = new ArrayList<>();
 
    public void addFile_training(int roleID, String fileName, String datasetName) {

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(fileName);
        HashMap<String, Session> sessions = logFilesParserTraining.getUsersSessions();

        SessionEvaluation SE_training = new SessionEvaluation(datasetName);

        ArrayList<SFR> sfrs_training = SE_training.composeSFRs(sessions);

        rolesTrainingSessions.put(roleID, sfrs_training);
    }
    
    public void addFile_detection(int roleID, String fileName, String datasetName) {

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(fileName);
        HashMap<String, Session> sessions = logFilesParserTraining.getUsersSessions();

        SessionEvaluation SE_detection = new SessionEvaluation(datasetName);

        ArrayList<SFR> sfrs_detection = SE_detection.composeSFRs(sessions);
        
        ArrayList<SFR> roleData = rolesDetectionSessions.get(roleID);
        if(roleData == null) {
            rolesDetectionSessions.put(roleID, sfrs_detection);
        } else {
            roleData.addAll(sfrs_detection);
        }
        
        
    }

    public void start(int numTraining, String clusterType) {
        
        this.clusterType = clusterType;
        
        if(clusterType.equals("EM")) {            
            clusterer = new EM();
        } else if(clusterType.equals("FF")) {
            clusterer = new FarthestFirst();
        } else if(clusterType.equals("Cobweb")) {
            clusterer = new Cobweb();
        }
        
        //writeCsvForOutlierDetection_FPs(numTraining);
        
        writeCsvForOutlierDetection(numTraining);
        
        outlierDetResult_FPs = new ArrayList<>();
        checkOutliers_FPs();
        
        outlierDetResult_FNs = new ArrayList<>();
        checkOutliers_FNs();
        
        writeARFF(numTraining);
        buildClusterer();

        findMapping(numTraining);
        
        System.out.println("--" + numTraining + "--");
        System.out.println("-------FPs");
        evaluate_FPs();
        System.out.println("-------FNs");
        evaluate_FNs();
    }

    private void writeARFF(int numTraining) {
        try {
            trueRoles_FPs = new ArrayList<>();
            trueRoles_FNs = new ArrayList<>();
            PrintWriter trainingWriter = new PrintWriter(outputFileTraining, "UTF-8");
            PrintWriter detectionWriter_FPs = new PrintWriter(clustering_input_FPs, "UTF-8");
            PrintWriter detectionWriter_FNs = new PrintWriter(clustering_input_FNs, "UTF-8");

            trainingWriter.println("@RELATION session");
            detectionWriter_FPs.println("@RELATION session");
            detectionWriter_FNs.println("@RELATION session");

            for (int i = 0; i < tablesNames.length; i++) {
                trainingWriter.println("@ATTRIBUTE t_" + i + " NUMERIC");
                detectionWriter_FPs.println("@ATTRIBUTE t_" + i + " NUMERIC");
                detectionWriter_FNs.println("@ATTRIBUTE t_" + i + " NUMERIC");
            }

            trainingWriter.println("@DATA");
            detectionWriter_FPs.println("@DATA");
            detectionWriter_FNs.println("@DATA");

            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {

                int roleID = sessionEntry.getKey();

                ArrayList<SFR> SFRs = sessionEntry.getValue();
                
                for (int i = 0; i < numTraining; i++) {
                    SFR sfr = SFRs.get(i);
                    if(i < portion * numTraining) {
                        trainingWriter.println(sfr.composeSFR_str(tablesNames));
                    } else {
                        String SFR_str = sfr.composeSFR_str(tablesNames);
                        detectionWriter_FPs.println(SFR_str);
                        trueRoles_FPs.add(roleID);
                        detectionSFRs_FPs.add(SFR_str);
                    }
                }
            }
            
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesDetectionSessions = rolesDetectionSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesDetectionSessions) {

                int roleID = sessionEntry.getKey();

                ArrayList<SFR> SFRs = sessionEntry.getValue();
                
                for (SFR sfr : SFRs) {
                    String SFR_str = sfr.composeSFR_str(tablesNames);
                        detectionWriter_FNs.println(SFR_str);
                        trueRoles_FNs.add(roleID);
                        detectionSFRs_FNs.add(SFR_str);
                    
                }
            }
            
            trainingWriter.close();
            detectionWriter_FPs.close();
            detectionWriter_FNs.close();

        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(SessionEvaluation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buildClusterer() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(outputFileTraining));
            trainingInstances = new Instances(reader);
            reader.close();
            
            clusterer.buildClusterer(trainingInstances);
            
        } catch (Exception ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void findMapping(int numTraining) {
        try {
            rolesMapping = new HashMap<>();
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {

                int roleID = sessionEntry.getKey();

                int count = 0;

                ArrayList<Integer> roleMapping = new ArrayList<>();

                for(int i = 0; i < portion * numTraining; i++) {
                    int pred = clusterer.clusterInstance(trainingInstances.instance(count));
                    count++;
                    if(!roleMapping.contains(pred)) {
                        roleMapping.add(pred);
                    }
                }

                rolesMapping.put(roleID, roleMapping);

            }
        } catch(Exception ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void evaluate_FPs() {
        BufferedReader reader = null;
        int falsePositives = 0;
        try {
            reader = new BufferedReader(new FileReader(clustering_input_FPs));
            Instances instances = new Instances(reader);
            instances.setClassIndex(instances.numAttributes() - 1);
            reader.close();
            
            for(int i = 0; i < instances.size(); i++) {
                if(outlierDetResult_FPs.get(i)) {
                    falsePositives++;
                } else {
                    Instance instance = instances.get(i);
                    int pred = clusterer.clusterInstance(instance);

                    ArrayList<Integer> mapping = rolesMapping.get(trueRoles_FPs.get(i));

                    if(!mapping.contains(pred)) {
                        falsePositives++;
                    }
                }
            }
            System.out.println(falsePositives + "/" + instances.size());
        } catch (Exception ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    // not correct
    private void evaluate_FNs() {
        BufferedReader reader = null;
        int falseNegatives = 0;
        int count = 0;
        try {
            reader = new BufferedReader(new FileReader(clustering_input_FNs));
            Instances instances = new Instances(reader);
            instances.setClassIndex(instances.numAttributes() - 1);
            reader.close();
            
            for(int i = 0; i < instances.size(); i++) {
                
                Instance instance = instances.get(i);
                int pred = clusterer.clusterInstance(instance);
                ArrayList<Integer> mapping = rolesMapping.get(trueRoles_FNs.get(i));
                
                if(!this.outlierDetResult_FNs.get(i) && mapping.contains(pred)) {
                    falseNegatives++;
                }
                count++;
            }
            System.out.println(falseNegatives + "/" + count);
        } catch (Exception ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void writeCsvForOutlierDetection(int numTraining) {
        
        PrintWriter trainingWriter = null;
        PrintWriter detectionWriter_FPs = null;
        PrintWriter detectionWriter_FNs = null;
        try {
            trainingWriter = new PrintWriter(outlierDetection_input_training, "UTF-8");
            detectionWriter_FPs = new PrintWriter(outlierDetection_input_FPs, "UTF-8");
            detectionWriter_FNs = new PrintWriter(outlierDetection_input_FNs, "UTF-8");
            
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesTrainingSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesTrainingSessions) {
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                for (int i = 0; i < numTraining; i++) {
                    SFR sfr = SFRs.get(i);
                    if(i < portion * numTraining) {
                        trainingWriter.println(sfr.composeSFR_str(tablesNames));
                    } else {
                        String SFR_str = sfr.composeSFR_str(tablesNames);
                        detectionWriter_FPs.println(SFR_str);
                    }
                }
            }
            
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesDetectionSessions = rolesDetectionSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesDetectionSessions) {
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                for (SFR sfr : SFRs) {
                    String SFR_str = sfr.composeSFR_str(tablesNames);
                    detectionWriter_FNs.println(SFR_str);
                }
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            trainingWriter.close();
            detectionWriter_FNs.close();
            detectionWriter_FPs.close();
        }
    }
    
    private void checkOutliers_FPs() {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py " + outlierDetection_input_training + " " + outlierDetection_input_FPs + " " + outlierDetection_output_FPs);
            pr.waitFor();
            
            FileReader fileReader = new FileReader(outlierDetection_output_FPs);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while((line = bufferedReader.readLine()) != null) {
                int result = Integer.parseInt(line);
                boolean isOutlier = result == -1 ? true: false;
                outlierDetResult_FPs.add(isOutlier);
            }   

            // Always close files.
            bufferedReader.close();       
            
        } catch (IOException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void checkOutliers_FNs() {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py " + outlierDetection_input_training + " " + outlierDetection_input_FNs + " " + outlierDetection_output_FNs);
            pr.waitFor();
            
            FileReader fileReader = new FileReader(outlierDetection_output_FNs);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while((line = bufferedReader.readLine()) != null) {
                int result = Integer.parseInt(line);
                boolean isOutlier = result == -1 ? true: false;
                outlierDetResult_FNs.add(isOutlier);
            }   

            // Always close files.
            bufferedReader.close();       
            
        } catch (IOException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Training.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
}

