/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.File;

/**
 *
 * @author asma
 */
public class Clustering_python {

//    String datasetName = "seats";
//    String dirName = "/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/";
//    String[] tablesNames = {"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};

    String datasetName = "tpcc";
    String dirName = "/home/asma/Desktop/tpcc_logs/";

    String outlierDetection_input_training = dirName + "outlierDetection_input_training.csv";
    String outlierDetection_input_FPs = dirName + "outlierDetection_input_FPs.csv";
    String outlierDetection_input_FNs = dirName + "outlierDetection_input_FNs.csv";
    String outlierDetection_output_FPs = dirName + "outlierDetection_output_FPs.txt";
    String outlierDetection_output_FNs = dirName + "outlierDetection_output_FNs.txt";
    
    String clustering_output_FPs_multipleLength = dirName + "clustering_output_FPs_multipleLength.txt";
    String clustering_input_FNs_multipleLength = dirName + "clustering_input_FNs_multipleLength.txt";
    String outlierDetection_output_FPs_multipleLength = dirName + "outlierDetection_output_FPs_multipleLength.txt";
    String outlierDetection_input_FNs_multipleLength = dirName + "outlierDetection_input_FNs_multipleLength.txt";

    String clustering_input_Training = dirName + "clustering_training.csv";
    String clustering_input_FPs = dirName + "clustering_detection_FPs.csv";
    String clustering_input_FNs = dirName + "clustering_detection_FNs.csv";

    HashMap<Integer, ArrayList<SFR>> rolesTrainingSessions = new HashMap<>();

    HashMap<Integer, ArrayList<SFR>> rolesDetectionSessions = new HashMap<>();

    
    //String[] tablesNames = new String[] {"customer", "district", "history", "item", "new_order", "oorder", "order_line", "stock", "warehouse"};
    
//    String[] tablesNames = new String[] {
//      "access_info", "call_forwarding", "special_facility", "subscriber"  
//    };
    
    
    String[] tablesNames = {"order_line", "new_order", "stock", "oorder", "history", "customer", "district", "item", "warehouse"};
    
//    String[] tablesNames = {"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};
    
    
    int[] tablesCards;

    float portion = 0.8f;

    ArrayList<Integer> trueRoles_FPs;
    ArrayList<Integer> trueRoles_FNs;

    HashMap<Integer, ArrayList<Integer>> rolesMapping;

    ArrayList<String> detectionSFRs_FPs = new ArrayList<>();
    ArrayList<SFR> detectionSFRs_FPs_orig = new ArrayList<>();
    
    ArrayList<String> detectionSFRs_FNs = new ArrayList<>();

    ArrayList<Boolean> outlierDetResult_FPs = new ArrayList<>();
    ArrayList<Boolean> outlierDetResult_FNs = new ArrayList<>();

    ArrayList<String> FNs_diffStrs = new ArrayList<>();

    private String OutlierDetection_distanceMetricsFileName = dirName + "OutlierDetection_distMetrics.txt";
    private String FNs_resultFileName = dirName + "FNs_result.txt";
    
    private String clustering_distanceMetricsFileName = dirName + "Clustering_distMetrics_otherRoles.txt";

    public void addFile_training(int roleID, String fileName, String datasetName) {

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(fileName);
        HashMap<String, Session> sessions = logFilesParserTraining.getUsersSessions();

        SessionEvaluation SE_training = new SessionEvaluation(datasetName);

        ArrayList<SFR> sfrs_training = SE_training.composeSFRs(sessions);

        rolesTrainingSessions.put(roleID, sfrs_training);
    }

    public void addFile_detection(int roleID, String fileName, String datasetName) {

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(fileName);
        HashMap<String, Session> sessions = logFilesParserTraining.getUsersSessions();

        SessionEvaluation SE_detection = new SessionEvaluation(datasetName);

        ArrayList<SFR> sfrs_detection = SE_detection.composeSFRs(sessions);

        ArrayList<SFR> roleData = rolesDetectionSessions.get(roleID);
        if (roleData == null) {
            rolesDetectionSessions.put(roleID, sfrs_detection);
        } else {
            roleData.addAll(sfrs_detection);
        }
    }

    int numTraining;

    public void start(float contamination) {
        setTablesCards();

        writeCsvForOutlierDetection();
        
        
        for(float m = 0.1f; m < 5.0f; m += 0.1f) {
            createOutliers_multipleLengths_outlierDetection(m);
            //createOutliers_multipleLengths_clustering(m);
        }

//        outlierDetResult_FPs = new ArrayList<>();
//        checkOutliers_FPs(contamination);
//        outlierDetResult_FNs = new ArrayList<>();
//        checkOutliers_FNs(contamination);
//
//        writeClusteringInput();
//        runClusterers();
    }

    private void writeClusteringInput() {
        try {
            trueRoles_FPs = new ArrayList<>();
            trueRoles_FNs = new ArrayList<>();
            PrintWriter trainingWriter = new PrintWriter(clustering_input_Training, "UTF-8");
            PrintWriter detectionWriter_FPs = new PrintWriter(clustering_input_FPs, "UTF-8");
            PrintWriter detectionWriter_FNs = new PrintWriter(clustering_input_FNs, "UTF-8");

            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {

                int roleID = sessionEntry.getKey();

                ArrayList<SFR> SFRs = sessionEntry.getValue();

                for (int i = 0; i < SFRs.size(); i++) {
                    SFR sfr = SFRs.get(i);
                    if (i < portion * SFRs.size()) {
                        trainingWriter.println(sfr.composeSFR_str(tablesNames));
                    } else {
                        String SFR_str = sfr.composeSFR_str(tablesNames);
                        detectionWriter_FPs.println(SFR_str);
                        trueRoles_FPs.add(roleID);
                        detectionSFRs_FPs.add(SFR_str);
                        detectionSFRs_FPs_orig.add(sfr);
                    }
                }
            }

            int count = 0;
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesDetectionSessions = rolesDetectionSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesDetectionSessions) {

                int roleID = sessionEntry.getKey();
                ArrayList<SFR> SFRs = sessionEntry.getValue();

                for (SFR sfr : SFRs) {
                    String SFR_str = sfr.composeSFR_str(tablesNames);
                    detectionWriter_FNs.println(SFR_str);
                    trueRoles_FNs.add(roleID);
                    detectionSFRs_FNs.add(SFR_str);
                }
            }
            trainingWriter.close();
            detectionWriter_FPs.close();
            detectionWriter_FNs.close();

        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(SessionEvaluation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void runClusterers() {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/clustering.py " + clustering_input_Training + " " + clustering_input_FPs + " " + dirName);
            pr.waitFor();
            String[] allClusterersNames = {"k_means_20", "k_means_8", "k_means_10", "k_means_15", "k_means_3",
                "Affinity_propagation_0.5", "Affinity_propagation_0.7", "Affinity_propagation_0.9",
                "Mean_shift",
                "Birch_20", "Birch_15", "Birch_10", "Birch_8", "Birch_3",
                "Mini_Batch_KMeans_20", "Mini_Batch_KMeans_15", "Mini_Batch_KMeans_10", "Mini_Batch_KMeans_8", "Mini_Batch_KMeans_3"
            };
            for (String clustererName : allClusterersNames) {
                System.out.println("--" + clustererName + "--");
                checkClustererAccuracy(clustererName);
                System.out.println("--------------------------------------");
            }
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void findMapping(ArrayList<Integer> clusters_training) {
        try {
            int count = 0;
            rolesMapping = new HashMap();
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {

                int roleID = sessionEntry.getKey();
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                
                ArrayList<Integer> roleMapping = new ArrayList<>();

                for (int i = 0; i < portion * SFRs.size(); i++) {
                    int pred = clusters_training.get(count);
                    count++;
                    if (!roleMapping.contains(pred)) {
                        roleMapping.add(pred);
                    }
                }
                rolesMapping.put(roleID, roleMapping);
            }
        } catch (Exception ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private int evaluate_FPs(ArrayList<Integer> clusters_fps) {
        int falsePositives = 0;
        try {

            for (int i = 0; i < clusters_fps.size(); i++) {
                int pred = clusters_fps.get(i);

                ArrayList<Integer> mapping = rolesMapping.get(trueRoles_FPs.get(i));
                if (!mapping.contains(pred)) {
                    falsePositives++;
                }
                
            }
            System.out.println("-- Result of clustering on 20% of training data -- ");
            System.out.println("FPs: " + falsePositives + "/" + clusters_fps.size());
        } catch (Exception ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }

        return falsePositives;
    }
    
    private void evaluate_Clustering_otherRoles(String clustererName, ArrayList<Integer> clusters_fps) {
        
        ArrayList<Integer> allRoles = new ArrayList<>();
        allRoles.addAll(rolesTrainingSessions.keySet());
        BufferedWriter bw = null;
        FileWriter FNs_Writer = null;
        
        try {
            
            File outputFile = new File(clustering_distanceMetricsFileName);

            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }

            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }

            FNs_Writer = new FileWriter(outputFile.getAbsoluteFile(), true);
            bw = new BufferedWriter(FNs_Writer);
            
            bw.write("--" + clustererName + "--\n");
            
            for (int i = 0; i < clusters_fps.size(); i++) {
                
                int trueRole = trueRoles_FPs.get(i);
                int pred = clusters_fps.get(i);
                SFR sfr = detectionSFRs_FPs_orig.get(i);
                
                for(int r : allRoles) {
                    String diffStr = computeDiff(sfr, r);
                    if(r != trueRole) {
                        ArrayList<Integer> mapping = rolesMapping.get(r);
                        if (mapping.contains(pred)) {
                            bw.write(diffStr + " 1\n");
                        } else {
                            bw.write(diffStr + " 0\n");
                        }
                    }
                }
            }
            bw.write("========================================\n");
        } catch (IOException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
                if (FNs_Writer != null) {
                    FNs_Writer.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private int evaluate_FNs(ArrayList<Integer> clusters_fns) {

        PrintWriter resultWriter = null;
        int falseNegatives = 0;
        try {
            resultWriter = new PrintWriter(FNs_resultFileName, "UTF-8");
            for (int i = 0; i < clusters_fns.size(); i++) {
                int pred = clusters_fns.get(i);
                ArrayList<Integer> mapping = rolesMapping.get(trueRoles_FNs.get(i));

                boolean isOutlier = this.outlierDetResult_FNs.get(i);
                boolean isInClusters = mapping.contains(pred);

                if (!isOutlier && isInClusters) {
                    falseNegatives++;
                }

                resultWriter.println(!mapping.contains(pred));

            }
            resultWriter.close();
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }
        return falseNegatives;
    }

    private void writeCsvForOutlierDetection() {

        PrintWriter trainingWriter = null;
        PrintWriter detectionWriter_FPs = null;
        PrintWriter detectionWriter_FNs = null;
        try {
            trainingWriter = new PrintWriter(outlierDetection_input_training, "UTF-8");
            detectionWriter_FPs = new PrintWriter(outlierDetection_input_FPs, "UTF-8");
            detectionWriter_FNs = new PrintWriter(outlierDetection_input_FNs, "UTF-8");

            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesTrainingSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesTrainingSessions) {
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                for (int i = 0; i < SFRs.size(); i++) {
                    SFR sfr = SFRs.get(i);
                    if (i < portion * SFRs.size()) {
                        trainingWriter.println(sfr.composeSFR_str_multipleLength(1.0f, tablesNames));
                    } else {
                        String SFR_str = sfr.composeSFR_str_multipleLength(1.0f, tablesNames);
                        detectionWriter_FPs.println(SFR_str);
                    }
                }
            }

            int count = 0;
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesDetectionSessions = rolesDetectionSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesDetectionSessions) {
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                int roleID = sessionEntry.getKey();
                for (SFR sfr : SFRs) {
                    String SFR_str = sfr.composeSFR_str_multipleLength(1.0f, tablesNames);
                    detectionWriter_FNs.println(SFR_str);

                    String diffStr = computeDiff(sfr, roleID);
                    FNs_diffStrs.add(diffStr);
                    count++;
                    
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            trainingWriter.close();
            detectionWriter_FNs.close();
            detectionWriter_FPs.close();
        }
    }

    private void checkOutliers_FPs(float contamination) {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py " + contamination + " " + outlierDetection_input_training + " " + outlierDetection_input_FPs + " " + outlierDetection_output_FPs);
            pr.waitFor();

            FileReader fileReader = new FileReader(outlierDetection_output_FPs);

            int numOutliers = 0;
            int count = 0;

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                int result = Integer.parseInt(line);
                boolean isOutlier;
                isOutlier = (result == -1);
                outlierDetResult_FPs.add(isOutlier);
                if (isOutlier) {
                    numOutliers++;
                }
                count++;
            }
            
            bufferedReader.close();

            System.out.println("-- Result of outlier detection on 20% of training data -- ");
            System.out.println("FPs: " + numOutliers + "/" + count);

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void checkOutliers_FNs(float contamination) {
        Runtime rt = Runtime.getRuntime();
        BufferedWriter bw = null;
        FileWriter FNs_Writer = null;

        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py " + contamination + " " + outlierDetection_input_training + " " + outlierDetection_input_FNs + " " + outlierDetection_output_FNs);
            pr.waitFor();

            FileReader fileReader = new FileReader(outlierDetection_output_FNs);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            File outputFile = new File(OutlierDetection_distanceMetricsFileName);

            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }

            // if file doesnt exists, then create it
            if (!outputFile.exists()) {
                outputFile.createNewFile();
            }
            int numOutliers = 0;
            int count = 0;

            FNs_Writer = new FileWriter(outputFile.getAbsoluteFile(), true);
            bw = new BufferedWriter(FNs_Writer);

            for (int i = 0; (line = bufferedReader.readLine()) != null; i++) {
                int result = Integer.parseInt(line);
                boolean isOutlier = result == -1;
                outlierDetResult_FNs.add(isOutlier);
                if (isOutlier) {
                    numOutliers++;
                }
                count++;
                FNs_Writer.write(FNs_diffStrs.get(i) + " " + isOutlier + "\n");
            }

            System.out.println("FNs: " + numOutliers + "/" + count);
            bufferedReader.close();

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (bw != null) {

                    bw.close();
                }
                if (FNs_Writer != null) {
                    FNs_Writer.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void checkClustererAccuracy(String clustererName) {

        String file1 = dirName + "clusteringResult_" + clustererName + "_training";
        String file2 = dirName + "clusteringResult_" + clustererName + "_detection_FPs";
        String file3 = dirName + "clusteringResult_" + clustererName + "_detection_FNs";

        ArrayList<Integer> clusters_training = getArray(file1);
        ArrayList<Integer> clusters_fps = getArray(file2);
//        ArrayList<Integer> clusters_fns = getArray(file3);

        findMapping(clusters_training);
        evaluate_FPs(clusters_fps);
        evaluate_Clustering_otherRoles(clustererName, clusters_fps);
//        int numFalseNegatives = evaluate_FNs(clusters_fns);

    }

    private ArrayList<Integer> getArray(String fileName) {

        FileReader fileReader = null;
        ArrayList<Integer> result = new ArrayList<>();
        try {
            fileReader = new FileReader(fileName);
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.add(Integer.parseInt(line));
            }
            bufferedReader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fileReader.close();
            } catch (IOException ex) {
                Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return result;
    }

    private String computeDiff(SFR sfr_0, int roleID) {
        ArrayList<SFR> roleTrainingSessions = rolesTrainingSessions.get(roleID);
        SFR_Diff sfr_diff = new SFR_Diff();
        for (int i = 0; i < 50; i++) {
            SFR sfr_1 = roleTrainingSessions.get(i);
            sfr_0.getDiff(sfr_diff, sfr_1, tablesNames, tablesCards);
        }
        return sfr_diff.toString();
    }

    public void setTablesCards() {
        SessionEvaluation SE_training = new SessionEvaluation(datasetName);
        tablesCards = new int[tablesNames.length];
        for (int i = 0; i < tablesNames.length; i++) {
            int count = SE_training.getTableSel(tablesNames[i]);
            tablesCards[i] = count;
        }
    }
    
    public void createOutliers_multipleLengths_outlierDetection(float multiple) {
        PrintWriter detectionWriter_FNs = null;
        try {
            detectionWriter_FNs = new PrintWriter(outlierDetection_input_FNs_multipleLength, "UTF-8");
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesTrainingSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesTrainingSessions) {
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                for (int i = 0; i < SFRs.size(); i++) {
                    SFR sfr = SFRs.get(i);
                    String SFR_str = sfr.composeSFR_str_multipleLength(multiple, tablesNames);
                    detectionWriter_FNs.println(SFR_str);
                }
            }
            
            detectionWriter_FNs.close();
            
            Runtime rt = Runtime.getRuntime();
            String prog = "python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py 0.0001 " + outlierDetection_input_training + " " + outlierDetection_input_FNs_multipleLength + " " + outlierDetection_output_FPs_multipleLength;
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py 0.0001 " + outlierDetection_input_training + " " + outlierDetection_input_FNs_multipleLength + " " + outlierDetection_output_FPs_multipleLength);
            pr.waitFor();
            
            String line;
            int numOutliers = 0;
            int count = 0;
            FileReader fileReader = new FileReader(outlierDetection_output_FPs_multipleLength);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null) {
                int result = Integer.parseInt(line);
                boolean isOutlier = result == -1;
                if (isOutlier) {
                    numOutliers++;
                }
                count++;
            }
            
            System.out.println("Multiple = " + multiple + " " + numOutliers + "/" + count);
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            
        }
    }
    
    
    private void createOutliers_multipleLengths_clustering(float multiple) {
        try {
            PrintWriter trainingWriter = new PrintWriter(clustering_input_Training, "UTF-8");
            PrintWriter detectionWriter_FNs = new PrintWriter(clustering_input_FNs_multipleLength, "UTF-8");
            
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {

                int roleID = sessionEntry.getKey();
                ArrayList<SFR> SFRs = sessionEntry.getValue();

                for (int i = 0; i < portion * SFRs.size(); i++) {
                    SFR sfr = SFRs.get(i);
                    trainingWriter.println(sfr.composeSFR_str_multipleLength(1.0f, tablesNames));
                    detectionWriter_FNs.println(sfr.composeSFR_str_multipleLength(multiple, tablesNames));
                }
            }
            
            trainingWriter.close();
            detectionWriter_FNs.close();
            
            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/clustering.py " + clustering_input_Training + " " + clustering_input_FNs_multipleLength + " " + dirName);
            pr.waitFor();
            String[] allClusterersNames = {"k_means_20", "k_means_8", "k_means_10", "k_means_15", "k_means_3",
                "Affinity_propagation_0.5", "Affinity_propagation_0.7", "Affinity_propagation_0.9",
                "Mean_shift",
                "Birch_20", "Birch_15", "Birch_10", "Birch_8", "Birch_3",
                "Mini_Batch_KMeans_20", "Mini_Batch_KMeans_15", "Mini_Batch_KMeans_10", "Mini_Batch_KMeans_8", "Mini_Batch_KMeans_3"
            };
            
            
            
            for (String clustererName : allClusterersNames) {
                String file1 = dirName + "clusteringResult_" + clustererName + "_training";
                String file2 = dirName + "clusteringResult_" + clustererName + "_detection_FPs";
                
                ArrayList<Integer> clusters_training = getArray(file1);
                ArrayList<Integer> clusters_fns = getArray(file2);
                int FNs = 0;
                int count = 0;
                
                findMapping(clusters_training);
                
                for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {
                    int roleID = sessionEntry.getKey();
                    ArrayList<Integer> mapping = rolesMapping.get(roleID);
                    
                    ArrayList<SFR> SFRs = sessionEntry.getValue();
                    for (int i = 0; i < portion * SFRs.size(); i++) {
                        int pred = clusters_fns.get(count);
                        
                        if(mapping.contains(pred)) {
                            FNs++;
                        }
                        count++;
                    }
                }
                System.out.println(clustererName + ": multiple = " + multiple + " " + FNs + "/" + count);
            }
            
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(SessionEvaluation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
