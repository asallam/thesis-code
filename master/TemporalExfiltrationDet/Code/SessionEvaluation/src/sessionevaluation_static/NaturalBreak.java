package sessionevaluation_static;

/**
 *
 * @author asma
 */
public class NaturalBreak {
    float start;
    float end;
    
    public NaturalBreak(float start, float end) {
        this.start = start;
        this.end = end;
    }
    
    public NaturalBreak(float start) {
        this.start = start;
        this.end = Float.MAX_VALUE;
    }

    float getStart() {
        return start;
    }

    float getEnd() {
        return end;
    }
    
}
