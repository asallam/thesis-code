/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

/**
 *
 * @author asma
 */
class SFR_Diff {

    float maxAvgSel = Float.MIN_VALUE;
    float maxDiffSel = Float.MIN_VALUE;
    
    float maxAvgRows = Float.MIN_VALUE;
    float maxDiffRows = Float.MIN_VALUE;
    
    float minDiffSel = Float.MAX_VALUE;
    float minDiffRows = Float.MAX_VALUE;
    
    float minTotalSel = Float.MAX_VALUE;
    float minTotalRow = Float.MAX_VALUE;
    
    void addAvgSel(float newAvg) {
        maxAvgSel = (newAvg > maxAvgSel) ? newAvg : maxAvgSel;
    }
    
    void addAvgRows(float newAvgRows) {
        maxAvgRows = (newAvgRows > maxAvgRows) ? newAvgRows : maxAvgRows;
    }

    void maxDiffSel(float newMax) {
        maxDiffSel = (newMax > maxDiffSel) ? newMax : maxDiffSel;
    }
    
    void maxDiffRows(float newMaxRows) {
        maxDiffRows = (newMaxRows > maxDiffRows) ? newMaxRows : maxDiffRows;
    }
    
    void minDiffSel(float newMin) {
        minDiffSel = (newMin < minDiffSel) ? newMin : minDiffSel;
    }
    
    void minDiffRows(float newMinRows) {
        minDiffRows = (newMinRows < minDiffRows) ? newMinRows : minDiffRows;
    }

    @Override
    public String toString() {
        return maxAvgSel + " " + maxDiffSel + " " + maxAvgRows + " " + maxDiffRows + " " + minDiffSel + " " + minDiffRows + " " + minTotalSel + " " + minTotalRow;
    }

    void minTotalSel(float newSel) {
        this.minTotalSel = (newSel < minTotalSel) ? newSel : minTotalSel;
    }

    void minTotalRow(float newRow) {
        this.minTotalRow = (newRow < minTotalRow) ? newRow : minTotalRow;
    }
    
}
