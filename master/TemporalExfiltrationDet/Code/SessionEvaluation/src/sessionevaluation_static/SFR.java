/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author asma
 */
public class SFR {
    HashMap<String, Float> sels = new HashMap<>();
    long sessionLength;
    
    public SFR (long sessionLength) {
        this.sessionLength = sessionLength;
    }
    
    public void addSel(String tableName, float sel) {
       sels.put(tableName.toLowerCase(), sel);
    }
    
    public String composeSFR_str(String []tablesNames) {
        String SFR_str = "";//String.valueOf(sessionLength);
        for(String tableName : tablesNames) {
            Float sel = sels.get(tableName);
            if(!SFR_str.isEmpty()) {
                SFR_str = SFR_str + ",";
            }
            if(sel == null) {
                SFR_str = SFR_str + "0";
            } else {
                SFR_str = SFR_str + String.format("%.2f", sel);
            }
        }
        return SFR_str;
    }
    
    public String composeSFR_str_multipleLength(float multiple, String []tablesNames) {
        String strWithoutLength = composeSFR_str(tablesNames);
        long newLength = (long) (sessionLength * multiple);
        
        return (String.valueOf(newLength) + "," + strWithoutLength);
    }
    
    SFR_Diff  getDiff(ArrayList<SFR> SFRs, String []tablesNames, int []tablesNumRows) {
        SFR_Diff sfr_diff = new SFR_Diff();
        for (SFR sfr : SFRs) {
            getDiff(sfr_diff, sfr, tablesNames, tablesNumRows);
        }
        return sfr_diff;
    }

    void getDiff(SFR_Diff sfr_diff, SFR sfr_1, String []tablesNames, int []tablesNumRows) {
        int count = 0;
        
        float maxDiffSel  = Float.MIN_VALUE;
        float maxDiffRows = Float.MIN_VALUE;
        
        float minDiffSel  = Float.MAX_VALUE;
        float minDiffRows = Float.MAX_VALUE;
        
        float totalDiffSel = 0;
        float totalDiffRows = 0;
        
        float totalSel = 0;
        float totalRow = 0;
        
        for(int i = 0; i < tablesNames.length; i++) {
            String tableName = tablesNames[i];
            int tableNumRows = tablesNumRows[i];
            float sel1 = sfr_1.tableSel(tableName);
            float sel0 = this.tableSel(tableName);
            if(sel0 > sel1) {
                float diff = sel0 - sel1;
                maxDiffSel = (diff > maxDiffSel) ? diff : maxDiffSel;
                float diffRows = tableNumRows * (sel0 - sel1);
                maxDiffRows = (diffRows > maxDiffRows) ? diffRows : maxDiffRows;
                
                minDiffSel = (diff < minDiffSel) ? diff : minDiffSel;
                minDiffRows = (diffRows < minDiffRows) ? diffRows : minDiffRows;
                
                totalDiffSel += diff;
                totalDiffRows += diffRows;
                count++;
            }
            
            totalSel += Math.abs(sel0 - sel1);
            totalRow += Math.abs(sel0 - sel1) * tablesNumRows[i];
        }
        
        float avgDiffSel = totalDiffSel / count;
        float avgDiffRows = totalDiffRows / count;
        sfr_diff.addAvgSel(avgDiffSel);
        sfr_diff.addAvgRows(avgDiffRows);
        
        sfr_diff.maxDiffSel(maxDiffSel);
        sfr_diff.maxDiffRows(maxDiffRows); 
        
        sfr_diff.minDiffSel(minDiffSel);
        sfr_diff.minDiffRows(minDiffRows);
        
        sfr_diff.minTotalSel(totalSel);
        sfr_diff.minTotalRow(totalRow);
    }

    private float tableSel(String tableName) {
        Float sel = sels.get(tableName);
        if(sel == null) {
            return 0;
        } else {
            return sel;
        }
    }

    Long getLength() {
        return sessionLength;
    }
}
