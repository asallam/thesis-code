/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class Main {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
//            String datasetName = "seats";
//            String dirName = "/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/";
            /*File folder = new File(dirName);
             File[] listOfFiles = folder.listFiles();
            
             for (File oneFile : listOfFiles) {
             if (oneFile.isFile() && oneFile.getName().endsWith(".csv")) {
            
             String fileFullName = dirName + "/" + oneFile.getName();
            
             LogFilesParser logFilesParserTraining = new LogFilesParser();
             logFilesParserTraining.parseFile(fileFullName);
             HashMap<String, Session> trainingSessions = logFilesParserTraining.getUsersSessions();
            
             SessionEvaluation SE_training = new SessionEvaluation(datasetName);
             ArrayList<SFR> sfrs_training = SE_training.composeSFRs(trainingSessions);
            
             String outputFileName = fileFullName.substring(0, fileFullName.indexOf(".")) + "_SFR";
            
             SE_training.printSFRs(sfrs_training, outputFileName);
             }
            
             }*/
            /*Training training = new Training();
             training.addFile_training(1, dirName + "training_1.csv", datasetName);
             training.addFile_training(2, dirName + "training_2.csv", datasetName);
             training.addFile_training(3, dirName + "training_3.csv", datasetName);
             training.addFile_training(4, dirName + "training_4.csv", datasetName);
             training.addFile_training(5, dirName + "training_5.csv", datasetName);
             training.addFile_training(6, dirName + "training_6.csv", datasetName);
            
             //        training.addFile_detection(1, dirName + "rate_100.csv", datasetName);
            
             training.addFile_detection(2, dirName + "rate_15.csv", datasetName);
            
             //training.addFile_detection(2, dirName + "rate_100_2_1.csv", datasetName);
             //training.addFile_detection(2, dirName + "rate_100_2_2.csv", datasetName);
             //training.addFile_detection(2, dirName + "rate_100_2_3.csv", datasetName);
            
             System.out.println("----------------------");
             for (int i = 1000; i <= 1000; i += 100) {
             //System.out.println("--EM--");
             training.start(i, "EM");
             //System.out.println("--FF--");
             //training.start(i, "FF");
             //System.out.println("--Cobweb--");
             //training.start(i, "Cobweb");
             }
             System.out.println("----------------------");
             }
             */
            String datasetName = "tpcc";
            String dirName = "/home/asma/Desktop/tpcc_logs/";

            File file = new File("/home/asma/Desktop/tpcc_logs/output.txt");
            fos = new FileOutputStream(file);
            PrintStream ps = new PrintStream(fos);
            System.setOut(ps);

            Clustering_python cp = new Clustering_python();
            cp.addFile_training(1, dirName + "training_1.csv", datasetName);
            cp.addFile_training(2, dirName + "training_2.csv", datasetName);
            cp.addFile_training(3, dirName + "training_3.csv", datasetName);
            cp.addFile_training(4, dirName + "training_4.csv", datasetName);
            cp.addFile_training(5, dirName + "training_5.csv", datasetName);

//            cp.addFile_detection(1, dirName + "detection_1_1.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_1_2.csv", datasetName);
//            
//            cp.addFile_detection(2, dirName + "detection_2.csv", datasetName);
//            
//            cp.addFile_detection(3, dirName + "detection_3.csv", datasetName);
//            
//            cp.addFile_detection(4, dirName + "detection_4.csv", datasetName);
//            cp.addFile_training(5, dirName + "training_5.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_1.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_2.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_3.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_4.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_5.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_6.csv", datasetName);
//            cp.addFile_detection(1, dirName + "detection_NO_7.csv", datasetName);
////            
//            cp.addFile_detection(2, dirName + "detection_payment_1.csv", datasetName);
//            cp.addFile_detection(2, dirName + "detection_payment_2.csv", datasetName);
////            
//            cp.addFile_detection(3, dirName + "detection_os_1.csv", datasetName);
//            cp.addFile_detection(3, dirName + "detection_os_2.csv", datasetName);
//            cp.addFile_detection(3, dirName + "detection_os_3.csv", datasetName);
//            cp.addFile_detection(3, dirName + "detection_os_4.csv", datasetName);
////            
//            cp.addFile_detection(4, dirName + "detection_delivery_1.csv", datasetName);
//            cp.addFile_detection(4, dirName + "detection_delivery_2.csv", datasetName);
//            cp.addFile_detection(4, dirName + "detection_delivery_3.csv", datasetName);
//            cp.addFile_detection(4, dirName + "detection_delivery_4.csv", datasetName);
//            
//            cp.addFile_detection(5, dirName + "detection_sl.csv", datasetName);
            
//            cp.addFile_training(1, dirName + "detection_NO_1.csv", datasetName);
//            cp.addFile_training(1, dirName + "detection_NO_2.csv", datasetName);
//            cp.addFile_training(1, dirName + "detection_NO_3.csv", datasetName);
//            cp.addFile_training(1, dirName + "detection_NO_4.csv", datasetName);
//            cp.addFile_training(1, dirName + "detection_NO_5.csv", datasetName);
//            cp.addFile_training(1, dirName + "detection_NO_6.csv", datasetName);
//            cp.addFile_training(1, dirName + "detection_NO_7.csv", datasetName);
//           
//            cp.addFile_training(2, dirName + "detection_payment_1.csv", datasetName);
//            cp.addFile_training(2, dirName + "detection_payment_2.csv", datasetName);
//           
//            cp.addFile_training(3, dirName + "detection_os_1.csv", datasetName);
//            cp.addFile_training(3, dirName + "detection_os_2.csv", datasetName);
//            cp.addFile_training(3, dirName + "detection_os_3.csv", datasetName);
//            cp.addFile_training(3, dirName + "detection_os_4.csv", datasetName);
//            
//            cp.addFile_training(4, dirName + "detection_delivery_1.csv", datasetName);
//            cp.addFile_training(4, dirName + "detection_delivery_2.csv", datasetName);
//            cp.addFile_training(4, dirName + "detection_delivery_3.csv", datasetName);
//            cp.addFile_training(4, dirName + "detection_delivery_4.csv", datasetName);
////
//            cp.addFile_training(5, dirName + "detection_sl.csv", datasetName);

            cp.start(0.0001f);

//            float []cs = {0.01f, 0.05f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f};
//            
//            for(float c: cs) {
//                System.out.println("c = " + c);
//                cp.start(1000, c);
//            }
            //String fileName = dirName + "rate_" + 90 + ".csv";
//            String fileName = dirName + "detection_os_15-90.csv";
//            cp.addFile_detection(3, fileName, datasetName);
//            for(float c: cs) {
//                System.out.println("c = " + c);
//                cp.start(1000, c);
//            }
//            
//            cp.start(1000, 0.0001f);
//            int[] rates = {60, 70, 80, 90};//15, 30, 40, 50};//, 30, 40, 50};//, 60, 70, 80, 90};
//            for(int rate : rates) {
//                System.out.println("rate = " + rate);
//                System.out.println("---------------------" );
//                
//                String fileName = dirName + "rate_" + rate + ".csv";
//                
//                cp.addFile_detection(2, fileName, datasetName);
//                
//                for (int i = 1000; i <= 1000; i += 200) {
//                    cp.start(i, 0.0001f);
//                }
//                
//            }
//                
//                   cp.start(1000, 0.0001f);
//                '
//                System.out.println("---------------------" );
//                
//            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
