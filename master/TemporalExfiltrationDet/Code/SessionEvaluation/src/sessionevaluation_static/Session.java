/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author asma
 */
public class Session {
    Date startTime;
    Date endTime;
    String UID;
    
    ArrayList<String> allQueries;
    
    public Session(Date startTime, String UID) {
        this.startTime = startTime;
        allQueries = new ArrayList<>();
        this.UID = UID;
    }
    
    public void endSession(Date endTime) {
        this.endTime = endTime;
    }
    
    public void addQuery(Date lastQueryTime, String query) {
        this.endTime = lastQueryTime;
        this.allQueries.add(query);
    }

    long getLength() {
        if(endTime == null) {
            return 0;
        }
        return this.endTime.getTime() - this.startTime.getTime();
    }
}
