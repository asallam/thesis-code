/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class LogFilesParser {

    HashMap<String, ArrayList<String>> usersQueries;

    ArrayList<String> UIDs = new ArrayList<>();
    ArrayList<String> queries = new ArrayList<>();

    HashMap<String, Session> allSessions;

    public LogFilesParser() {
        allSessions = new HashMap<>();
    }

    public void parseTrainingFiles(String dirName) {

        File folder = new File(dirName);
        File[] listOfFiles = folder.listFiles();

        for (File oneFile : listOfFiles) {
            if (oneFile.isFile()) {
                String fileName = oneFile.getName();
                if (fileName.endsWith(".csv")) {
                    parseFile(dirName + "/" + oneFile.getName());
                }
            }
        }
    }

    public void parseFile(String fileName) {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            while (line != null) {

                if (line.contains("\",\"parameters:")) {
                    String[] lineParts = line.split(",");
                    String UID = lineParts[3];
                    String dateStr = lineParts[0];
                    Date date = convertStringToDate(dateStr);
                    String query = composeQuery(line);

                    if (query != null) {
                        addQueryToSession(UID, date, query);
                    }

                    UIDs.add(UID);
                    queries.add(query);
                    
                } else if (line.contains("SET SESSION")) {
                    String[] lineParts = line.split(",");
                    String dateStr = lineParts[0];
                    Date date = convertStringToDate(dateStr);
                    String UID = lineParts[3];
                    startNewSession(date, UID);
                }

                line = br.readLine();
            }

            //System.out.println();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private String composeQuery(String line) {
        String query = line.substring(line.indexOf(":", line.indexOf("execute ") + 8) + 2, line.indexOf("\",\"parameters:"));
        query = query.replaceAll("\"", "");

        String allParams = line.substring(line.indexOf("parameters:"), line.indexOf("\",", line.indexOf("parameters:")));
        allParams = allParams.replaceAll("parameters:", "");
        //System.out.println(allParams);

        String[] parametersSplit = allParams.split(", ");
        for (int i = parametersSplit.length - 1; i >= 0; i--) {
            String[] keyValue = parametersSplit[i].split("=");
            keyValue[0] = keyValue[0].trim();
            keyValue[0] = keyValue[0].replaceAll("\\$", "__");

            /*keyValue[1] = keyValue[1].replaceAll("\\$", "0");
             keyValue[1] = keyValue[1].replaceAll("\"", "1");
             keyValue[1] = keyValue[1].replaceAll("'", "2");
             keyValue[1] = "'" + keyValue[1] + "'";*/
            keyValue[1] = keyValue[1].trim();

            query = query.replaceAll("\\$", "__");

            query = query.replaceAll(keyValue[0], keyValue[1]);
        }

        return query;
    }

    public ArrayList<String> getUIDs() {
        return UIDs;
    }

    public ArrayList<String> getAllQueries() {
        return queries;
    }

    private Date convertStringToDate(String dateStr) {
        try {
            String[] dateParts = dateStr.split(" ");

            SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm:ss");
            Date date = date12Format.parse(dateParts[1]);
            return date;
        } catch (ParseException ex) {
            Logger.getLogger(LogFilesParser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private void startNewSession(Date date, String UID) {
        Session session = new Session(date, UID);
        allSessions.put(UID, session);
    }

    public void addQueryToSession(String UID, Date date, String query) {
        Session session = allSessions.get(UID);
        if(session != null)
            session.addQuery(date, query);
    }

    public void endSession(Date date, String UID) {
        Session session = allSessions.get(UID);
        session.endSession(date);
    }

    public HashMap getUsersSessions() {
        return allSessions;
    }
}
