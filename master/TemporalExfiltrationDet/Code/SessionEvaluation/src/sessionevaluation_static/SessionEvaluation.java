/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import Utils.DB_connector;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class SessionEvaluation {
    // seats
//    String[] tablesNames = {"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};
//    String[] tablesAtts = {"AL_ID", "AP_ID", "D_AP_ID0, D_AP_ID1", "CFH_NAME", "*", "CO_ID", "C_ID", "F_ID", "FF_C_ID, FF_AL_ID", "R_ID, R_C_ID, R_F_ID"};
    
    // tpcc
    String[] tablesNames = {"order_line", "new_order", "stock", "oorder", "history", "customer", "district", "item", "warehouse"};
    String[] tablesAtts = {"ol_w_id, ol_d_id, ol_o_id, ol_number", "no_w_id, no_d_id, no_o_id", "s_w_id, s_i_id", 
        "o_w_id, o_d_id, o_id", "*", "c_w_id, c_d_id, c_id", "d_w_id, d_id",
            "i_id", "w_id"
    };
    
    // tatp
//    String[] tablesNames = {"subscriber", "access_info", "call_forwarding", "special_facility" };
//    String[] tablesAtts = {"s_id", "s_id, ai_type", "s_id, sf_type, start_time", "s_id, sf_type"};
//    
    
    
    HashMap<String, String> tablesAttsMap = new HashMap<>();
    
    String allTablesNames = "";
    
    String dataset;
    
    DB_connector dB_connector;
    
    String dirName;
    
    public SessionEvaluation(String dataset) {
        
        for(int i = 0; i < tablesNames.length; i++) {
            tablesAttsMap.put(tablesNames[i].toLowerCase(), tablesAtts[i].toLowerCase());
            
            if(!allTablesNames.equals("")) {
                allTablesNames = allTablesNames + ", "; 
            }
            allTablesNames = allTablesNames + tablesNames[i].toLowerCase();
            
        }
        this.dataset = dataset;
        
        dB_connector = new DB_connector(dataset);
        
        dirName = "/home/asma/Desktop/PG_logs/" + dataset + "/";
    }
    
    public ArrayList<SFR> composeSFRs(HashMap<String, Session> roleSessions) {
        
        Set<Map.Entry<String, Session>> allEntries = roleSessions.entrySet();
        
        ArrayList<SFR> allSFRs = new ArrayList<>();
        
        for(Map.Entry<String, Session> userEntry: allEntries) {
            HashMap<String, ArrayList<String>> tablesQueries = new HashMap<>();
            Session sessionInfo = userEntry.getValue();
            long length = sessionInfo.getLength();
            if(length == 0) {
                continue;
            }
            
            ArrayList<String> sessionQueries = sessionInfo.allQueries;
            
            for(String query: sessionQueries) {
                String queryLower = query.toLowerCase();
                if (queryLower.startsWith("select")) {
                    String tablesPortion = query.substring(queryLower.indexOf(" from") + 5, queryLower.indexOf("where"));
                    tablesPortion = tablesPortion.trim();
                    String tablesArr[] = tablesPortion.split(",");
                    
                    String predicate;
                    if (queryLower.contains("for update")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("for update")).trim();
                    } else if (queryLower.contains("order by")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("order by")).trim();
                    } else {
                        predicate = query.substring(queryLower.indexOf("where") + 5).trim();
                    }

                    for (String table : tablesArr) {
                        String tableName = table.trim();
                        
                        ArrayList<String> tableQueries = tablesQueries.get(tableName);
                        if(tableQueries == null) {
                            tableQueries = new ArrayList<>();
                            tablesQueries.put(tableName, tableQueries);
                        }
                        
                        
                        tableQueries.add(tablesPortion + " where " + predicate);
                    }
                }
            }
            allSFRs.add(composeSFR(length, tablesQueries));
        }
        
        return allSFRs;                
    }

    private SFR composeSFR(long length, HashMap<String, ArrayList<String>> tablesQueries) {
        
        SFR sfr = new SFR(length);
        
        Set<Map.Entry<String, ArrayList<String>>> allEntries = tablesQueries.entrySet();
        for(Map.Entry<String, ArrayList<String>> entry: allEntries) {
            String tableName = entry.getKey();
            String allPreds = "";
            String subQuery = "";
            String tableAtts = tablesAttsMap.get(tableName.toLowerCase());
            for(String predicate : entry.getValue()) {
                if(!subQuery.isEmpty()) {
                    subQuery = subQuery + " UNION ";
                }
                subQuery = subQuery + "(select " + tableAtts + " from " + predicate + ")";
            }
         
            String query = "select count(*) from (" + subQuery + ") as temp";
            
            int count = dB_connector.executeQuery_getCount(query);
            int tableRowCount = dB_connector.getNumTableRows(tableName);
            
            float sel = (float)count * 1000000 / tableRowCount;
            sfr.addSel(tableName, sel);
            //System.out.println(sel);
        }
        return sfr;
    }
    
    public void printSFRs(ArrayList<SFR> allSFRs, String fileName) {
        
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(fileName, "UTF-8");
            
            for(SFR sfr : allSFRs) {
                String sfrStr = sfr.composeSFR_str(tablesNames);
                writer.println(sfrStr);
                //System.out.println(sfrStr);
            }
            
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SessionEvaluation.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SessionEvaluation.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
        
    }

    int getTableSel(String tableName) {
        return dB_connector.getNumTableRows(tableName);
    }
}
