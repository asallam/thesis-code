/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionevaluation_static;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;

/**
 *
 * @author asma
 */
public class SE_withIntervals {

//    String datasetName = "seats";
//    String dirName = "/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/";
//    String[] tablesNames = {"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};

    String datasetName = "tpcc";
    String dirName = "/home/asma/Desktop/tpcc_logs/";
    HashMap<Integer, ArrayList<SFR>> rolesTrainingSessions = new HashMap<>();

    HashMap<Integer, ArrayList<SFR>> rolesDetectionSessions = new HashMap<>();

    //String[] tablesNames = new String[] {"customer", "district", "history", "item", "new_order", "oorder", "order_line", "stock", "warehouse"};
//    String[] tablesNames = new String[] {
//      "access_info", "call_forwarding", "special_facility", "subscriber"  
//    };
    //TPCC
    String[] tablesNames = {"order_line", "new_order", "stock", "oorder", "history", "customer", "district", "item", "warehouse"};
    // Seats
//    String[] tablesNames = {"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};
    int[] tablesCards;

    float portion = 0.8f;

    ArrayList<Integer> trueRoles_FPs;
    ArrayList<Integer> trueRoles_FNs;

    HashMap<Integer, ArrayList<Integer>> rolesMapping;

    ArrayList<String> detectionSFRs_FPs = new ArrayList<>();
    ArrayList<SFR> detectionSFRs_FPs_orig = new ArrayList<>();

    ArrayList<String> detectionSFRs_FNs = new ArrayList<>();

    ArrayList<Boolean> outlierDetResult_FPs = new ArrayList<>();
    ArrayList<Boolean> outlierDetResult_FNs = new ArrayList<>();

    ArrayList<String> FNs_diffStrs = new ArrayList<>();

    private String OutlierDetection_distanceMetricsFileName = dirName + "OutlierDetection_distMetrics.txt";
    private String FNs_resultFileName = dirName + "FNs_result.txt";

    private String clustering_distanceMetricsFileName = dirName + "Clustering_distMetrics_otherRoles.txt";

    private String trainingSessionsLength_fileName = dirName + "trainingSessionsLengths.txt";
    private String detectionSessionsLength_fileName = dirName + "detectionSessionsLengths.txt";

    private String outputTrainingNaturalBreaks_fileName = dirName + "naturalBreaks.txt";
    private String outputDetectionNaturalBreaks_fileName = dirName + "outputDetectionNaturalBreaks.txt";

    ArrayList<NaturalBreak> naturalBreaks = new ArrayList<>();
    ArrayList<Float> breaksIntervals = new ArrayList<>();

    public SE_withIntervals() {
        setTablesCards();
    }

    public void setTablesCards() {
        SessionEvaluation SE_training = new SessionEvaluation(datasetName);
        tablesCards = new int[tablesNames.length];
        for (int i = 0; i < tablesNames.length; i++) {
            int count = SE_training.getTableSel(tablesNames[i]);
            tablesCards[i] = count;
        }
    }

    public void addFile_training(int roleID, String fileName, String datasetName) {

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(fileName);
        HashMap<String, Session> sessions = logFilesParserTraining.getUsersSessions();

        SessionEvaluation SE_training = new SessionEvaluation(datasetName);

        ArrayList<SFR> sfrs_training = SE_training.composeSFRs(sessions);

        rolesTrainingSessions.put(roleID, sfrs_training);
    }

    public void addFile_detection(int roleID, String fileName, String datasetName) {

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(fileName);
        HashMap<String, Session> sessions = logFilesParserTraining.getUsersSessions();

        SessionEvaluation SE_detection = new SessionEvaluation(datasetName);

        ArrayList<SFR> sfrs_detection = SE_detection.composeSFRs(sessions);

        ArrayList<SFR> roleData = rolesDetectionSessions.get(roleID);
        if (roleData == null) {
            rolesDetectionSessions.put(roleID, sfrs_detection);
        } else {
            roleData.addAll(sfrs_detection);
        }
    }

    ArrayList<Long> detectionSessionsLengths_ToCheckFalsePositives = new ArrayList<>();

    public void writeSessionsLengths() {
        PrintWriter trainingWriter = null;
        PrintWriter detectionWriter = null;

        try {
            Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
            trainingWriter = new PrintWriter(trainingSessionsLength_fileName, "UTF-8");
            detectionWriter = new PrintWriter(detectionSessionsLength_fileName, "UTF-8");

            for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {
                ArrayList<SFR> SFRs = sessionEntry.getValue();
                for (int i = 0; i < SFRs.size(); i++) {
                    SFR sfr = SFRs.get(i);
                    if (i < portion * SFRs.size()) {
                        trainingWriter.write(sfr.getLength() + "\n");
                    } else {
                        detectionSessionsLengths_ToCheckFalsePositives.add(sfr.getLength());
                    }
                }
            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(SE_withIntervals.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SE_withIntervals.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            trainingWriter.close();
            detectionWriter.close();
        }
    }

    public void findTrainingIntervals() {
        writeSessionsLengths();
        runPythonFindIntervals();
        readTrainingIntervals();
        findFalsePositiveSessionsLengths();
    }

    public void detectOutliers() {
        partitionSFRs();
    }

    private void runPythonFindIntervals() {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/naturalBreaks.py " + trainingSessionsLength_fileName + " " + detectionSessionsLength_fileName + " " + outputTrainingNaturalBreaks_fileName + " " + outputDetectionNaturalBreaks_fileName);
            pr.waitFor();

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void readTrainingIntervals() {
        try {
            FileReader fileReader = new FileReader(outputTrainingNaturalBreaks_fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                breaksIntervals.add(Float.parseFloat(line));
            }

            for (int i = 0; i < breaksIntervals.size(); i++) {
                if (i == breaksIntervals.size() - 1) {
                    naturalBreaks.add(new NaturalBreak(breaksIntervals.get(i)));
                } else {
                    naturalBreaks.add(new NaturalBreak(breaksIntervals.get(i), breaksIntervals.get(i + 1)));
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SE_withIntervals.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SE_withIntervals.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    String outlierDetection_input_training = dirName + "outlierDetection_input_training.csv";
    String outlierDetection_input_FNs = dirName + "outlierDetection_input_FNs.csv";
    String outlierDetection_output_FNs = dirName + "outlierDetection_output_FNs.txt";

    public void partitionSFRs() {
        int numOutliers = 0;
        int count = 0;
        try {
            for (NaturalBreak nb : naturalBreaks) {

                ArrayList<SFR> trainingSFRs = new ArrayList<>();

                FileWriter trainingFileWriter = new FileWriter(outlierDetection_input_training);
                BufferedWriter trainingBufferedWriter = new BufferedWriter(trainingFileWriter);

                FileWriter detectionFileWriter = new FileWriter(outlierDetection_input_FNs);
                BufferedWriter detectionBufferedWriter = new BufferedWriter(detectionFileWriter);

                int countTraining = 0;
                Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesSessions = rolesTrainingSessions.entrySet();
                for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesSessions) {
                    ArrayList<SFR> SFRs = sessionEntry.getValue();
                    for (int i = 0; i < SFRs.size(); i++) {
                        SFR sfr = SFRs.get(i);
                        if (sfr.getLength() >= nb.getStart() && sfr.getLength() < nb.getEnd()) {
                            if (i < portion * SFRs.size()) {
                                trainingBufferedWriter.write(sfr.composeSFR_str(tablesNames) + "\n");
                                countTraining++;
                                trainingSFRs.add(sfr);
                            }
                        }
                    }
                }

                trainingBufferedWriter.close();
                trainingFileWriter.close();

                System.out.println("Training: " + countTraining);

                ArrayList<String> diffs = new ArrayList<>();
                ArrayList<Integer> outliers = new ArrayList<>();

                int currCount = 0;
                Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesDetectionSessions = rolesDetectionSessions.entrySet();
                for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesDetectionSessions) {
                    ArrayList<SFR> SFRs = sessionEntry.getValue();
                    for (SFR sfr : SFRs) {
                        if (sfr.getLength() >= nb.getStart() && sfr.getLength() < nb.getEnd()) {
                            detectionBufferedWriter.write(sfr.composeSFR_str(tablesNames) + "\n");
                            currCount++;
                            SFR_Diff diff = sfr.getDiff(trainingSFRs, tablesNames, tablesCards);
                            diffs.add(diff.toString());
                        }
                    }
                }
                detectionBufferedWriter.close();
                detectionFileWriter.close();

                if (currCount > 0) {
                    numOutliers += runOutlierDetection(outliers);
                    count += currCount;
                }

                for (int i = 0; i < diffs.size(); i++) {
                    System.out.print(diffs.get(i) + " " + outliers.get(i) + "\n");
                }
            }

            System.out.println("--" + numOutliers + "/" + count);
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SE_withIntervals.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    private int runOutlierDetection(ArrayList<Integer> outliers) {
        Runtime rt = Runtime.getRuntime();
        try {
            Process pr = rt.exec("python /home/asma/Desktop/PG_logs/seats/SE/detectOutliers.py 0.0001 " + outlierDetection_input_training + " " + outlierDetection_input_FNs + " " + outlierDetection_output_FNs);
            pr.waitFor();

            FileReader fileReader = new FileReader(outlierDetection_output_FNs);

            int numOutliers = 0;

            int count = 0;
            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                int result = Integer.parseInt(line);
                boolean isOutlier;
                isOutlier = (result == -1);
                if (isOutlier) {
                    numOutliers++;
                    outliers.add(1);
                } else {
                    outliers.add(0);
                }
                count++;
            }

            bufferedReader.close();

            System.out.println("-----" + numOutliers + "/" + count);

            return numOutliers;

        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(Clustering_python.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public static void main(String[] args) {

//        String datasetName = "seats";
//        String dirName = "/home/asma/Desktop/Seats_logs/";
//
//        SE_withIntervals se = new SE_withIntervals();
//
//        se.addFile_training(1, dirName + "training_1.csv", datasetName);
//        se.addFile_training(2, dirName + "training_2.csv", datasetName);
//        se.addFile_training(3, dirName + "training_3.csv", datasetName);
//        se.addFile_training(4, dirName + "training_4.csv", datasetName);
//
//        se.addFile_detection(1, dirName + "detection_1_1.csv", datasetName);
//        se.addFile_detection(1, dirName + "detection_1_2.csv", datasetName);
//        se.addFile_detection(2, dirName + "detection_2.csv", datasetName);
//        se.addFile_detection(3, dirName + "detection_3.csv", datasetName);
//        se.addFile_detection(4, dirName + "detection_4.csv", datasetName);

        String datasetName = "tpcc";
        String dirName = "/home/asma/Desktop/tpcc_logs/";
        
        SE_withIntervals se = new SE_withIntervals();
        
        se.addFile_training(1, dirName + "training_1.csv", datasetName);
        se.addFile_training(2, dirName + "training_2.csv", datasetName);
        se.addFile_training(3, dirName + "training_3.csv", datasetName);
        se.addFile_training(4, dirName + "training_4.csv", datasetName);
        se.addFile_training(5, dirName + "training_5.csv", datasetName);

        se.addFile_detection(1, dirName + "detection_NO_1.csv", datasetName);
        se.addFile_detection(1, dirName + "detection_NO_2.csv", datasetName);
        se.addFile_detection(1, dirName + "detection_NO_3.csv", datasetName);
        se.addFile_detection(1, dirName + "detection_NO_4.csv", datasetName);
        se.addFile_detection(1, dirName + "detection_NO_5.csv", datasetName);
        se.addFile_detection(1, dirName + "detection_NO_6.csv", datasetName);
        se.addFile_detection(1, dirName + "detection_NO_7.csv", datasetName);
            
        se.addFile_detection(2, dirName + "detection_payment_1.csv", datasetName);
        se.addFile_detection(2, dirName + "detection_payment_2.csv", datasetName);
            
        se.addFile_detection(3, dirName + "detection_os_1.csv", datasetName);
        se.addFile_detection(3, dirName + "detection_os_2.csv", datasetName);
        se.addFile_detection(3, dirName + "detection_os_3.csv", datasetName);
        se.addFile_detection(3, dirName + "detection_os_4.csv", datasetName);
            
        se.addFile_detection(4, dirName + "detection_delivery_1.csv", datasetName);
        se.addFile_detection(4, dirName + "detection_delivery_2.csv", datasetName);
        se.addFile_detection(4, dirName + "detection_delivery_3.csv", datasetName);
        se.addFile_detection(4, dirName + "detection_delivery_4.csv", datasetName);
        se.addFile_detection(5, dirName + "detection_sl.csv", datasetName);
        se.findTrainingIntervals();
        se.detectOutliers();

    }

    void getDistOfLengths() {

        ArrayList<Float> lengthMultiples = new ArrayList<>();
        float lastBreak = breaksIntervals.get(breaksIntervals.size() - 1);

        Set<Map.Entry<Integer, ArrayList<SFR>>> allRolesDetectionSessions = rolesDetectionSessions.entrySet();
        for (Map.Entry<Integer, ArrayList<SFR>> sessionEntry : allRolesDetectionSessions) {
            ArrayList<SFR> SFRs = sessionEntry.getValue();
            for (SFR sfr : SFRs) {
                System.out.println(sfr.getLength() / lastBreak);
            }
        }
    }

    private void findFalsePositiveSessionsLengths() {
        int anomalies = 0;
        for (Long detectionSessionsLengths_ToCheckFalsePositive : detectionSessionsLengths_ToCheckFalsePositives) {
            if (detectionSessionsLengths_ToCheckFalsePositive > breaksIntervals.get(breaksIntervals.size() - 1)) {
                anomalies++;
            }
        }
        System.out.println(anomalies + "/" + detectionSessionsLengths_ToCheckFalsePositives.size());
    }
}
