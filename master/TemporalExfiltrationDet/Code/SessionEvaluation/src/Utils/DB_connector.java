/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author asma
 */
public class DB_connector {

    String userName = "postgres";
    String password = "mlfas.com";

    Connection connection = null;
    
    public DB_connector(String datasetName) {
        try {
            makeConnection(datasetName);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void makeConnection(String datasetName) throws SQLException {

        try {
            Class.forName("org.postgresql.Driver");

            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/" + datasetName, userName,
                    password);

            if (connection == null) {
                System.err.println("Failed to make connection!");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    
    public int executeQuery_getCount(String query) {
        try {
            Statement stmt = connection.createStatement();
            //query = query.replaceAll("user", "\"user\"");
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public int analyzeStmt_getCount(String query) {
        try {
            Statement stmt = connection.createStatement();
            //query = query.replaceAll("user", "\"user\"");
            ResultSet rs = stmt.executeQuery("explain " + query);
            rs.next();
            String result = rs.getString(1);
            String numRowsStr = result.substring(result.indexOf("rows=") + 5, result.indexOf(" ", result.indexOf("rows="))).trim();
            int numRows = Integer.parseInt(numRowsStr);
            return numRows;    
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public int executeUpdate(String query) {
        try {
            query = query.replaceAll("\"\"", "\"");
            //query = query.replaceAll("user", "\"user\"");
            Statement stmt = connection.createStatement();
            int numRowsAffected = stmt.executeUpdate(query);
            return numRowsAffected;    
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public void executeAnalyze() {
        try {
            Statement stmt = connection.createStatement();
            stmt.execute("analyze");
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void executeAnalyze(String tableName) {
        try {
            Statement stmt = connection.createStatement();
            if(tableName.equals("user")) {
                stmt.execute("analyze \"" + tableName + "\"");
            } else {
                stmt.execute("analyze " + tableName);
            }
            
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public int getNumTableRows(String tableName) {
        try {
            Statement stmt = connection.createStatement();
            String query = "Select count(*) from " + tableName;
            ResultSet rs = stmt.executeQuery(query);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

}
