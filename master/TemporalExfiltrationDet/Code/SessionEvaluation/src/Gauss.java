/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author asma
 */
public class Gauss {
    
    
    public static void doGaussianElimination(double A[][]) {
        printA(A);
        
        int nFillIns = 0;
        
        for(int i = 0; i < A.length - 1; i++) {
            int fillins[][] = new int[A.length][A[0].length];
            for(int j = i + 1; j < A.length; j++) {
                if(A[j][i] != 0) {
                    double multiplier = A[i][i]/ A[j][i];
                    
                    for(int k = i; k < A[0].length; k++) {

                        A[j][k] = A[j][k]  * multiplier - A[i][k];
                        if(A[j][k] != 0) {
                            nFillIns++;
                            fillins[j][k] = 1;
                            
                        } 

                    }
                }
            }
            printFillins(fillins);
            printA(A);
            
        }
        
        System.out.println("N_Fillins = " + nFillIns);
    }
    
    public static void main(String[] strs) {
        //double A[][] ={{1, 5, 7}, {-2, -7, -5}};
        double A[][] = //{{1, -3, 1, 4}, {2, -8, 8, -2}, {-6, 3, -15, 9}};
                
        {{2, 0, 1, 0, 0, 0, 0, 1, 0, 0},
            {1, 2, 0, 0, 0, 0, 1, 0, 0, 0},
            {0, 1, 2, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 2, 1, 1, 0, 0, 0, 0},
            {0, 0, 0, 1, 2, 0, 1, 1, 0, 0},
            {0, 0, 0, 0, 0, 2, 0, 0, 1, 0},
            {1, 0, 0, 1, 0, 0, 2, 1, 0, 1},
            {0, 0, 0, 0, 1, 1, 0, 2, 0, 0},
            {1, 1, 0, 0, 1, 0, 0, 1, 2, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 1, 2}
        };
        Gauss.doGaussianElimination(A);
        
    }

    private static void printA(double[][] A) {
        System.out.println("------------------------------");
        for(int i = 0; i < A.length; i++) {
            for(int j = 0; j < A[0].length; j++) {
                System.out.print(A[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("------------------------------");
    }

    private static void printFillins(int[][] fillins) {
        for(int i = 0; i < fillins.length; i++) {
            for(int j = 0; j < fillins[0].length; j++) {
                System.out.print(fillins[i][j] + " ");
            }
            System.out.println();
        }
    }
    
}
