/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.ArrayList;

/**
 *
 * @author asma
 */
public class TablesCombinationData_Detection {

    ArrayList<String> tablesNames;
    String tablesNamesStr;
    
    float selectivityThreshold;

    float currentSel;

    int numRowsCartesian;

    String combinedPredicates;

    DB_connector dB_connector;
    
    boolean isActualAnomaly = false;
    boolean isAnomalous = false;
    
    float remainingSel = 0;

    int numQueriesTillAnomaly = 0;
    boolean countQueriesTillAnomaly = false;
    float anomalousQueriesSel = 0;
    
    int numAnomaliesTillLastCheck = 0;

    public int getNumQueriesTillAnomaly() {
        return numQueriesTillAnomaly;
    }
    
    public float getAnomalousQueriesSel() {
        return anomalousQueriesSel;
    }
    
    public TablesCombinationData_Detection(DB_connector dB_connector, ArrayList<String> tablesNames, float selThreshold, String tablesNamesStr) {
        this.tablesNames = tablesNames;
        this.dB_connector = dB_connector;

        for (String tableName : tablesNames) {
            String query = "select count(*) from " + tableName;
            query = query.replaceAll("\"\"", "\"");
            numRowsCartesian = dB_connector.executeQuery_getCount(query);
        }

        combinedPredicates = "";
        this.selectivityThreshold = selThreshold;
        this.tablesNamesStr = tablesNamesStr;
    }

    public TablesCombinationData_Detection(ArrayList<String> tablesNames) {
        this.tablesNames = tablesNames;
    }

    public void setConnector(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    public float isAnomalous(String predicate) {
//        if(isAnomalous) {
//            return true;
//        }
        float anomalyDegree = 0;
        String query;
        float sel;
        query = "select * from " + tablesNamesStr + " where " + predicate;
        query = query.replaceAll("\"\"", "\"");
        int count = dB_connector.analyzeStmt_getCount(query);
        sel = (float) count / numRowsCartesian;
        
        if (currentSel < selectivityThreshold) {
            currentSel += sel;
            return anomalyDegree;
        } else { // get the actual selectivity
            int rowCount;
            if(combinedPredicates.isEmpty()) {
                rowCount = 0;
            } else {
                query = "select count(*) from " + tablesNamesStr + " where " + combinedPredicates;
                query = query.replaceAll("\"\"", "\"");
                rowCount = dB_connector.executeQuery_getCount(query);
            }
            float totalSel = (float)rowCount / numRowsCartesian;
            
            if(totalSel <= selectivityThreshold) {
                currentSel = totalSel + sel;
                
                if(countQueriesTillAnomaly) {
                    numQueriesTillAnomaly++;
                    anomalousQueriesSel += sel;
                }
                
                return anomalyDegree;
            } else {
                anomalyDegree = (totalSel - selectivityThreshold) / selectivityThreshold;
                
                if(anomalyDegree > 1) {
                    System.out.println();
                }
                
                numAnomaliesTillLastCheck++;
                
                isAnomalous = true;
                return anomalyDegree;
            }
        }
    }
    
    public String getCombinedPredicate(String predicate) {
        String p = "";
        if (!combinedPredicates.equals("")) {
            p = combinedPredicates + " OR ";
        }
        p = p + "(" + predicate + ")";
        return p;
    }

    public void addPredicate(String predicate) {
        if (!combinedPredicates.equals("")) {
            combinedPredicates = combinedPredicates + " OR ";
        }
        combinedPredicates = combinedPredicates + "(" + predicate + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TablesCombinationData_Detection) {
            TablesCombinationData_Detection other = (TablesCombinationData_Detection) obj;
            ArrayList<String> tables1 = this.tablesNames;
            ArrayList<String> tables2 = other.getTablesNames();
            if (tables1.size() != tables2.size()) {
                return false;
            }
            for (String table1 : tables1) {
                if (!tables2.contains(table1)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private ArrayList<String> getTablesNames() {
        return tablesNames;
    }

    float epsilon = 1e-5f;
    public boolean isActualAnomaly(String predicate) {
        
        if(isActualAnomaly) {
            return true;
        }
        
        String p = getCombinedPredicate(predicate);
        String query = "select count(*) from " + tablesNamesStr + " where " + p;
        query = query.replaceAll("\"", "");
        int rowCount = dB_connector.executeQuery_getCount(query);
        float totalSel = (float)rowCount / numRowsCartesian;
        
        float pastSel;
        if(!combinedPredicates.isEmpty()) {
            query = "select count(*) from " + tablesNamesStr + " where " + combinedPredicates;
            query = query.replaceAll("\"", "");
            rowCount = dB_connector.executeQuery_getCount(query);
        } else {
            rowCount = 0;
        }
        
        pastSel = (float)rowCount / numRowsCartesian;
        
        boolean result = totalSel - pastSel > epsilon;
        
        if(result) {
            isActualAnomaly = true;
        }
        
        return result;
    }

    public void recordSel() {
        String query;
        int rowCount;
        if(!combinedPredicates.isEmpty()) {
            query = "select count(*) from " + tablesNamesStr + " where " + combinedPredicates;
            query = query.replaceAll("\"", "");
            rowCount = dB_connector.executeQuery_getCount(query);
        } else {
            rowCount = 0;
        }
        
        float sel = (float)rowCount / numRowsCartesian;
        
        remainingSel = selectivityThreshold - sel;
        
        countQueriesTillAnomaly = true;
    }
    
    public float getRemainingSel() {
       return remainingSel;
    }
    
    public int numRowsCartesian() {
        return numRowsCartesian;
    }
}
