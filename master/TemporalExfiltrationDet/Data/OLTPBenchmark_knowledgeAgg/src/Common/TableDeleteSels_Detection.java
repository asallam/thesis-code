/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;
/**
 *
 * @author asma
 */
public class TableDeleteSels_Detection {
    String tableName;
    int maxDeletes;
    int numDeletesSoFar;
    
    DB_connector dB_connector;
    
    float remainingSel;
    int tableRowCount;

    public TableDeleteSels_Detection(DB_connector dB_connector, String tablesName, int maxDeletes) {
        this.tableName = tablesName;
        this.dB_connector = dB_connector;
        this.maxDeletes = maxDeletes;
    }

    public TableDeleteSels_Detection(String tableName) {
        this.tableName = tableName;
    }

    public void setConnector(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    public boolean checkAnomaly(String query) {
        return numDeletesSoFar >= maxDeletes;
    }
    
    public int executeQuery(String query) {
        int count = dB_connector.executeUpdate(query);
        numDeletesSoFar += count;
        return count;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TableDeleteSels_Detection) {
            TableDeleteSels_Detection other = (TableDeleteSels_Detection) obj;
            String table1 = this.tableName;
            String table2 = other.getTableName();
           
            return table1.equals(table2);
            
        } else {
            return false;
        }
    }

    private String getTableName() {
        return tableName;
    }
    
    public void recordSel() {
        tableRowCount = dB_connector.getNumTableRows(tableName);
        remainingSel = (float)(maxDeletes - numDeletesSoFar) / tableRowCount;
    }

    public int getTableRowCount() {
        return tableRowCount;
    }
    
    public float getRemainingSel() {
        return remainingSel;
    }
}
