/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import Common.DB_connector;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asma
 */
public class TableInsertSels_Training {
    String tableName;
    HashMap<String, Integer> usersInserts;
    int maxInserts;
    DB_connector dB_connector;
    
    public TableInsertSels_Training(DB_connector dB_connector, String tableName) {
        this.tableName = tableName;
        usersInserts = new HashMap<>();
        this.dB_connector = dB_connector;
    }
    
    public void addOne(String UID) {
        Integer userInserts = usersInserts.get(UID);
        if (userInserts == null) {
            userInserts = 1;
            usersInserts.put(UID, userInserts);
        } else {
            userInserts++;
            usersInserts.replace(UID, userInserts);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TableInsertSels_Training) {
            TableInsertSels_Training other = (TableInsertSels_Training) obj;
            String table1 = this.tableName;
            String table2 = other.getTableName();
            return table1.equals(table2);
        } else {
            return false;
        }
    }

    public String getTableName() {
        return tableName;
    }
       
    public void computeMax() {
        for (Map.Entry pair : usersInserts.entrySet()) {
            int numInserts = (int) pair.getValue();
            if(maxInserts < numInserts) {
                maxInserts = numInserts;
            }
        }
    }
    
    public int getMaxInserts() {
        return maxInserts;
    }

    void addSel(String UID, String query) {
        int count = dB_connector.executeUpdate(query);
        Integer userInserts = usersInserts.get(UID);
        if (userInserts == null) {
            usersInserts.put(UID, count);
        } else {
            usersInserts.replace(UID, userInserts + count);
        }
        
    }

    DB_connector getConnector() {
        return dB_connector;
    }
    
}
