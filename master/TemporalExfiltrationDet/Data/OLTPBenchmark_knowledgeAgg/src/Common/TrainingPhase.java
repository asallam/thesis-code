/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asma
 */
public class TrainingPhase {

    HashMap<String, ArrayList<String>> usersQueries;
    ArrayList<String> UIDs;
    ArrayList<String> queries;

    ArrayList<TablesCombinationData_Training> selectTablesCombinationDataArr = new ArrayList<>();
    ArrayList<TableInsertSels_Training> insertSelsArr = new ArrayList<>();
    ArrayList<TableUpdateSels_Training> updateSelsArr = new ArrayList<>();
    ArrayList<TableDeleteSels_Training> deleteSelsArr = new ArrayList<>();

    DB_connector dB_connector;

    public TrainingPhase(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    public RoleProfile getProfile() {
        return new RoleProfile(selectTablesCombinationDataArr, insertSelsArr, updateSelsArr, deleteSelsArr);
    }

    public void doTraining(ArrayList<String> UIDs, ArrayList<String> queries, HashMap<String, ArrayList<String>> usersQueries) {

        this.UIDs = UIDs;
        this.queries = queries;
        this.usersQueries = usersQueries;

        organizeQueries(dB_connector);
        for (TablesCombinationData_Training c : selectTablesCombinationDataArr) {
            c.computeThreshold();
        }

        for (TableInsertSels_Training s : insertSelsArr) {
            s.computeMax();
        }

        for (TableDeleteSels_Training s : deleteSelsArr) {
            s.computeMax();
        }

        for (TableUpdateSels_Training s : updateSelsArr) {
            s.computeMax();
        }

        System.out.println();
    }

    /*wrong*/
    public void organizeQueries1(DB_connector dB_connector) {

        for (Map.Entry pair : usersQueries.entrySet()) {
            String UID = (String) pair.getKey();
            ArrayList<String> queries = (ArrayList<String>) pair.getValue();

            for (String query : queries) {
                String queryLower = query.toLowerCase();
                if (queryLower.startsWith("select")) {
                    String tablesPortion = query.substring(queryLower.indexOf("from") + 4, queryLower.indexOf("where"));
                    tablesPortion = tablesPortion.trim();
                    String tablesArr[] = tablesPortion.split(",");
                    for (int i = 0; i < tablesArr.length; i++) {
                        tablesArr[i] = tablesArr[i].trim();
                    }
                    ArrayList<String> tablesArrL = new ArrayList<>();
                    for (String table : tablesArr) {
                        String tableName = table.trim();
                        tablesArrL.add(tableName);
                    }
                    Collections.sort(tablesArrL.subList(1, tablesArrL.size()));
                    int index = selectTablesCombinationDataArr.indexOf(new TablesCombinationData_Training(tablesArrL));
                    TablesCombinationData_Training tablesCombinationEntry;
                    if (index == -1) {
                        tablesCombinationEntry = new TablesCombinationData_Training(dB_connector, tablesArrL);
                        selectTablesCombinationDataArr.add(tablesCombinationEntry);
                    } else {
                        tablesCombinationEntry = selectTablesCombinationDataArr.get(index);
                    }
                    String predicate;
                    if (queryLower.contains("for update")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("for update")).trim();
                    } else if (queryLower.contains("order by")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("order by")).trim();
                    } else {
                        predicate = query.substring(queryLower.indexOf("where") + 5).trim();
                    }

                    tablesCombinationEntry.addPredicate(UID, predicate);
                } else if (queryLower.startsWith("insert")) {
                    int indexOfBracket = queryLower.indexOf("(");
                    int indexOfValues = queryLower.indexOf("values");
                    String tablePortion;
                    if (indexOfBracket < indexOfValues) {
                        tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfBracket).trim();
                    } else {
                        tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfValues).trim();
                    }
                    int index = insertSelsArr.indexOf(new TableInsertSels_Training(dB_connector, tablePortion));
                    TableInsertSels_Training insertSelsEntry;
                    if (index == -1) {
                        insertSelsEntry = new TableInsertSels_Training(dB_connector, tablePortion);
                        insertSelsArr.add(insertSelsEntry);
                    } else {
                        insertSelsEntry = insertSelsArr.get(index);
                    }
                    insertSelsEntry.addSel(UID, query);

                } else if (queryLower.startsWith("delete")) {
                    String tablePortion;
                    int indexOfWhere = queryLower.indexOf("where");
                    if (indexOfWhere != -1) {
                        tablePortion = query.substring(queryLower.indexOf("delete from") + 11, indexOfWhere).trim();
                    } else {
                        tablePortion = query.substring(queryLower.indexOf("delete from") + 11).trim();
                    }
                    int index = deleteSelsArr.indexOf(new TableDeleteSels_Training(tablePortion));
                    TableDeleteSels_Training deleteSelsEntry;
                    if (index == -1) {
                        deleteSelsEntry = new TableDeleteSels_Training(dB_connector, tablePortion);
                        deleteSelsArr.add(deleteSelsEntry);
                    } else {
                        deleteSelsEntry = deleteSelsArr.get(index);
                    }

                    String predicate;
                    if (indexOfWhere != -1) {
                        predicate = query.substring(indexOfWhere + 5).trim();
                    } else {
                        predicate = "1";
                    }
                    deleteSelsEntry.addSel(UID, query);
                } else if (queryLower.startsWith("update")) {
                    String tablePortion;
                    tablePortion = query.substring(queryLower.indexOf("update") + 6, queryLower.indexOf("set")).trim();
                    int index = updateSelsArr.indexOf(new TableUpdateSels_Training(dB_connector, tablePortion));
                    TableUpdateSels_Training updateSelsEntry;
                    if (index == -1) {
                        updateSelsEntry = new TableUpdateSels_Training(dB_connector, tablePortion);
                        updateSelsArr.add(updateSelsEntry);
                    } else {
                        updateSelsEntry = updateSelsArr.get(index);
                    }
                    int indexOfWhere = queryLower.indexOf("where");
                    String predicate;
                    if (indexOfWhere != -1) {
                        predicate = query.substring(indexOfWhere + 5).trim();
                    } else {
                        predicate = "1";
                    }
                    updateSelsEntry.addSel(UID, query);
                }
            }
        }
        System.out.println();
    }

    public void organizeQueries(DB_connector dB_connector) {

        for (int i = 0; i < queries.size(); i++) {

            String UID = UIDs.get(i);
            String query = queries.get(i);
            //System.out.println(UID + ": " + query);

            String queryLower = query.toLowerCase();
            if (queryLower.startsWith("select")) {
                String tablesPortion = query.substring(queryLower.indexOf(" from") + 5, queryLower.indexOf("where"));
                tablesPortion = tablesPortion.trim();
                String tablesArr[] = tablesPortion.split(",");
                ArrayList<String> tablesArrayList = new ArrayList<>();
                for (String table : tablesArr) {
                    String tableName = table.trim();
                    tablesArrayList.add(tableName);
                }
                int index = selectTablesCombinationDataArr.indexOf(new TablesCombinationData_Training(tablesArrayList));
                TablesCombinationData_Training tablesCombinationEntry;
                if (index == -1) {
                    tablesCombinationEntry = new TablesCombinationData_Training(dB_connector, tablesArrayList);
                    selectTablesCombinationDataArr.add(tablesCombinationEntry);
                } else {
                    tablesCombinationEntry = selectTablesCombinationDataArr.get(index);
                }
                String predicate;
                if (queryLower.contains("for update")) {
                    predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("for update")).trim();
                } else if (queryLower.contains("order by")) {
                    predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("order by")).trim();
                } else {
                    predicate = query.substring(queryLower.indexOf("where") + 5).trim();
                }

                tablesCombinationEntry.addPredicate(UID, predicate);
            } else if (queryLower.startsWith("insert")) {
                int indexOfBracket = queryLower.indexOf("(");
                int indexOfValues = queryLower.indexOf("values");
                String tablePortion;
                if (indexOfBracket < indexOfValues) {
                    tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfBracket).trim();
                } else {
                    tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfValues).trim();
                }
                int index = insertSelsArr.indexOf(new TableInsertSels_Training(dB_connector, tablePortion));
                TableInsertSels_Training insertSelsEntry;
                if (index == -1) {
                    insertSelsEntry = new TableInsertSels_Training(dB_connector, tablePortion);
                    insertSelsArr.add(insertSelsEntry);
                } else {
                    insertSelsEntry = insertSelsArr.get(index);
                }
                insertSelsEntry.addSel(UID, query);

            } else if (queryLower.startsWith("delete")) {
                String tablePortion;
                int indexOfWhere = queryLower.indexOf("where");
                if (indexOfWhere != -1) {
                    tablePortion = query.substring(queryLower.indexOf("delete from") + 11, indexOfWhere).trim();
                } else {
                    tablePortion = query.substring(queryLower.indexOf("delete from") + 11).trim();
                }
                int index = deleteSelsArr.indexOf(new TableDeleteSels_Training(tablePortion));
                TableDeleteSels_Training deleteSelsEntry;
                if (index == -1) {
                    deleteSelsEntry = new TableDeleteSels_Training(dB_connector, tablePortion);
                    deleteSelsArr.add(deleteSelsEntry);
                } else {
                    deleteSelsEntry = deleteSelsArr.get(index);
                }

                String predicate;
                if (indexOfWhere != -1) {
                    predicate = query.substring(indexOfWhere + 5).trim();
                } else {
                    predicate = "1";
                }
                deleteSelsEntry.addSel(UID, query);
            } else if (queryLower.startsWith("update")) {
                String tablePortion;
                tablePortion = query.substring(queryLower.indexOf("update") + 6, queryLower.indexOf("set")).trim();
                int index = updateSelsArr.indexOf(new TableUpdateSels_Training(dB_connector, tablePortion));
                TableUpdateSels_Training updateSelsEntry;
                if (index == -1) {
                    updateSelsEntry = new TableUpdateSels_Training(dB_connector, tablePortion);
                    updateSelsArr.add(updateSelsEntry);
                } else {
                    updateSelsEntry = updateSelsArr.get(index);
                }
                int indexOfWhere = queryLower.indexOf("where");
                String predicate;
                if (indexOfWhere != -1) {
                    predicate = query.substring(indexOfWhere + 5).trim();
                } else {
                    predicate = "1";
                }
                updateSelsEntry.addSel(UID, query);
            }
        }

    }
}
