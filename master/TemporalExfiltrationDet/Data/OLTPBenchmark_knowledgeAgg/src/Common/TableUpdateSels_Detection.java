/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import Common.DB_connector;


/**
 *
 * @author asma
 */
public class TableUpdateSels_Detection {

    String tableName;
    int maxUpdates;
    
    int numUpdatesSoFar;
    
    DB_connector dB_connector;
    
    float remainingSel;
    int tableRowCount;

    public int getTableRowCount() {
        return tableRowCount;
    }

    public TableUpdateSels_Detection(DB_connector dB_connector, String tablesName, int maxUpdates) {
        this.tableName = tablesName;
        this.dB_connector = dB_connector;
        this.maxUpdates = maxUpdates;
    }

    public TableUpdateSels_Detection(String tableName) {
        this.tableName = tableName;
    }

    public void setConnector(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    public boolean checkAnomaly(String query) {
        if(maxUpdates == 0 && numUpdatesSoFar == 0) {
            return false;
        }
        return numUpdatesSoFar >= maxUpdates;
    }
    
    public int executeQuery(String query) {
        int count = dB_connector.executeUpdate(query);
        numUpdatesSoFar += count;
        return count;
    }
    
    

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TableUpdateSels_Detection) {
            TableUpdateSels_Detection other = (TableUpdateSels_Detection) obj;
            String table1 = this.tableName;
            String table2 = other.getTableName();
           
            return table1.equals(table2);
            
        } else {
            return false;
        }
    }

    private String getTableName() {
        return tableName;
    }
    
    public int getNumUpdatesSoFar() {
        return numUpdatesSoFar;
    }
    
    public void recordSel() {
        tableRowCount = dB_connector.getNumTableRows(tableName);
        remainingSel = (float)(maxUpdates - numUpdatesSoFar) / tableRowCount;
    }
    
    public float getRemainingSel() {
        return remainingSel;
    }
}
