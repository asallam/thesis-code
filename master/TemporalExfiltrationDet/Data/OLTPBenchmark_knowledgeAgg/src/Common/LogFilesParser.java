/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author asma
 */
public class LogFilesParser {
    HashMap<String, ArrayList<String>> usersQueries;
    
    ArrayList<String> UIDs;
    ArrayList<String> queries;
    
    public LogFilesParser() {
        usersQueries = new HashMap<>();
        
        UIDs = new ArrayList<>();
        queries = new ArrayList<>();
    }
    
    public HashMap<String, ArrayList<String>> getUsersQueries() {
        return usersQueries;
    }
    
    public void parseTrainingFiles(String dirName) {
        
        File folder = new File(dirName);
        File[] listOfFiles = folder.listFiles();

        for (File oneFile : listOfFiles) {
            if (oneFile.isFile()) {
                String fileName = oneFile.getName();
                if(fileName.endsWith(".csv")) {
                    parseFile(dirName + "/" + oneFile.getName());
                }
            }
        }
    }
    
    public void parseFile(String fileName) {
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
        String line = br.readLine();
        while (line != null) {
            
            
            if(line.contains("\",\"parameters:")) {
                String[] lineParts = line.split(",");
                String UID = lineParts[3];
                String query = composeQuery(line);
                if(query != null) {
                    
                    ArrayList<String> userQueries = usersQueries.get(UID);
                    if(userQueries == null) {
                        userQueries = new ArrayList<>();
                        usersQueries.put(UID, userQueries);
                    }
                    userQueries.add(query.toLowerCase());

                    UIDs.add(UID);
                    queries.add(query.toLowerCase());
                }
            }
            line = br.readLine();
        }
    
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    private String composeQuery(String line) {
        String query = line.substring(line.indexOf(":", line.indexOf("execute ") + 8) + 2, line.indexOf("\",\"parameters:"));
        query = query.replaceAll("\"", "");
        
        
        String allParams = line.substring(line.indexOf("parameters:"), line.indexOf("\",", line.indexOf("parameters:")));
        allParams = allParams.replaceAll("parameters:", "");
        System.out.println(allParams);
        
        String[] parametersSplit = allParams.split(", ");
        for (int i = parametersSplit.length - 1; i >= 0; i--) {
            String[] keyValue = parametersSplit[i].split("=");
            keyValue[0] = keyValue[0].trim();
            keyValue[0] = keyValue[0].replaceAll("\\$", "__");
            
            /*keyValue[1] = keyValue[1].replaceAll("\\$", "0");
            keyValue[1] = keyValue[1].replaceAll("\"", "1");
            keyValue[1] = keyValue[1].replaceAll("'", "2");
            keyValue[1] = "'" + keyValue[1] + "'";*/
            keyValue[1] = keyValue[1].trim();
            
            query = query.replaceAll("\\$", "__");
            
            query = query.replaceAll(keyValue[0], keyValue[1]);
        }
        
        
        
        return query;
    }

    public ArrayList<String> getUIDs() {
        return UIDs;
    }

    public ArrayList<String> getAllQueries() {
        return queries;
    }
    
}
