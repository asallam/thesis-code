/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;


/**
 *
 * @author asma
 */
public class TableChanges {
    String tableName;
    int numChanges;
    int numRows;
    
    int numFalsePositives;
    int numFalseNegatives;
    
    DB_connector dB_connector;

    public TableChanges(String tableName, DB_connector dB_connector) {
        this.tableName = tableName;
        this.numRows = dB_connector.getNumTableRows(tableName);
        this.dB_connector = dB_connector;
    }
    
    public boolean needsUpdates() {
        return numChanges >= 0.01f * numRows;
    }
    
    public void applyChanges() {
        dB_connector.executeAnalyze(tableName);
        numChanges = 0;
        numRows = dB_connector.getNumTableRows(tableName);
    }
    
    public void addChanges(int numNewChanges) {
        this.numChanges += numNewChanges;
    }
    
    public void checkAndExecute() {
        if(needsUpdates()) {
            applyChanges();
        }
    }
    
    public void addFP() {
        numFalsePositives++;
    }
    
    public void addFN() {
        numFalseNegatives++;
    }

    public int getFPs() {
        return numFalsePositives;
    }

    public int getFNs() {
        return numFalseNegatives;
    }
    
    
    
}
