/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import java.util.ArrayList;

/**
 *
 * @author asma
 */
public class RoleProfile {
    ArrayList<TablesCombinationData_Training> selectTablesCombinationDataArr = new ArrayList<>();
    ArrayList<TableInsertSels_Training> insertSelsArr = new ArrayList<>();
    ArrayList<TableUpdateSels_Training> updateSelsArr = new ArrayList<>();
    ArrayList<TableDeleteSels_Training> deleteSelsArr = new ArrayList<>();
    
    public RoleProfile(ArrayList<TablesCombinationData_Training> selectTablesCombinationDataArr,
            ArrayList<TableInsertSels_Training> insertSelsArr,
            ArrayList<TableUpdateSels_Training> updateSelsArr,
            ArrayList<TableDeleteSels_Training> deleteSelsArr) {
        this.selectTablesCombinationDataArr = selectTablesCombinationDataArr;
        this.insertSelsArr = insertSelsArr;
        this.deleteSelsArr = deleteSelsArr;
        this.updateSelsArr = updateSelsArr;
    }

    public ArrayList<TablesCombinationData_Training> getSelectProfile() {
        return selectTablesCombinationDataArr;
    }
    
    public ArrayList<TableInsertSels_Training> getInsertProfile() {
        return insertSelsArr;
    }

    public ArrayList<TableUpdateSels_Training> getUpdateProfile() {
        return updateSelsArr;
    }

    public ArrayList<TableDeleteSels_Training> getDeleteProfile() {
        return deleteSelsArr;
    }
   
}
