/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import Common.DB_connector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asma
 */
public class TableUpdateSels_Training {

    String tableName;
    int maxUpdates;
    HashMap<String, Integer> usersUpdates;
    DB_connector dB_connector;

    public TableUpdateSels_Training(DB_connector dB_connector, String tablesName) {
        this.tableName = tablesName;
        this.dB_connector = dB_connector;
        usersUpdates = new HashMap<>();
    }

    public TableUpdateSels_Training(String tableName) {
        this.tableName = tableName;
    }

    public void setConnector(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    public void computeMax() {
        for (Map.Entry pair : usersUpdates.entrySet()) {
            int numUpdates = (int) pair.getValue();
            if(maxUpdates < numUpdates) {
                maxUpdates = numUpdates;
            }
        }
    }

    void addSel(String UID, String query) {
        int count = dB_connector.executeUpdate(query);
        Integer userUpdates = usersUpdates.get(UID);
        if (userUpdates == null) {
            usersUpdates.put(UID, count);
        } else {
            usersUpdates.replace(UID, userUpdates + count);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TableUpdateSels_Training) {
            TableUpdateSels_Training other = (TableUpdateSels_Training) obj;
            String table1 = this.tableName;
            String table2 = other.getTableName();
           
            return table1.equals(table2);
            
        } else {
            return false;
        }
    }

    public String getTableName() {
        return tableName;
    }

    public int getMaxUpdates() {
        return maxUpdates;
    }

    public DB_connector getConnector() {
        return dB_connector;
    }
}
