/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class Histogram {
    public static void main(String args[]) {
        String fileName = "/home/asma/Desktop/PG_logs/tpcc/FPs1.txt";
        
        ArrayList<Integer> vals = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            while(line != null) {
                int num = Integer.parseInt(line);
                vals.add(num);
                line = br.readLine();
            }
            
            int binLength = vals.size() / 4;
            System.out.println(binLength);
            int i;
            for(i = 0; i < vals.size(); i += binLength) {
                int sum = 0;
                int j;
                for(j = i; j < i + binLength && j < vals.size(); j++) {
                    sum += vals.get(j);
                }
                int numQueries = j - i;
                float percentage = (float)sum/numQueries *100;
                System.out.println(percentage);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Histogram.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Histogram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
