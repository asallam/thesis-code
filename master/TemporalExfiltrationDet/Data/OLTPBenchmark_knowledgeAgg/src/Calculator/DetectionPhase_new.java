/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculator;

import java.util.Arrays;
import Common.RoleProfile;
import Common.TableDeleteSels_Training;
import Common.TableInsertSels_Training;
import Common.TableUpdateSels_Detection;
import Common.TableDeleteSels_Detection;
import Common.TablesCombinationData_Training;
import Common.TableInsertSels_Detection;
import Common.TableUpdateSels_Training;
import Common.TablesCombinationData_Detection;
import Common.TableChanges;
import Common.DB_connector;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class DetectionPhase_new {

    HashMap<String, HashMap<String, TablesCombinationData_Detection>> tablesInfo_detection;
    HashMap<String, HashMap<String, TableDeleteSels_Detection>> tablesDeleteSels;
    HashMap<String, HashMap<String, TableInsertSels_Detection>> tablesInsertSels;
    HashMap<String, HashMap<String, TableUpdateSels_Detection>> tablesUpdateSels;

    HashMap<String, TableChanges> tablesChanges;

    int numAnomalies = 0;

    int numAnomaliesSelect = 0;
    int numAnomaliesUpdate = 0;
    int numAnomaliesInsert = 0;
    int numAnomaliesDelete = 0;

    int numQueriesSelect = 0;
    int numQueriesUpdate = 0;
    int numQueriesInsert = 0;
    int numQueriesDelete = 0;

    int numQueriesSelect2 = 0;
    int numQueriesUpdate2 = 0;
    int numQueriesInsert2 = 0;
    int numQueriesDelete2 = 0;

    ArrayList<Float> allAnomalyDegrees = new ArrayList<>();

    public int getNumQueriesSelect2() {
        return numQueriesSelect2;
    }

    public int getNumQueriesUpdate2() {
        return numQueriesUpdate2;
    }

    public int getNumQueriesInsert2() {
        return numQueriesInsert2;
    }

    public int getNumQueriesDelete2() {
        return numQueriesDelete2;
    }

    public int getNumQueriesSelect() {
        return numQueriesSelect;
    }

    public int getNumQueriesUpdate() {
        return numQueriesUpdate;
    }

    public int getNumQueriesInsert() {
        return numQueriesInsert;
    }

    public int getNumQueriesDelete() {
        return numQueriesDelete;
    }

    public int getNumAnomaliesSelect() {
        return numAnomaliesSelect;
    }

    public int getNumAnomaliesUpdate() {
        return numAnomaliesUpdate;
    }

    public int getNumAnomaliesInsert() {
        return numAnomaliesInsert;
    }

    public int getNumAnomaliesDelete() {
        return numAnomaliesDelete;
    }

    int numQueries = 0;
    int numQueries2 = 0;

    DB_connector dB_connector;

    public DetectionPhase_new(DB_connector dB_connector, ArrayList<String> UIDs_detection, RoleProfile roleProfile, HashMap<String, TableChanges> tablesChanges) {

        this.tablesChanges = tablesChanges;
        this.dB_connector = dB_connector;

        ArrayList<TablesCombinationData_Training> tablesInfo_training = roleProfile.getSelectProfile();
        ArrayList<TableInsertSels_Training> tablesInserts_training = roleProfile.getInsertProfile();
        ArrayList<TableDeleteSels_Training> tablesDelete_training = roleProfile.getDeleteProfile();
        ArrayList<TableUpdateSels_Training> tablesUpdates_training = roleProfile.getUpdateProfile();

        tablesInfo_detection = new HashMap<>();
        tablesDeleteSels = new HashMap<>();
        tablesInsertSels = new HashMap<>();
        tablesUpdateSels = new HashMap<>();

        for (String UID : UIDs_detection) {
            tablesInfo_detection.put(UID, new HashMap<>());
            tablesDeleteSels.put(UID, new HashMap<>());
            tablesInsertSels.put(UID, new HashMap<>());
            tablesUpdateSels.put(UID, new HashMap<>());
        }

        for (TablesCombinationData_Training t : tablesInfo_training) {
            ArrayList<String> tablesNames = t.getTablesNames();
            float selThreshold = t.getSelThreshold();
            Collections.sort(tablesNames, new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    return s1.compareToIgnoreCase(s2);
                }
            });

            String key = "";
            String tablesNamesStr = "";
            for (String tableName : tablesNames) {
                key = key + tableName;
                if (!tablesNamesStr.isEmpty()) {
                    tablesNamesStr = tablesNamesStr + ",";
                }
                tablesNamesStr = tablesNamesStr + tableName;
            }

            for (String UID : UIDs_detection) {
                HashMap h = tablesInfo_detection.get(UID);
                h.put(key, new TablesCombinationData_Detection(dB_connector, tablesNames, selThreshold, tablesNamesStr));
            }
        }

        for (TableDeleteSels_Training d : tablesDelete_training) {
            int maxDeletes = d.getMaxDeletes();
            String tableName = d.getTableName();
            for (String UID : UIDs_detection) {
                HashMap h = tablesDeleteSels.get(UID);
                h.put(tableName, new TableDeleteSels_Detection(dB_connector, tableName, maxDeletes));
            }
        }

        for (TableInsertSels_Training i : tablesInserts_training) {
            int maxInserts = i.getMaxInserts();
            String tableName = i.getTableName();
            for (String UID : UIDs_detection) {
                HashMap h = tablesInsertSels.get(UID);
                h.put(tableName, new TableInsertSels_Detection(dB_connector, tableName, maxInserts));
            }
        }

        for (TableUpdateSels_Training u : tablesUpdates_training) {
            int maxUpdates = u.getMaxUpdates();
            String tableName = u.getTableName();
            for (String UID : UIDs_detection) {
                HashMap h = tablesUpdateSels.get(UID);
                h.put(tableName, new TableUpdateSels_Detection(dB_connector, tableName, maxUpdates));
            }
        }
    }

    int falsePositives;
    int falseNegatives;

    int falseNegativesSelect = 0;
    int falseNegativesInsert = 0;
    int falseNegativesDelete = 0;
    int falseNegativesUpdate = 0;

    int falsePositivesSelect = 0;
    int falsePositivesInsert = 0;
    int falsePositivesDelete = 0;
    int falsePositivesUpdate = 0;

    public int getFalsePositivesSelect() {
        return falsePositivesSelect;
    }

    public int getFalsePositivesInsert() {
        return falsePositivesInsert;
    }

    public int getFalsePositivesDelete() {
        return falsePositivesDelete;
    }

    public int getFalsePositivesUpdate() {
        return falsePositivesUpdate;
    }

    public int getFalseNegativesSelect() {
        return falseNegativesSelect;
    }

    public int getFalseNegativesInsert() {
        return falseNegativesInsert;
    }

    public int getFalseNegativesDelete() {
        return falseNegativesDelete;
    }

    public int getFalseNegativesUpdate() {
        return falseNegativesUpdate;
    }

    public int getFalsePositives() {
        return falsePositives;
    }

    public int getFalseNegatives() {
        return falseNegatives;
    }

    ArrayList<Integer> FPsMultiples_arr = new ArrayList<>();
    ArrayList<Integer> FPs_arr = new ArrayList<>();

    public void checkQueries_FP(ArrayList<String> UIDs, ArrayList<String> queries) {

        for (int r = 0; r < queries.size(); r++) {
            numQueries++;

            String UID = UIDs.get(r);
            String query = queries.get(r);

            FPsMultiples_arr.add(r);
            FPs_arr.add(falsePositives);

            System.out.println(r + "/" + queries.size());
            System.out.println("-" + UID + ": " + query);

            String queryLower = query.toLowerCase();
            if (queryLower.startsWith("select")) {
                numQueriesSelect++;

                String tablesPortion = query.substring(queryLower.indexOf(" from") + 5, queryLower.indexOf("where"));
                tablesPortion = tablesPortion.trim();
                String tablesArr[] = tablesPortion.split(",");
                for (int i = 0; i < tablesArr.length; i++) {
                    tablesArr[i] = tablesArr[i].trim();
                }
                String key = "";
                if (tablesArr.length > 1) {
                    List<String> tablesArrL = Arrays.asList(tablesArr);

                    Collections.sort(tablesArrL);
                    for (String table : tablesArrL) {
                        String tableName = table.trim();
                        key = key + tableName;
                    }
                } else {

                    key = tablesArr[0];
                    /*if(tablesArr[0].contains(" ")) {
                     key = tablesArr[0].substring(0, tablesArr[0].indexOf(" "));
                     } else {
                     key = tablesArr[0];
                     }*/
                }

                HashMap<String, TablesCombinationData_Detection> h = tablesInfo_detection.get(UID);
                TablesCombinationData_Detection s = h.get(key);
                if (s == null) {
                    falsePositivesSelect++;
                    falsePositives++;
                } else {

                    String predicate;
                    if (queryLower.contains("for update")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("for update")).trim();
                    } else if (queryLower.contains("order by")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("order by")).trim();
                    } else {
                        predicate = query.substring(queryLower.indexOf("where") + 5).trim();
                    }

                    float anomalyDegree = s.isAnomalous(predicate);
                    if (anomalyDegree > 0) {
                        falsePositives++;
                        falsePositivesSelect++;

                        for (String tableName : tablesArr) {
                            String tableNameL = tableName.trim().toLowerCase();
                            tableNameL = tableNameL.replaceAll("\"\"", "");
                            if (tableNameL.contains(" ")) {
                                tableNameL = tableNameL.substring(0, tableNameL.indexOf(" "));
                            }
                            TableChanges tc = tablesChanges.get(tableNameL);
                            if (tc == null) {
                                System.out.println();
                            }
                            tc.addFP();
                        }
                    }
                    s.addPredicate(predicate);
                    allAnomalyDegrees.add(anomalyDegree);
                }
            } /*else if (queryLower.startsWith("update")) {
                numQueriesUpdate++;
                String tablePortion = query.substring(queryLower.indexOf("update") + 6, queryLower.indexOf("set")).trim();
                tablePortion = tablePortion.replaceAll("\"", "");
                HashMap<String, TableUpdateSels_Detection> h = tablesUpdateSels.get(UID);
                TableUpdateSels_Detection u = h.get(tablePortion);
                TableChanges tc = tablesChanges.get(tablePortion.toLowerCase());
                if (u == null) {
                    falsePositives++;
                    falsePositivesUpdate++;
                } else {
                    if (u.checkAnomaly(query)) {
                        falsePositives++;
                        falsePositivesUpdate++;
                        tc.addFP();
                    }

                    int numNewChanges = u.executeQuery(query);
                    tc.addChanges(numNewChanges);
                    tc.checkAndExecute();
                }

            } else if (queryLower.startsWith("delete")) {
                numQueriesDelete++;
                String tablePortion;
                int indexOfWhere = queryLower.indexOf("where");
                if (indexOfWhere != -1) {
                    tablePortion = query.substring(queryLower.indexOf("delete from") + 11, indexOfWhere).trim();
                } else {
                    tablePortion = query.substring(queryLower.indexOf("delete from") + 11).trim();
                }

                HashMap<String, TableDeleteSels_Detection> h = tablesDeleteSels.get(UID);
                TableDeleteSels_Detection d = h.get(tablePortion);
                String tablePortionL = tablePortion.toLowerCase();
                tablePortionL = tablePortionL.replaceAll("\"", "");
                TableChanges tc = tablesChanges.get(tablePortionL);
                if (d == null) {
                    falsePositives++;
                    falsePositivesDelete++;

                } else {
                    if (d.checkAnomaly(query)) {
                        falsePositives++;
                        falsePositivesDelete++;
                        tc.addFP();
                    }
                    int numNewChanges = d.executeQuery(query);
                    tc.addChanges(numNewChanges);
                    tc.checkAndExecute();
                }
            } else if (queryLower.startsWith("insert")) {
                numQueriesInsert++;
                int indexOfBracket = queryLower.indexOf("(");
                int indexOfValues = queryLower.indexOf("values");
                String tablePortion;
                if (indexOfBracket < indexOfValues) {
                    tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfBracket).trim();
                } else {
                    tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfValues).trim();
                }

                HashMap<String, TableInsertSels_Detection> h = tablesInsertSels.get(UID);
                TableInsertSels_Detection i = h.get(tablePortion);

                TableChanges tc = tablesChanges.get(tablePortion.toLowerCase());

                if (i == null) {
                    falsePositives++;
                    falsePositivesInsert++;
                } else {
                    if (i.checkAnomaly(query)) {
                        falsePositives++;
                        falsePositivesInsert++;
                        tc.addFP();
                    }
                    int numNewChanges = i.executeQuery(query);
                    tc.addChanges(numNewChanges);
                    tc.checkAndExecute();
                }
            }*/
        }
    }

    ArrayList<Integer> FNsMultiples_arr = new ArrayList<>();
    ArrayList<Integer> FNs_arr = new ArrayList<>();

    public void checkQueries_FN(ArrayList<String> UIDs, ArrayList<String> queries,
            ArrayList<String> allUIDs, ArrayList<String> allUIDs2, int numOriginalDetectionQueries) {

        for (int r = 0; r < queries.size(); r++) {
            numQueries2++;

            if (r % numOriginalDetectionQueries == 0) {
                int numRounds = r / numOriginalDetectionQueries;
                FNsMultiples_arr.add(numRounds);
                FNs_arr.add(falseNegatives);
                if (numRounds == 10) {
                    break;
                }
            }

            String UID2 = UIDs.get(r);
            String query = queries.get(r);

            String UID = allUIDs.get(allUIDs2.indexOf(UID2));

            System.out.println(r + "/" + queries.size());
            System.out.println("-" + UID + ": " + query);

            String queryLower = query.toLowerCase();
            if (queryLower.startsWith("select")) {
                numQueriesSelect2++;

                String tablesPortion = query.substring(queryLower.indexOf("from") + 4, queryLower.indexOf("where"));
                tablesPortion = tablesPortion.trim();
                String tablesArr[] = tablesPortion.split(",");
                String key = "";
                for (String table : tablesArr) {
                    String tableName = table.trim();
                    key = key + tableName;
                }

                HashMap<String, TablesCombinationData_Detection> h = tablesInfo_detection.get(UID);
                TablesCombinationData_Detection s = h.get(key);
                if (s != null) {
                    String predicate;
                    if (queryLower.contains("for update")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("for update")).trim();
                    } else if (queryLower.contains("order by")) {
                        predicate = query.substring(queryLower.indexOf("where") + 5, queryLower.indexOf("order by")).trim();
                    } else {
                        predicate = query.substring(queryLower.indexOf("where") + 5).trim();
                    }

                    if (s.isActualAnomaly(predicate) && s.isAnomalous(predicate) > 0) {
                        falseNegatives++;
                        falseNegativesSelect++;
                        for (String tableName : tablesArr) {
                            String tableNameL = tableName.trim().toLowerCase();
                            tableNameL = tableNameL.replaceAll("\"", "");
                            if (tableNameL.contains(" ")) {
                                tableNameL = tableNameL.substring(0, tableNameL.indexOf(" "));
                            }
                            TableChanges tc = tablesChanges.get(tableNameL);
                            tc.addFN();
                        }
                    }
                    s.addPredicate(predicate);
                }
            } else if (queryLower.startsWith("update")) {
                numQueriesUpdate2++;
                String tablePortion = query.substring(queryLower.indexOf("update") + 6, queryLower.indexOf("set")).trim();
                HashMap<String, TableUpdateSels_Detection> h = tablesUpdateSels.get(UID);
                TableUpdateSels_Detection u = h.get(tablePortion);
                tablePortion = tablePortion.replaceAll("\"", "");
                TableChanges tc = tablesChanges.get(tablePortion.toLowerCase());
                if (u != null) {
                    if (!u.checkAnomaly(query)) {
                        falseNegatives++;
                        falseNegativesUpdate++;
                        tc.addFN();
                    }
                    int numNewChanges = u.executeQuery(query);
                    tc.addChanges(numNewChanges);
                    tc.checkAndExecute();
                }

            } else if (queryLower.startsWith("delete")) {
                numQueriesDelete2++;
                String tablePortion;
                int indexOfWhere = queryLower.indexOf("where");
                if (indexOfWhere != -1) {
                    tablePortion = query.substring(queryLower.indexOf("delete from") + 11, indexOfWhere).trim();
                } else {
                    tablePortion = query.substring(queryLower.indexOf("delete from") + 11).trim();
                }

                HashMap<String, TableDeleteSels_Detection> h = tablesDeleteSels.get(UID);
                TableDeleteSels_Detection d = h.get(tablePortion);
                tablePortion = tablePortion.replaceAll("\"", "");
                TableChanges tc = tablesChanges.get(tablePortion.toLowerCase());
                if (d != null) {
                    if (!d.checkAnomaly(query)) {
                        falseNegatives++;
                        falseNegativesDelete++;
                        tc.addFN();
                    }
                    int numNewChanges = d.executeQuery(query);
                    tc.addChanges(numNewChanges);
                    tc.checkAndExecute();
                }

            } else if (queryLower.startsWith("insert")) {
                numQueriesInsert2++;
                int indexOfBracket = queryLower.indexOf("(");
                int indexOfValues = queryLower.indexOf("values");
                String tablePortion;
                if (indexOfBracket < indexOfValues) {
                    tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfBracket).trim();
                } else {
                    tablePortion = query.substring(queryLower.indexOf("into") + 4, indexOfValues).trim();
                }

                HashMap<String, TableInsertSels_Detection> h = tablesInsertSels.get(UID);
                TableInsertSels_Detection i = h.get(tablePortion);
                tablePortion = tablePortion.replaceAll("\"", "");
                TableChanges tc = tablesChanges.get(tablePortion.toLowerCase());

                if (i != null) {
                    if (!i.checkAnomaly(query)) {
                        falseNegatives++;
                        falseNegativesInsert++;
                    }
                    query = query.replaceAll("\"", "");
                    int numNewChanges = i.executeQuery(query);

                    tc.addChanges(numNewChanges);
                    tc.checkAndExecute();
                }
            }
        }

        FNsMultiples_arr.add(queries.size() / numOriginalDetectionQueries);
        FNs_arr.add(falseNegatives);
    }

    public int getNumQueries() {
        return numQueries;
    }

    public int getNumAnomalies() {
        return numAnomalies;
    }

    public int getNumFalseNegatives() {
        return falseNegatives;
    }

    public int getNumQueries2() {
        return numQueries2;
    }

    public void recordCurrentSels() {
        for (Map.Entry<String, HashMap<String, TablesCombinationData_Detection>> entries1 : tablesInfo_detection.entrySet()) {
            for (Map.Entry entries2 : entries1.getValue().entrySet()) {
                TablesCombinationData_Detection t = (TablesCombinationData_Detection) entries2.getValue();
                t.recordSel();
            }
        }
    }

    public void printDetectionData(String dirName) {

        try {
            PrintWriter writer = new PrintWriter(dirName + "detectionResults.txt", "UTF-8");
            writer.println("SELECT");
            for (Map.Entry entry1 : tablesInfo_detection.entrySet()) {
                String k1 = (String) entry1.getKey();
                writer.println("-" + k1);
                HashMap<String, TablesCombinationData_Detection> h1 = (HashMap<String, TablesCombinationData_Detection>) entry1.getValue();
                for (Map.Entry entry2 : h1.entrySet()) {
                    String k2 = (String) entry2.getKey();
                    writer.println("--" + k2);
                    TablesCombinationData_Detection t = (TablesCombinationData_Detection) entry2.getValue();
                    writer.println("---" + t.getRemainingSel());
                    writer.println("---" + t.numRowsCartesian());
                    writer.println("---" + t.getNumQueriesTillAnomaly());
                    writer.println("---" + t.getAnomalousQueriesSel());
                }
            }

            writer.println("INSERT");
            for (Map.Entry entry1 : tablesInsertSels.entrySet()) {
                String k1 = (String) entry1.getKey();
                writer.println("-" + k1);
                HashMap<String, TableInsertSels_Detection> h1 = (HashMap<String, TableInsertSels_Detection>) entry1.getValue();
                for (Map.Entry entry2 : h1.entrySet()) {
                    String k2 = (String) entry2.getKey();
                    writer.println("--" + k2);
                    TableInsertSels_Detection t = (TableInsertSels_Detection) entry2.getValue();
                    writer.println("---" + t.getRemainingSel());
                    writer.println("---" + t.getTableRowCount());
                }
            }

            writer.println("DELETE");
            for (Map.Entry entry1 : tablesDeleteSels.entrySet()) {
                String k1 = (String) entry1.getKey();
                writer.println("-" + k1);
                HashMap<String, TableDeleteSels_Detection> h1 = (HashMap<String, TableDeleteSels_Detection>) entry1.getValue();
                for (Map.Entry entry2 : h1.entrySet()) {
                    String k2 = (String) entry2.getKey();
                    writer.println("--" + k2);
                    TableDeleteSels_Detection t = (TableDeleteSels_Detection) entry2.getValue();
                    writer.println("---" + t.getRemainingSel());
                    writer.println("---" + t.getTableRowCount());
                }
            }

            writer.println("UPDATE");
            for (Map.Entry entry1 : tablesUpdateSels.entrySet()) {
                String k1 = (String) entry1.getKey();
                writer.println("-" + k1);
                HashMap<String, TableUpdateSels_Detection> h1 = (HashMap<String, TableUpdateSels_Detection>) entry1.getValue();
                for (Map.Entry entry2 : h1.entrySet()) {
                    String k2 = (String) entry2.getKey();
                    writer.println("--" + k2);
                    TableUpdateSels_Detection t = (TableUpdateSels_Detection) entry2.getValue();
                    writer.println("---" + t.getRemainingSel());
                    writer.println("---" + t.getTableRowCount());
                }
            }

            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printFPs(String dirName) {
        try {
            PrintWriter writer = new PrintWriter(dirName + "FPs.txt", "UTF-8");

            for (int i = 0; i < FPsMultiples_arr.size(); i++) {
                writer.println(FPsMultiples_arr.get(i) + " " + FPs_arr.get(i));
            }
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printFNs(String dirName) {
        try {
            PrintWriter writer = new PrintWriter(dirName + "FNs.txt", "UTF-8");

            for (int i = 0; i < FNsMultiples_arr.size(); i++) {
                writer.println(FNsMultiples_arr.get(i) + " " + FNs_arr.get(i));
            }

            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void printAnomalyDegrees(String dirName) {

        try (PrintWriter writer = new PrintWriter(dirName + "anomalyDegrees.txt", "UTF-8")) {
            
            allAnomalyDegrees.stream().forEach((anomalyDegree) -> {
                writer.println(anomalyDegree);
            });
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DetectionPhase_new.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DetectionPhase_new.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
