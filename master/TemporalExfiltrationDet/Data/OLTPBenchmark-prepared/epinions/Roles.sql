DROP ROLE IF EXISTS epinions_role_u;
DROP ROLE IF EXISTS epinions_u;
DROP ROLE IF EXISTS epinions_role_w;
DROP ROLE IF EXISTS epinions_w;

CREATE ROLE epinions_role_u;
CREATE ROLE epinions_u WITH LOGIN;

CREATE ROLE epinions_role_w;
CREATE ROLE epinions_w WITH LOGIN;

GRANT epinions_role_u to epinions_u;
GRANT epinions_role_w to epinions_w;

GRANT INSERT, SELECT, UPDATE, DELETE ON users TO epinions_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON item TO epinions_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON review TO epinions_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON review_rating TO epinions_role_u;
GRANT INSERT, SELECT, UPDATE, DELETE ON trust TO epinions_role_u;

GRANT INSERT, SELECT, UPDATE, DELETE ON users TO epinions_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON item TO epinions_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON review TO epinions_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON review_rating TO epinions_role_w;
GRANT INSERT, SELECT, UPDATE, DELETE ON trust TO epinions_role_w;
