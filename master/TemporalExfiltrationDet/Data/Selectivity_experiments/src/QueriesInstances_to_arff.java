import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


public class QueriesInstances_to_arff {
	
	void convert(String filePath) {
		int []rolesIDs = {};
		try{
			String dir = "C:\\Users\\asmaa\\Desktop\\testData\\queriesInstances\\";
			String fileName = "queriesInstances_1000_t";
			BufferedWriter writer = new BufferedWriter(new FileWriter(dir + fileName + ".arff"));
	      // creates a FileWriter Object     

	      //Creates a FileReader Object
	      File fileI = new File(dir + fileName + ".csv");
	      FileReader fr = new FileReader(fileI);
	      BufferedReader br = new BufferedReader(fr);
	      
	      String line;
	      
	      String firstLine = br.readLine();
	      String[] attsNames = firstLine.split(",");
	      
	      // now writing the header
	      writer.write("@relation AD\n\n");
	      
	      if(attsNames.length > 51) {
	    	  System.err.println("error");
	      }
	      
	      for(int i = 1; i < attsNames.length; i++) {
	    	  writer.write("@attribute " + attsNames[i] + " {0, 1}\n");
	      }
	      writer.write("@attribute role{16412, 32994, 32992, 16410, 16408}\n\n");
	      
	      writer.write("@data\n");
	      while((line = br.readLine()) != null) {
	          String[] strs = line.split(",");
	          for(int i = 1; i < strs.length; i++) {
	        	  if(i != 1) {
	        		  writer.write(",");
	        	  }
	        	  writer.write(strs[i]);
	          }
	          writer.write("," + strs[0] + "\n");
	      }
	      fr.close();
	      writer.flush();
	      writer.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		QueriesInstances_to_arff r = new QueriesInstances_to_arff();
		r.convert("");
	}
}
