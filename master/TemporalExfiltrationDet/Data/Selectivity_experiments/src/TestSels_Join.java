import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class TestSels_Join {
	public static void main(String[] args) {
		int numTrainingRecs = 2000;
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		for(int i = 0; i < numTrainingRecs/2; i++) {
			String query = "Select * from table0, table1 where table0.c0 < 5;";
			// table0 is a medium and table 1 is large
			trainingQueries.add(query);
		}
		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\diffSels_join\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role0.txt"));
			out.write("C\n");
			out.write("user0\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		trainingQueries.clear();
		
		
		for(int i = 0; i < numTrainingRecs/2; i++) {
			String query = "Select * from table0, table1 where table0.c0 < 2 and table1.c0 < 2;";
			// table0 is a small and table 1 is small
			trainingQueries.add(query);
		}
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\diffSels_join\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role1.txt"));
			out.write("C\n");
			out.write("user1\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
