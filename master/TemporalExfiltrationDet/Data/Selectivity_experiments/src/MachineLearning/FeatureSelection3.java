package MachineLearning;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


public class FeatureSelection3 {
	
	int numTables = 20;
	int numColsPerTable = 5;
	
	int numRoles = 4;
	
	int numTrainingRecs = 1000;
	int numTrainingRecsPerRole = numTrainingRecs / numRoles;
	int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / numTables;
	int numTrainingRecsPerRolePerTablePerCol = numTrainingRecsPerRolePerTable / numColsPerTable;
	
	float portion = 0.5f;
	
	ArrayList<String> allDetectionQueries = new ArrayList<String>();
	ArrayList<String> tablesNames = new ArrayList<String>();
	ArrayList<Integer> colsIDs = new ArrayList<Integer>();
	
	public void generateRole(int roleID) {
		
		boolean detectionQueriesIDs[] = new boolean[numTrainingRecsPerRole];
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}
		
		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for(int i = 0; i < numTrainingRecsPerRole/4; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsPerRole;
			detectionQueriesIDs[rNumber] = true;
		}
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		
		int currQueryID = 0;
		int start = (int) (roleID * numTables * (1- portion) / numRoles);
		int nT = (int) (numTables * (1- portion) / numRoles);
				
		for(int i = start; i < start + nT; i++) {
			for(int j = 0; j < numColsPerTable; j++) {
				for(int k = 0; k < numTrainingRecsPerRolePerTablePerCol; k++) {
					String query = "SELECT c" + j + " from table" + i + ";";
					
					if(inList(j, "table" + i)) {
						if(detectionQueriesIDs[currQueryID]) {
							allDetectionQueries.add(roleID  + "," + query);
						} else {
							trainingQueries.add(query);
						}
					}
					currQueryID++;
				}
			}
		}
		
		start = (int) (numTables * (1- portion));
		for(int i = start + 1; i < numTables; i++) {
			for(int j = 0; j < numColsPerTable; j++) {
				for(int k = 0; k < numTrainingRecsPerRolePerTablePerCol; k++) {
					String query = "SELECT c" + j + " from table" + i + ";";
					if(inList(j, "table" + i)) {
						if(detectionQueriesIDs[currQueryID]) {
							allDetectionQueries.add(roleID  + "," + query);
						} else {
							trainingQueries.add(query);
						}
					}
					currQueryID++;
				}
			}
		}
		
		
		try {
			String currDirectory = "C:\\Users\\asmaa\\Desktop\\testData\\FS\\" + portion + "\\" + numTrainingRecs + "\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	private boolean inList(int colID, String tableName) {
		int i = 0;
		for (Iterator iterator = tablesNames.iterator(); iterator.hasNext(); i++) {
			String currTableName = (String) iterator.next();
			if(currTableName.equals(tableName) && colID == colsIDs.get(i)) {
				return true;
			}
		}
		return false;
	}

	public void writeDetectionQueries() {
		try {
			String currDirectory = "C:\\Users\\asmaa\\Desktop\\testData\\FS\\" + portion + "\\" + numTrainingRecs + "\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory + "\\detection.txt"));
			
			for(int i = 0; i < allDetectionQueries.size(); i++) {
				out.write(allDetectionQueries.get(i) + "\n");
			}
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void readFeatures() {
		float portion1 = 0.75f;
		File file = new File("C:\\Users\\asmaa\\Desktop\\testData\\FS\\FS_" + portion1 + ".txt");
	    FileInputStream fis = null;
	    BufferedInputStream bis = null;
	    DataInputStream dis = null;

	    try {
	      fis = new FileInputStream(file);

	      // Here BufferedInputStream is added for fast reading.
	      bis = new BufferedInputStream(fis);
	      dis = new DataInputStream(bis);

	      int numFeatures = 100;
	      
	      
	      int numFeaturesToConsider = (int) (portion1 * numFeatures);
	      
	      
	      for(int i = 0; i < numFeaturesToConsider; i++) {
	    	  String line = dis.readLine();
	    	  
	    	  String lineParts[] = line.split(" ");
	    	  String[] table_Col = lineParts[2].split("_");
	    	  String tableName = table_Col[0];
	    	  int colID = Integer.parseInt(table_Col[1]);
	    	  tablesNames.add(tableName);
	    	  colsIDs.add(colID);
	    	 
	      }

	      // dispose all the resources after using them.
	      fis.close();
	      bis.close();
	      dis.close();

	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }
	
	public void generateAll() {
		readFeatures();
		for(int i = 0; i < numRoles; i++) {
			generateRole(i);
		}
	}
	
	
	public static void main(String[] args) {
		FeatureSelection3 fs = new FeatureSelection3();
		fs.generateAll();
		fs.writeDetectionQueries();
	}
	
}
