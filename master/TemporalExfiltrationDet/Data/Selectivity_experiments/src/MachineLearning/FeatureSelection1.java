package MachineLearning;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class FeatureSelection1 {

	int numTables = 20;
	int numColsPerTable = 10;
	int numRoles = 10;
	int numTrainingRecs = 5000;
	int numTrainingRecsPerRole = numTrainingRecs / numRoles;
	int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / numTables;
	int numTrainingRecsPerRolePerTablePerCol = numTrainingRecsPerRolePerTable / numColsPerTable;
	
	
	
	public void generateRole(int roleID) {
		
		boolean detectionQueriesIDs[] = new boolean[numTrainingRecsPerRole];
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}
		
		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for(int i = 0; i < numTrainingRecsPerRole/4; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsPerRole;
			detectionQueriesIDs[rNumber] = true;
		}
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		ArrayList<String> detectionQueries = new ArrayList<String>();
		
		int currQueryID = 0;
		for(int i = 0; i < numTables; i++) {
			if(i < numRoles && roleID != i) {
				continue;
			}
			for(int j = 0; j < numColsPerTable; j++) {
				for(int k = 0; k < numTrainingRecsPerRolePerTablePerCol; k++) {
					String query = "SELECT * from table" + i + ";";
					if(detectionQueriesIDs[currQueryID]) {
						detectionQueries.add(query);
					} else {
						trainingQueries.add(query);
					}
					currQueryID++;
				}
			}
		}
		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\featureSel1\\" + numTrainingRecs + "\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			String filePath = "C:\\Users\\asmaa\\Desktop\\testData\\featureSel1\\" + numTrainingRecs + "\\detection\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(filePath + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < detectionQueries.size(); i++) {
				out.write(detectionQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	

	}
	
	public void generateAll() {
		for(int i = 0; i < numRoles; i++) {
			generateRole(i);
		}
	}
	
	public static void main(String[] args) {
		FeatureSelection1 fs = new FeatureSelection1();
		fs.generateAll();
	}
	
}
