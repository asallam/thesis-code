package MachineLearning;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class featureSelParser {

	public static void main(String[] args) {
		File file = new File("C:\\Users\\asmaa\\Desktop\\testData\\FS\\FS.txt");
	    FileInputStream fis = null;
	    BufferedInputStream bis = null;
	    DataInputStream dis = null;

	    try {
	      fis = new FileInputStream(file);

	      // Here BufferedInputStream is added for fast reading.
	      bis = new BufferedInputStream(fis);
	      dis = new DataInputStream(bis);

	      int numFeatures = 100;
	      float portion = 0.2f;
	      
	      int numFeaturesToConsider = (int) (portion * numFeatures);
	      
	      
	      for(int i = 0; i < numFeaturesToConsider; i++) {
	    	  String line = dis.readLine();
	    	  
	    	  String lineParts[] = line.split(" ");
	    	  String[] table_Col = lineParts[2].split("_");
	    	  String tableName = table_Col[0];
	    	  String colName = table_Col[1];
	    	  System.out.println(tableName + " " + colName);
	      }

	      // dispose all the resources after using them.
	      fis.close();
	      bis.close();
	      dis.close();

	    } catch (FileNotFoundException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }

}
	

