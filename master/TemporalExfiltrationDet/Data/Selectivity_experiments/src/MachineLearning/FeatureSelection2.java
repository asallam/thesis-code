package MachineLearning;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/*used to test MLC*/

public class FeatureSelection2 {
	
	int numTables = 20;
	int numColsPerTable = 10;
	
	float portion = 0.35f;
	
	int numRoles = 10;
	
	int numTrainingRecs = 1000;
	int numTrainingRecsPerRole = numTrainingRecs / numRoles;
	int numTablesAccessedPerRole = (int) ((numTables - 1) * portion  + 1);
	int numTrainingRecsPerRolePerTable = numTrainingRecsPerRole / numTablesAccessedPerRole;
	int numTrainingRecsPerRolePerTablePerCol = numTrainingRecsPerRolePerTable / numColsPerTable;
	
	
	
	ArrayList<String> allDetectionQueries = new ArrayList<String>();
	
	ArrayList<String> detection0 = new ArrayList<String>();
	
	public void generateRole(int roleID) {
		
		boolean detectionQueriesIDs[] = new boolean[numTrainingRecsPerRole];
		for (int i = 0; i < detectionQueriesIDs.length; i++) {
			detectionQueriesIDs[i] = false;
		}
		
		Random rGen = new Random();
		// now choose which queries in training and which in detection
		for(int i = 0; i < numTrainingRecsPerRole/4; i++) {
			int rNumber = Math.abs(rGen.nextInt()) % numTrainingRecsPerRole;
			detectionQueriesIDs[rNumber] = true;
		}
		
		ArrayList<String> trainingQueries = new ArrayList<String>();
		
		
		int currQueryID = 0;
		int start = (int) (roleID * numTables * (1- portion) / numRoles);
		int nT = (int) (numTables * (1- portion) / numRoles);
		
		for(int i = start; i < start + nT; i++) {
			for(int j = 0; j < numTrainingRecsPerRolePerTable; j++) {
				String query = "SELECT * from table" + i + ";";
				if(detectionQueriesIDs[currQueryID % numTrainingRecsPerRole]) {
					allDetectionQueries.add(roleID + "," + query);
					
					for(int r0 = 0; r0 < numRoles; r0++) {
						if(r0 == roleID) continue;
						else {
							detection0.add(r0 + "," + query);
						}
					}
					
					
				} else {
					trainingQueries.add(query);
				}
				currQueryID++;
			}
		}
		
		// common queries among the roles
		start = (int) (numTables * (1- portion));
		for(int i = start; i < numTables; i++) {
			for(int j = 0; j < numTrainingRecsPerRolePerTable; j++) {
				String query = "SELECT * from table" + i + ";";
				if(detectionQueriesIDs[currQueryID % numTrainingRecsPerRole]) {
					allDetectionQueries.add(roleID + "," + query);
				} else {
					trainingQueries.add(query);
				}
				currQueryID++;
			}
		}
		
		
		try {
			String currDirectory = "C:\\Users\\asmaa\\Desktop\\testData\\FS\\" + portion + "\\" + numTrainingRecs + "\\";
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory + "role" + roleID + ".txt"));
			out.write("C\n");
			out.write("user" + roleID + "\n");
			out.write("c\n");
			out.write("Q\n");
			for(int i = 0; i < trainingQueries.size(); i++) {
				out.write(trainingQueries.get(i) + "\n");
			}
			out.write("q");
		    out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void generateAll() {
		for(int i = 0; i < numRoles; i++) {
			generateRole(i);
		}
		
		try {
			String currDirectory = "C:\\Users\\asmaa\\Desktop\\testData\\FS\\" + portion + "\\" + numTrainingRecs + "\\detection.txt";
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory));
//			out.write("C\n");
//			out.write("user" + roleID + "\n");
//			out.write("c\n");
//			out.write("Q\n");
			for(int i = 0; i < allDetectionQueries.size(); i++) {
				out.write(allDetectionQueries.get(i) + "\n");
			}
//			out.write("q");
		    out.close();
//		    allDetectionQueries.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			String currDirectory = "C:\\Users\\asmaa\\Desktop\\testData\\FS\\" + portion + "\\" + numTrainingRecs + "\\detection0.txt";
			BufferedWriter out = new BufferedWriter(new FileWriter(currDirectory));
//			out.write("C\n");
//			out.write("user" + roleID + "\n");
//			out.write("c\n");
//			out.write("Q\n");
			for(int i = 0; i < detection0.size(); i++) {
				out.write(detection0.get(i) + "\n");
			}
//			out.write("q");
		    out.close();
//		    allDetectionQueries.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args) {
		FeatureSelection2 fs = new FeatureSelection2();
		fs.generateAll();
	}
	
}
