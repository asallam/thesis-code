import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale

MY_FILE = '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_1_SFR'

df_1 = pd.read_csv( '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_1_SFR')
df_2 = pd.read_csv( '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_2_SFR')
df_3 = pd.read_csv( '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_3_SFR')
df_4 = pd.read_csv( '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_4_SFR')
df_5 = pd.read_csv( '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_5_SFR')
df_6 = pd.read_csv( '/home/asma/Desktop/PG_logs/seats/SE/sc_1/10-10/training_6_SFR')

df = np.concatenate((df_1, df_2, df_3, df_4, df_5, df_6))

kmeans = KMeans(init='k-means++', n_clusters=30, n_init=10)
kmeans.fit(df)

# Obtain labels for each point in mesh. Use last trained model.
Z = kmeans.predict(df)
print(Z)
