natts<- c(0, 0, 3.66, 0, 7.32, 0, 10.98, 0, 14.64, 0)
error_1 <- c(0,86.66666667, 0, 36.43410853, 0, 1.612903226, 0, 0, 0, 0)
error_2 <- c()

error <- table(error_1, error_2)

pdf('charts/1.pdf')

barplot(error, main="Car Distribution by Gears and VS",
  xlab="Number of Gears", col=c("darkblue","red"),
 	legend = rownames(counts), beside=TRUE)

box()
dev.off()
