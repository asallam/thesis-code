print(__doc__)
import sys
import numpy as np
import pylab as pl
import matplotlib.font_manager
from sklearn import svm
from numpy import genfromtxt

from scipy import stats
import matplotlib.pyplot as plt
import matplotlib.font_manager

from sklearn.covariance import EllipticEnvelope
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor

outliers_fraction = float(sys.argv[1])
trainingVector = genfromtxt(sys.argv[2], delimiter=',')
detectionVector = genfromtxt(sys.argv[3], delimiter=',')
outputOutliers = open(sys.argv[4], 'w')

n_samples = len(trainingVector)
clusters_separation = [0, 1, 2]
rng = np.random.RandomState(42)
ground_truth = np.ones(n_samples, dtype=int)


classifiers = {
#    "One-Class SVM": svm.OneClassSVM(nu=0.9, gamma=0.1),
  "Robust covariance": EllipticEnvelope(contamination=outliers_fraction),
#    "Isolation Forest": IsolationForest(max_samples=n_samples,
   #                                    contamination=outliers_fraction,
      #                                 random_state=rng, verbose=1)
 #   "Local Outlier Factor": LocalOutlierFactor(
    #    n_neighbors=45,
      #  contamination=outliers_fraction, metric=SFR_dist)
}

print("num samples = %d", n_samples)

for i, (clf_name, clf) in enumerate(classifiers.items()):
        # fit the data and tag outliers
#        if clf_name == "Local Outlier Factor":
#            y_pred = clf.fit_predict(trainingVector)
#            scores_pred = clf.negative_outlier_factor_
#        else:
	clf.fit(trainingVector)
        scores_pred = clf.decision_function(trainingVector)
        y_pred = clf.predict(detectionVector)
	for item in y_pred:
		outputOutliers.write("%s\n" % item)
#n_errors = (y_pred != ground_truth).sum()
#	print(clf_name)
#        print(n_errors)

