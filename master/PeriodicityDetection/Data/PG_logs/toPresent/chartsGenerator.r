fns_tpcc = read.table("/home/asma/Desktop/PG_logs/toPresent/FNs_tpcc.txt")
fns_tatp = read.table("/home/asma/Desktop/PG_logs/toPresent/FNs_tatp.txt")
fns_seats = read.table("/home/asma/Desktop/PG_logs/toPresent/FNs_seats.txt")
fns_all = read.table("/home/asma/Desktop/PG_logs/toPresent/FNs.txt")
multiples=c(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

jpeg('fns_tpcc.jpeg');
barplot(fns_tpcc$V1, names.arg = multiples, cex.names=1.75, cex.axis=2)
dev.off()

jpeg('fns_tatp.jpeg');
barplot(fns_tatp$V1, names.arg = multiples, cex.names=1.75, cex.axis=2)
dev.off()

jpeg('fns_seats.jpeg');
barplot(fns_seats$V1, names.arg = multiples, cex.names=1.75, cex.axis=2)
dev.off()

jpeg('fns_all.jpeg');
barplot(fns_all$V1, names.arg=c("TPCC", "Seats", "TATP"), cex.names=1.75, cex.axis=2)
dev.off()

fps_tpcc = read.table("/home/asma/Desktop/PG_logs/toPresent/FPs_tpcc.txt")
fps_tatp = read.table("/home/asma/Desktop/PG_logs/toPresent/FPs_tatp.txt")
fps_seats = read.table("/home/asma/Desktop/PG_logs/toPresent/FPs_seats.txt")
fps_all = read.table("/home/asma/Desktop/PG_logs/toPresent/FPs.txt")

portions=c(25, 50, 75, 100)

jpeg('fps_tpcc.jpeg');
barplot(fps_tpcc$V1, names.arg = portions, cex.names=1.75, cex.axis=2)
dev.off()

jpeg('fps_tatp.jpeg');
barplot(fps_tatp$V1, names.arg = portions, cex.names=1.75, cex.axis=2)
dev.off()

jpeg('fps_seats.jpeg');
barplot(fps_seats$V1, names.arg = portions, cex.names=1.75, cex.axis=2)
dev.off()

jpeg('fps_all.jpeg');
barplot(fps_all$V1, names.arg=c("TPCC", "Seats", "TATP"), cex.names=1.75, cex.axis=2)
dev.off()

revealedSel = read.table("/home/asma/Desktop/PG_logs/toPresent/revealedSel_select.txt")
jpeg('revealedSel.jpeg');
barplot(revealedSel$V1, names.arg=c("TPCC", "Seats", "TATP"), cex.names=1.75, cex.axis=2)
dev.off()
