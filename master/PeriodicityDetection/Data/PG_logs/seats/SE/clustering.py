print(__doc__)

from sklearn.cluster import KMeans
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import MeanShift
from sklearn.cluster import SpectralClustering
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import DBSCAN
from sklearn.cluster import Birch
from sklearn.cluster import FeatureAgglomeration
from sklearn.cluster import MiniBatchKMeans

import sys
import numpy as np
from numpy import genfromtxt

X = genfromtxt(sys.argv[1], delimiter=',')
Y_1 = genfromtxt(sys.argv[2], delimiter=',')
outDir = sys.argv[3]

estimators = [
('k_means_20', KMeans(n_clusters=20)),
('k_means_15', KMeans(n_clusters=15)),
('k_means_10', KMeans(n_clusters=10)),
('k_means_8', KMeans(n_clusters=8)),
('k_means_3', KMeans(n_clusters=3)),

('Affinity_propagation_0.5', AffinityPropagation(damping=0.5)),
('Affinity_propagation_0.7', AffinityPropagation(damping=0.7)),
('Affinity_propagation_0.9', AffinityPropagation(damping=0.9)),

('Mean_shift', MeanShift()),

('Birch_20', Birch(n_clusters=20)),
('Birch_15', Birch(n_clusters=15)),
('Birch_10', Birch(n_clusters=10)),
('Birch_8', Birch(n_clusters=8)),
('Birch_3', Birch(n_clusters=3)),

('Mini_Batch_KMeans_20', MiniBatchKMeans(n_clusters=20)),
('Mini_Batch_KMeans_15', MiniBatchKMeans(n_clusters=15)),
('Mini_Batch_KMeans_10', MiniBatchKMeans(n_clusters=10)),
('Mini_Batch_KMeans_8', MiniBatchKMeans(n_clusters=8)),
('Mini_Batch_KMeans_3', MiniBatchKMeans(n_clusters=3))
]

for name, est in estimators:
	print (name)

	outputFile_training_name = outDir + "clusteringResult_" + name + "_training"
	outputFile_training = open(outputFile_training_name , 'w')

	est.fit(X)

	result_training = est.predict(X)
	for item in result_training:
		outputFile_training.write("%s\n" % item)
# to be changed from FPs to test
	outputFile_detection_name_FPs = outDir + "clusteringResult_" + name + "_detection_FPs"
	outputFile_detection_FPs = open(outputFile_detection_name_FPs , 'w')
	result_detection_FPs = est.predict(Y_1)
	for item in result_detection_FPs:
		outputFile_detection_FPs.write("%s\n" % item)

#	outputFile_detection_name_FNs = outDir + "clusteringResult_" + name + "_detection_FNs"
#	outputFile_detection_FNs = open(outputFile_detection_name_FNs , 'w')
#	result_detection_FNs = est.predict(Y_2)
#	for item in result_detection_FNs:
#		outputFile_detection_FNs.write("%s\n" % item)
