package TestAccuracyVs;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author asma
 */
public class TestAccuracyVsSignalStrength {
    
    int[] period = {2, 3, 5, 7};
    int signalLength = 10000;
    int signalStrength[] = {1, 2, 5, 10, 20, 30, 50};
    
    public TestAccuracyVsSignalStrength() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void test() {
        for(int i = 0; i < signalStrength.length; i++) {
            int s = signalStrength[i];
            int[] signal = generateSeries(s);
            printSeries(signal);
        }
    }
    
    private int[] generateSeries(int signalStrength) {
        int signal[] = new int[signalLength];
        for(int j = 0; j < period.length; j++) {
            int currPeriod = period[j];
            for(int k = currPeriod; k < signalLength; k+= currPeriod) {
                signal[k] += signalStrength;
            }
        }
        return signal;
    }

    private void printSeries(int[] signal) {
        System.out.print("A = [");
        for(int i = 1; i < signal.length; i++) {
            System.out.print(signal[i] + " ");
        }
        System.out.print("];\n\n");
    }
}
