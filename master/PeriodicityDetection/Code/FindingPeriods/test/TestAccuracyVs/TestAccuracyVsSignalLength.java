package TestAccuracyVs;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author asma
 */
public class TestAccuracyVsSignalLength {
    int[] period = {2, 19};//, 41, 61};//{17, 19, 23, 29};//{2, 3, 5, 7};
    int signalStrength = 50;
    int signalsLengths[] = {200, 500, 1000, 2000, 5000, 10000, 200000};
    
    public TestAccuracyVsSignalLength() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test() {
        
        for(int i = 0; i < signalsLengths.length; i++) {
            int[] signal = generateSeries(signalsLengths[i]);
            printSeries(signal);
        }
        
        
    }
    
    private int[] generateSeries(int signalLength) {
        int signal[] = new int[signalLength];
        for(int i = 0; i < period.length; i++) {
            int periodLength = period[i];
            for(int k = periodLength; k < signalLength; k+= periodLength) {
                signal[k] += signalStrength;
            }
        }
        return signal;
    }

    private void printSeries(int[] signal) {
        System.out.print("A = [");
        for(int i = 1; i < signal.length; i++) {
            System.out.print(signal[i] + " ");
        }
        System.out.print("];\n\n");
    }
}
