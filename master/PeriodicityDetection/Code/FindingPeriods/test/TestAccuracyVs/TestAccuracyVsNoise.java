/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestAccuracyVs;

import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author asma
 */
public class TestAccuracyVsNoise {
    
    int[] period = {2, 3, 5, 7};
    int signalLength = 10000;
    int signalStrength = 50;
    float noisePercentage = 0.8f;
    int noiseLength = 25;
    double noiseVariance = 100;
    
    public TestAccuracyVsNoise() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test() {
        int[] signal = generateSeries();
        addGaussianNoise(signal);
        printSeries(signal);
        
    }
    
    private int[] generateSeries() {
        int signal[] = new int[signalLength];
        for(int j = 0; j < period.length; j++) {
            int currPeriod = period[j];
            for(int k = currPeriod; k < signalLength; k+= currPeriod) {
                signal[k] += signalStrength;
            }
        }
        return signal;
    }

    private void printSeries(int[] signal) {
        System.out.print("A = [");
        for(int i = 1; i < signal.length; i++) {
            System.out.print(signal[i] + " ");
        }
        System.out.print("];\n\n");
    }

    private void addRandomNoise(int[] signal) {
        Random r = new Random();
        int numNoiseSamples = (int) (noisePercentage * signalLength);
        for(int i = 0; i < numNoiseSamples; i++) {
            int x = Math.abs(r.nextInt()) % signalLength;
            signal[x] += noiseLength;
        }
    }
    
    private void addGaussianNoise(int[] signal) {
        GaussianRandom r = new GaussianRandom(noiseLength, noiseVariance);
        int numNoiseSamples = (int) (noisePercentage * signalLength);
        for(int i = 0; i < numNoiseSamples; i++) {
            int x = (int) (Math.abs(r.getGaussian()) % signalLength);
            signal[x] += noiseLength;
        }
    }
}
