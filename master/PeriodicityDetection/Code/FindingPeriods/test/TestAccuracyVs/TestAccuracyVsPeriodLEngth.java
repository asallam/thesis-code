/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestAccuracyVs;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author asma
 */
public class TestAccuracyVsPeriodLEngth {
    
    int[] period = //{2, 3};
    //{2, 7};
    //{2, 11};
    //{2, 19};
    //{2, 41};
    //{2, 61};
    //{2, 97};
    //{2, 151};
    //{2, 191};
    //{2, 251};
    //{2, 307};
    //{2, 353};
    //{2, 401};
    //{2, 457};
    //{2, 503};
    //{2, 557};
    {2, 997};
//19, 41, 61, 97};//{17, 19, 23, 29, 31};//{2, 3, 5, 7, 11, 13};
    int signalLength = 10000;
    int signalStrength = 50;
    
    public TestAccuracyVsPeriodLEngth() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @Test
    public void testPeriodLength() {
       int[] signal = generateSeries();
           printSeries(signal);
       
    }

    private int[] generateSeries() {
        int signal[] = new int[signalLength];
        int currPeriod = period[0] * period[1];
        for(int k = currPeriod; k < signalLength; k+= currPeriod) {
            signal[k] += signalStrength;
        }
        return signal;
    }

    private void printSeries(int[] signal) {
        System.out.print("A = [");
        for(int i = 1; i < signal.length; i++) {
            System.out.print(signal[i] + " ");
        }
        System.out.print("];\n\n");
    }
}
