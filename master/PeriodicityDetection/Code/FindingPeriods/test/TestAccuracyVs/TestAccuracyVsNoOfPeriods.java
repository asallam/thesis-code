package TestAccuracyVs;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author asma
 */
public class TestAccuracyVsNoOfPeriods {
    
    int[] period =// {2, 19, 41, 61, 97};
    //{17, 19, 23, 29};
    //{17, 19, 23, 29};
    {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997};
    int signalLength = 10000;
    int signalStrength = 50;
    
    public TestAccuracyVsNoOfPeriods() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testNoOfPeriods() {
       for(int i = 0; i < period.length; i++) {
           int[] signal = generateSeries(i);
           printSeries(signal);
           //printSeries2(signal);
       }
    }

    private int[] generateSeries(int i) {
        int signal[] = new int[signalLength];
        for(int j = 0; j <= i; j++) {
            int currPeriod = period[j];
            for(int k = currPeriod; k < signalLength; k+= currPeriod) {
                signal[k] += signalStrength;
            }
        }
        return signal;
    }
    
    private int[] generateSeries2() {
        int signal[] = new int[signalLength];
        for(int j = 0; j <= i; j++) {
            int currPeriod = period[j];
            for(int k = currPeriod; k < signalLength; k+= currPeriod) {
                signal[k] += signalStrength;
            }
        }
        
        return signal;
    }
    

    private void printSeries(int[] signal) {
        System.out.print("A = c(");
        for(int i = 1; i < signal.length; i++) {
            System.out.print(signal[i] + ",");
        }
        System.out.print(");\n\n");
        System.out.println("chisq.pd(A, 2, 72, .05)");
        
        System.out.print("t = [");
        int timeSignal[] = new int[signalLength];
        for(int k = 0; k < signalLength; k++) {
            timeSignal[k] = k;
        }
        for(int i = 1; i < signal.length; i++) {
            System.out.print(timeSignal[i] + " ");
        }
        System.out.print("];\n\n");
    }
    
    private void printSeries2(int[] signal) {
        //System.out.print("A = [");
        for(int i = 1; i < signal.length; i++) {
            System.out.println(i + " " + signal[i] + " 0");
        }
    }
}
