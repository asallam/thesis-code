package TestAccuracyOfAlignment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author asma
 */
public class Uniform_OneChange {
    
    int signalLength = 100;
    int signalStrength = 50;
    int trainingLength = 3;
    int anomalyLength = 1;
    
    public Uniform_OneChange() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testNoOfPeriods() {
        printSeries(generateSeries(), 'A');
        printSeries(generateTraining(), 'B');
    }

    private int[] generateTraining() {
        int signal[] = new int[trainingLength];
        for(int k = 0; k < trainingLength; k+= 1) {
            signal[k] += signalStrength;
        }
        
        int anomalyLoc = (int)Math.ceil(trainingLength / 2);
        signal[anomalyLoc] += anomalyLength;
        
        
        return signal;
    }
    
    private int[] generateSeries() {
        int signal[] = new int[signalLength];
        for(int k = 0; k < signalLength; k+= 1) {
            signal[k] += signalStrength;
        }
        return signal;
    }

    private void printSeries(int[] signal, char signalName) {
        System.out.print(signalName + " = [");
        for(int i = 0; i < signal.length; i++) {
            System.out.print(signal[i] + " ");
        }
        System.out.print("];\n\n");
    }
}
