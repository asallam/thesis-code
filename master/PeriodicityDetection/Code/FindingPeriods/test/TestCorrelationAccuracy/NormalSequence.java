/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestCorrelationAccuracy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Random;
import org.apache.commons.math3.distribution.ZipfDistribution;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author asma
 */
public class NormalSequence {

    int trainingLength = 1000;
    int MAX_SIGNAL_STRENGTH = 50;

    float detectionPortions[] = {.01f, 0.05f, 0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f};

    @Test
    public void start() {
        int[] training = generateZipfTraining();//generateRandomTraining();
        for (int i = 0; i < detectionPortions.length; i++) {
            generateDetection(training, detectionPortions[i]);
        }
    }

    private int[] generateZipfTraining() {
        int signal[] = new int[trainingLength];
        int s = 1;
        ZipfDistribution z = new ZipfDistribution(trainingLength, s);

        for (int k = 0; k < trainingLength; k += 1) {
            signal[k] += z.sample();
        }

        //printSeries(detectionSeries, "A");
        return signal;
    }

    private int[] generateDetection(int[] trainingSeries, float detectionPortion) {
        int detectionLength = (int) (detectionPortion * trainingLength);
        int[] detectionSeries = new int[detectionLength];

        Random r = new Random();
        int start = Math.abs(r.nextInt()) % (trainingSeries.length - detectionLength);
        
        for (int i = 0; i < detectionLength; i++) {
            detectionSeries[i] = trainingSeries[start + i];
        }

        //addOneRandomly(trainingSeries, detectionSeries);
        float percentagesToAdd[] = {0.5f, 1.0f, 1.5f, 2.0f, 2.5f, 3.0f};//, 3.0f, 3.5f, 4.0f, 5.0f, 6.0f};
        ArrayList<ArrayList<Double>> allCorrs = new ArrayList<>();
        for (int i = 0; i < percentagesToAdd.length; i++) {
            ArrayList<Double> corr1 = addNPercentageRandomly(trainingSeries, detectionSeries, percentagesToAdd[i]);
            allCorrs.add(corr1);
        }
        System.out.println();
//        for (int k = 0; k < allCorrs.get(0).size(); k++) {
//            for (ArrayList<Double> allCorr : allCorrs) {
//                System.out.print(allCorr.get(k) + " ");
//            }
//            System.out.println();
//        }
        return detectionSeries;
    }

    private void printSeries(int[] signal, String signalName) {
        System.out.print(signalName + " = [");
        for (int i = 0; i < signal.length; i++) {
            System.out.print(signal[i] + " ");
        }
        System.out.print("];\n\n");
    }

    private ArrayList<Double> addNPercentageRandomly(int[] trainingSeries, int[] signal, float percentageToAdd) {
        ArrayList<Double> allCorrs = new ArrayList();
        ArrayList<Integer> percentage = new ArrayList<>();
        percentage.add(0);percentage.add(10);percentage.add(20);percentage.add(30);percentage.add(40);percentage.add(50);
        percentage.add(60);percentage.add(70);percentage.add(80);percentage.add(90);percentage.add(100);
        for (int i = 0; i < percentage.size(); i++) {
            int detection1[] = Arrays.copyOf(signal, signal.length);
            int numChanged = percentage.get(i) * detection1.length / 100;
            Random r = new Random();
            for (int j = 0; j < numChanged; j++) {
                int index = Math.abs(r.nextInt()) % detection1.length;

                detection1[index] += (int) (detection1[index] * percentageToAdd);
            }
            //printSeries(detection1, "B_" + i);
            double diff = getDifference(trainingSeries, detection1);
            //System.out.println(diff);
            allCorrs.add(diff);
        }
        double falsePositives = computeFalsePositives(allCorrs, percentage);
        double falseNegatives = computeFalseNegatives(allCorrs, percentage);
        System.out.printf("%f %f %f\n", percentageToAdd, falsePositives, falseNegatives);
        return allCorrs;
    }
    
    // compute the normalized cross-correlation
    double getDifference(int[] trainingSeries, int[] detectionSeries) {
        double min = Double.MAX_VALUE;
        for (int i = 0; i < trainingSeries.length - detectionSeries.length; i++) {
            double newCorr = getDifference(i, trainingSeries, detectionSeries);
            if (newCorr < min) {
                min = newCorr;
            }
        }
        return min;
    }

    private double getDifference(int start, int[] trainingSeries, int[] detectionSeries) {
        double sum = 0;
        for (int k = 0; k < detectionSeries.length; k++) {
            int diff = (detectionSeries[k] - trainingSeries[k + start]) / trainingSeries[k + start];
            if(diff > 0) {
                sum += diff;
            }
        }
        return sum / detectionSeries.length;
    }

    private double computeFalsePositives(ArrayList<Double> allCorrs, ArrayList<Integer> Xs) {
        double falsePositives =  0;
        int num = 0;
        for(int i = 0; i < allCorrs.size() - 1; i++) {
            if(allCorrs.get(i) > allCorrs.get(i + 1)) {
                double x0 = 0;
                if(i != 0) {
                    x0 = Xs.get(i - 1);
                }
                double x1 = Xs.get(i + 1);
                double x = Xs.get(i);
                
                double y2 = allCorrs.get(i + 1);
                if(i != 0) {
                   y2 -=  allCorrs.get(i - 1);
                }
                
                double y = allCorrs.get(i);
                if(i != 0) {
                   y -=  allCorrs.get(i - 1);
                }
                
                double y_prime;
                double falsePositive;
                if(y2 == 0) {
                    falsePositive = 0;
                } else {
                    y_prime = (y2) * (x - x0) / (x1 - x0);
                    falsePositive = (y - y_prime) / y_prime;
                }
                
                num++;
                if(falsePositive > 0) {
                    falsePositives += falsePositive;
                }
                
            }
        }
        if(num == 0) {
            return 0;
        }
        return falsePositives / num;
    }

    private double computeFalseNegatives(ArrayList<Double> allCorrs, ArrayList<Integer> Xs) {
        double falseNegatives =  0;
        int num = 0;
        for(int i = 1; i < allCorrs.size() - 1; i++) {
            if(allCorrs.get(i) < allCorrs.get(i - 1)) {    
                         
                double x0 = Xs.get(i - 1);
                double x1 = Xs.get(i + 1);
                double x = Xs.get(i);
                
                double y1 = allCorrs.get(i - 1);
                double y2 = allCorrs.get(i + 1);
                double y = allCorrs.get(i);
                
                
                num++;
                
                double y_prime;
                double falseNegative;
                if(y2 == 0) {
                    falseNegative = 0;
                } else {
                    y_prime = y1 + (y2 - y1) * (x - x0) / (x1 - x0);
                    falseNegative = (y_prime - y) / y_prime;
                }
                
                if(falseNegative > 0) {
                    falseNegatives += falseNegative;
                }
            }
        }
        if(num == 0) {
            return 0;
        }
        return falseNegatives / num;
    }

}
