/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiscoveringBlocks;

import java.util.Date;

/**
 *
 * @author asma
 */
public class QueryAndTime {
    String queryStr;
    Date queryTime;
    long timeDiff;
    
    public QueryAndTime(String queryStr, Date queryTime, long timeDiff) {
        this.queryStr = queryStr;
        this.queryTime = queryTime;
        this.timeDiff = timeDiff;
    }
    
    public String getQueryString() {
        return queryStr;
    }
    
    public long getTimeDiff() {
        return timeDiff;
    }
    
}
