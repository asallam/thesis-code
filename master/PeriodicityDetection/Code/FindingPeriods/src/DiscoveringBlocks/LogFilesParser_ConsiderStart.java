/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiscoveringBlocks;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class LogFilesParser_ConsiderStart {
    
    HashMap<String, QueryTimeSeries> queryStartTimes = new HashMap<>();
    
    public void parseFile(String fileName) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();
            
            Date firstBeginTime = null;

            while (line != null) {
                if (line.contains("BEGIN")) {

                    Date beginTime = getBeginTime(line);
                    
                    if(firstBeginTime == null) {
                        firstBeginTime = beginTime;
                    }

                    // TODO Should consider that there may be other concurrent sessions
                    while (line != null && !line.contains("\",\"parameters:")) {
                        line = br.readLine();
                    }
                    if (line != null && line.contains("\",\"parameters:")) {
                        String[] lineParts = line.split(",");
                        String queryTime = lineParts[0];

                        String query = composeQuery(line);

                        Date firstQueryTime = formatTime(queryTime);

                        QueryTimeSeries QTS = queryStartTimes.get(query);
                        
                        if (QTS == null) {
                            QTS = new QueryTimeSeries();
                            queryStartTimes.put(query, QTS);
                            
                        }
                        ArrayList<QueryAndTime> nextQueries = new ArrayList<>();
                        line = br.readLine();
                        do {
                            if (line != null && line.contains("\",\"parameters:")) {
                                QueryAndTime qAT = getQueryAndTime(line, beginTime);
                                nextQueries.add(qAT);
                            }
                            line = br.readLine();
                        } while (line != null && !(line.contains("COMMIT") || line.contains("ROLLBACK")));

                        QTS.addTime(query, firstBeginTime, beginTime, firstQueryTime, nextQueries);
                        if(QTS.getNumEntries() >= 100) {
                            break;
                        }
                    }
                }

                
                
                line = br.readLine();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String composeQuery(String line) {
        String query = line.substring(line.indexOf(":", line.indexOf("execute ") + 8) + 2, line.indexOf("\",\"parameters:"));
        query = query.replaceAll("\"", "");
        return query;
    }

    public Date formatTime(String queryTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss.SSS");
        Date queryFormattedTime;
        try {
            queryFormattedTime = sdf.parse(queryTime);
            return queryFormattedTime;
        } catch (ParseException ex) {
            Logger.getLogger(LogFilesParser_ConsiderStart.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private long getDiff(Date queryFormattedTime, Date firstQueryTime) {
        long diff = queryFormattedTime.getTime() - firstQueryTime.getTime();
        return diff;
    }

    private Date getBeginTime(String line) {
        String[] lineParts = line.split(" ");
        String dateTime = lineParts[0] + " " + lineParts[1];
        Date beginTime = formatTime(dateTime);
        return beginTime;
    }

    private QueryAndTime getQueryAndTime(String queryLine, Date startTime) {

        String[] lineParts = queryLine.split(",");
        String queryTime = lineParts[0];

        String query = composeQuery(queryLine);
        Date queryFormattedTime = formatTime(queryTime);
        
        long timeDiff = getDiff(queryFormattedTime, startTime);

        QueryAndTime qAT = new QueryAndTime(query, queryFormattedTime, timeDiff);
        return qAT;
    }

    public HashMap<String, QueryTimeSeries> getQueriesStartTimes() {
        return this.queryStartTimes;
    }
    
 }
