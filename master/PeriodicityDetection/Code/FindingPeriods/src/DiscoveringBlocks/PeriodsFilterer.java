/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiscoveringBlocks;

import findingperiods.Bucket;
import findingperiods.PeriodInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author asma
 */
public class PeriodsFilterer {
    
    final static int MAX_NETWORK_DELAY = 1000;
    final static int MAX_PERIOD_DIFF = 100;
    
    final static float THRESOLD = 0.80f;
    
    ArrayList<PeriodInfo> confirmedPeriods;
    
    private boolean isInconfirmedPeriods(int periodLength) {
        for (PeriodInfo p : confirmedPeriods) {
            int confirmedPeriodLength = p.getPeriodLength();
            if (periodLength >= confirmedPeriodLength
                    && ((periodLength % confirmedPeriodLength < MAX_PERIOD_DIFF) || (confirmedPeriodLength - periodLength % confirmedPeriodLength < MAX_PERIOD_DIFF))) {
                return true;
            }
        }
        return false;
    }
    
    public void findPeriods(int resolution, int[] initialPeriods, HashMap<String, QueryTimeSeries> queryStartTimes) {
        Set<Map.Entry<String, QueryTimeSeries>> entrySet = queryStartTimes.entrySet();
        
        for(Map.Entry oneEntry : entrySet) {
            String queryStr = (String) oneEntry.getKey();
            QueryTimeSeries QTS = (QueryTimeSeries) oneEntry.getValue();
            
            QTS.composeSeries(resolution);
            
            int[] timeSeries = QTS.getSignal();
            Bucket[] buckets = QTS.getBuckets();
            
            printSignal(timeSeries);
            
            // TODO each query has different periods
           
            ArrayList<Integer> initialPeriodsAL = new ArrayList<>();
            for(int i = 0; i < initialPeriods.length; i++) {
                initialPeriodsAL.add(initialPeriods[i]);
            }
            
            filterPeriods(resolution, timeSeries, buckets, initialPeriodsAL);
            
            QTS.findNextQueries(confirmedPeriods);
            
            QTS.formStateMachine();
        }
    }
    
    private void printSignal(int[] signal) {
        for (int i = 0; i < signal.length; i++) {
            System.out.print(signal[i]);
            if(i != signal.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();
    }
    
    public void filterPeriods(int resolution, int[]signal, Bucket[] buckets, ArrayList<Integer> initialPeriods) {
        
        confirmedPeriods = new ArrayList<>();
        int delta;

        Collections.sort(initialPeriods);

        for (Integer periodLength : initialPeriods) {

            if (periodLength == 0) {
                continue;
            }
            
            if(isInconfirmedPeriods(periodLength)) {
                continue;
            }
            
            long finalBefore = 0;
            long finalAfter = 0;
            long finalStart = 0;
            int maxNumMatches = 0;

            for (int j = 0; j < periodLength; j++) {
                long startTime1 = 0;
                int numMatches = 0;

                delta = (int) ((float) MAX_NETWORK_DELAY / (2 * resolution));

                if (signal[j] > 0) {
                    long minAfter = Long.MAX_VALUE;
                    long minBefore = Long.MAX_VALUE;

                    Bucket bucket0 = buckets[j];

                    for (long q0 : bucket0.getQueriesTimes()) {
                        long maxAfter1 = Long.MIN_VALUE;
                        long maxBefore1 = Long.MIN_VALUE;

                        for (int k = j + periodLength; k < signal.length; k += periodLength) {

                            boolean matched = false;

                            long minTimeDiff = Long.MAX_VALUE;
                            long intervalContr = 0;

                            if (buckets[k] != null) {
                                ArrayList<Long> thisBuckets = buckets[k].getQueriesTimes();
                                for (long b : thisBuckets) {
                                    long requiredDist = (long) (k - j) * resolution;
                                    long diff = requiredDist - (b - q0);
                                    if (diff < 0) {
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = Math.abs(diff);
                                            intervalContr = -minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    } else {
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = diff;
                                            intervalContr = minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    }
                                }
                                matched = true;
                            }
                            for (int d = 1; d <= delta; d++) {
                                if (k - d >= 0 && buckets[k - d] != null) {
                                    ArrayList<Long> beforeBuckets = buckets[k - d].getQueriesTimes();
                                    for (long b : beforeBuckets) {
                                        long requiredDist = (long) (k - j) * resolution;
                                        long diff = requiredDist - (b - q0);
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = diff;
                                            intervalContr = -minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    }
                                    matched = true;
                                }
                                if (k + d <= buckets.length - 1 && buckets[k + d] != null) {
                                    ArrayList<Long> afterBuckets = buckets[k + d].getQueriesTimes();
                                    for (long b : afterBuckets) {
                                        long requiredDist = (long) (k - j) * resolution;
                                        long diff = (b - q0) - requiredDist;
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = diff;
                                            intervalContr = minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    }
                                    matched = true;
                                }
                            }

                            if (matched) {
                                if (intervalContr < 0) {
                                    if (maxBefore1 < -intervalContr) {
                                        maxBefore1 = -intervalContr;
                                    }
                                } else {
                                    if (maxAfter1 < intervalContr) {
                                        maxAfter1 = intervalContr;
                                    }
                                }
                                numMatches++;
                            }
                        }

                        if (maxBefore1 < minBefore) {
                            minBefore = maxBefore1;
                        }
                        if (maxAfter1 < minAfter) {
                            minAfter = maxAfter1;
                        }
                    }

                    if (numMatches > maxNumMatches) {
                        maxNumMatches = numMatches;
                        finalStart = startTime1;
                        finalBefore = minBefore;
                        finalAfter = minAfter;
                    }
                }
            }
            int numExpectedAppearances = signal.length / periodLength;
            if (maxNumMatches > THRESOLD * numExpectedAppearances) {
                PeriodInfo periodInfo = new PeriodInfo();
                periodInfo.setPeriodLength(periodLength);
                if (finalBefore <= 0) {
                    finalBefore = finalAfter;
                }
                periodInfo.setIntStart(finalBefore);
                if (finalAfter <= 0) {
                    finalAfter = finalBefore;
                }
                periodInfo.setInEnd(finalAfter);
                periodInfo.setStartTime(finalStart);
                periodInfo.setResolution(resolution);
                float percentageMissed = 1 - (float) maxNumMatches / numExpectedAppearances;
                periodInfo.setPercentageMissed(percentageMissed);
                
                confirmedPeriods.add(periodInfo);
                
//                PeriodInfo similarPeriod = isInconfirmedPeriods(periodLength);
                
//                if(similarPeriod != null) {
//                    if(similarPeriod.getPercentageMissed() > percentageMissed) {
//                        confirmedPeriods.remove(similarPeriod);
//                        confirmedPeriods.add(periodInfo);
//                    }                    
//                } else {
//                    confirmedPeriods.add(periodInfo);
//                }
            }
        }
    }
}
