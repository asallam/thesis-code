/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiscoveringBlocks;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asma
 */
public class Main {
    public static void main(String[] strs) {
        
        int[] resolutions = {200};

        // for 200, 500  resolution and simple logs in
        // /home/asma/Desktop/PG_logs/frequency/relationships/tatp
        /*int[][] initialPeriods = {
            {0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75},
            {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30}
        };*/
        
        // InsertCallForwarding: 
        // 200: 0   -10    10   -75   -50   -45   -30    30    45    50    75   -35   -25    25    35  -100   100   -20   -15    15
                // 20   -95   -65    65    95  -145   -40    40   -90   -80
        // 500:  0    -2     2    -4     4    -6     6    -8     8    10   -10   -12    12   -14    14   -16    16   -18    18   -20
        // 20   -22    22   -24    24   -26    26   -28    28   -30
        
        // write check
        // 500: 0, 2, 9, 11, 5, 27, 32, 25, 18, 6, 7, 18, 25, 32, 30, 15, 41, 35, 23, 20, 15, 23, 30, 35
        
        int[][] initialPeriods = {
            // WriteCheck
            //{0, 2, 9, 11, 5, 27, 32, 25, 18, 6, 7, 18, 25, 32, 30, 15, 41, 35, 23, 20, 15, 23, 30, 35}
            {5}
        };
        
        LogFilesParser_ConsiderStart lfp = new LogFilesParser_ConsiderStart();
        //lfp.parseFile("/home/asma/Desktop/PG_logs/frequency/relationships/tpcc_1.csv");
        lfp.parseFile("/home/asma/Desktop/PG_logs/frequency/relationships/tpcc/StockLevel.csv");
        
        HashMap<String, QueryTimeSeries> h = lfp.getQueriesStartTimes();
        
        PeriodsFilterer pf = new PeriodsFilterer();
        pf.findPeriods(resolutions[0], initialPeriods[0], h);
        
        for(Map.Entry oneEntry : h.entrySet()) {
            QueryTimeSeries qts = (QueryTimeSeries) oneEntry.getValue();
            qts.checkDetectionQueries();
        }
        /*for (int i = 0; i < resolutions.length; i++) {
            LogFilesParser_ConsiderStart lfp = new LogFilesParser_ConsiderStart();
            lfp.parseFile("/home/asma/Desktop/PG_logs/frequency/relationships/UpdateSubscriberData.csv");
            lfp.findPeriods(resolutions[i], initialPeriods[i]);
        }*/
    }

    
}
