/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DiscoveringBlocks;

import findingperiods.Bucket;
import findingperiods.PeriodInfo;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author asma
 */
public class QueryTimeSeries {

    String query;

    ArrayList<Long> beginTimes;
    ArrayList<Long> queryTimes;
    
    ArrayList<PeriodInfo> confirmedPeriods;

    ArrayList<ArrayList<QueryAndTime>> nextStrings;

    int numTrainingEntries;
    
    Bucket[] buckets;
    int[] signal;

    ArrayList<ArrayList<QueryAndTime>> confirmedNextQueries;
    
    StateMachine stateMachine;

    public QueryTimeSeries() {
        beginTimes = new ArrayList<>();
        queryTimes = new ArrayList<>();
        nextStrings = new ArrayList<>();
    }

    public void addTime(String query, Date firstBeginTime, Date beginTime, Date firstQueryTime, ArrayList<QueryAndTime> nextQueries) {
        this.query = query;
        
        long relativeBeginTime = getDiff(beginTime, firstBeginTime);
        beginTimes.add(relativeBeginTime);
                
        long relativeQueryTime = getDiff(firstQueryTime, beginTime);
        queryTimes.add(relativeQueryTime);
        
        nextStrings.add(nextQueries);        
    }

    private long getDiff(Date date1, Date date0) {
        long diff = date1.getTime() - date0.getTime();
        return diff;
    }

    public void composeSeries(int resolution) {
        
        numTrainingEntries = (int) Math.floor(0.8 * beginTimes.size());
        
        int numEntries = (int) (beginTimes.get(numTrainingEntries - 1) / resolution) + 1;

        signal = new int[numEntries];
        buckets = new Bucket[numEntries];

        for (int i = 0; i < numTrainingEntries; i++) {
            try {
                long beginTime = beginTimes.get(i);
                
                int index = (int) (beginTime / resolution);
                signal[index]++;
                if (buckets[index] == null) {
                    buckets[index] = new Bucket();
                }
                buckets[index].addQueryTime(beginTime);
            } catch (Exception e) {
                System.err.println("ERROR");
            }
        }
    }

    Bucket[] getBuckets() {
        return buckets;
    }

    int[] getSignal() {
        return signal;
    }

    public void findNextQueries(ArrayList<PeriodInfo> confirmedPeriods) {
        this.confirmedPeriods = confirmedPeriods;
        confirmedNextQueries = new ArrayList<>();
        // TODO remove 
        for (int i = 0; i < numTrainingEntries; i++) {
            confirmedNextQueries.add(nextStrings.get(i));
        }
        // TODO uncomment
        /*for (PeriodInfo periodInfo : confirmedPeriods) {
            for (int i = 0; i < numTrainingEntries; i++) {
                // TODO remove 
                confirmedNextQueries.add(nextStrings.get(i));
                
                
                long issueTime = beginTimes.get(i);
                
                for (long t = periodInfo.getStartTime();; t += (periodInfo.getPeriodLength() * periodInfo.getResolution())) {
                    if (issueTime >= (t - periodInfo.getIntStart()) && issueTime <= (t + periodInfo.getIntEnd())) {
                        confirmedNextQueries.add(nextStrings.get(i));
                        break;
                    }
                    if (issueTime < t) {
                        break;
                    }
                }
            }
        }*/
    }

    public void formStateMachine() {
        QueryInStateMachine startQuery = new QueryInStateMachine(query);
        
        long maxTimeDiff = Long.MIN_VALUE;
        for(long queryTime : queryTimes) {
            if(queryTime > maxTimeDiff) {
                maxTimeDiff = queryTime;
            }
        }
        
        startQuery.addPrevTimeDiff(maxTimeDiff);

        for (ArrayList<QueryAndTime> c0 : confirmedNextQueries) {
            QueryInStateMachine previousQuery = startQuery;
            
            for(int j = 0; j < c0.size(); j++) {
                QueryAndTime qat = c0.get(j);
                
                long timeDiff;
                if(j == 0) {
                    timeDiff = qat.getTimeDiff();
                } else {
                    QueryAndTime prevQat = c0.get(j - 1);
                    timeDiff = qat.getTimeDiff() - prevQat.getTimeDiff();
                }
                
                QueryInStateMachine nextQuery = previousQuery.addNextQuery(qat.getQueryString());
                
                if(nextQuery == previousQuery) {
                    nextQuery.addTimeDiffToSelf(timeDiff);
                } else {
                    nextQuery.addPrevTimeDiff(timeDiff);
                }
                
                previousQuery = nextQuery;
            }
        }
        
        ArrayList<QueryInStateMachine> nextAnticipatedQueries = new ArrayList<>();
        nextAnticipatedQueries.add(startQuery);
        // TODO remove commenting
        stateMachine = new StateMachine(null/*confirmedPeriods.get(0)*/, nextAnticipatedQueries, beginTimes.get(numTrainingEntries - 1));
        
    }
    
    public void checkDetectionQueries() {
        int numAnomalies = 0;
        int numQueries = 0;
        
        for(int i = numTrainingEntries; i < beginTimes.size(); i++) {
            System.out.println("b: " + beginTimes.get(i));
            numAnomalies += stateMachine.checkQuery(this.query, beginTimes.get(i), queryTimes.get(i));
            numQueries++;
            for(QueryAndTime qat : nextStrings.get(i)) {
                numAnomalies += stateMachine.checkQuery(qat.getQueryString(), beginTimes.get(i), qat.getTimeDiff());
                numQueries++;
            }
        }
        
        System.out.println(numAnomalies + "/" + numQueries);
    }

    int getNumEntries() {
        return beginTimes.size();
    }
}
