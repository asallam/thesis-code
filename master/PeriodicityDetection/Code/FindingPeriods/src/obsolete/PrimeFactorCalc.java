package obsolete;



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import findingperiods.PeriodInfo;
import java.util.ArrayList;

/**
 *
 * @author asma
 */
public class PrimeFactorCalc {

    // TODO
    public static int THRESHOLD = 1;
    public static int DELTA = 0;
    int[] signal;
    
    public PrimeFactorCalc(int[] signal) {
        this.signal = signal;
    }
    
    ArrayList<Integer> findPrimeFactors(int n) {
        ArrayList<Integer> primeFactors = new ArrayList<>();
        int i = 2;
        while (n > 1) {
            if (n % i == 0) {
                primeFactors.add(i);
                n = n / i;
            } else {
                i++;
            }
        }
        return primeFactors;
    }
    
    public ArrayList<PeriodInfo> findPeriods() {
        
        
        int overAllPeriod = findOverallPeriodUsingMatlab();
        if (overAllPeriod == 0) {
            return null;
        }
        
        ArrayList<Integer> primeFactors = findPrimeFactors(overAllPeriod);
        
        ArrayList<PeriodInfo> rejectedPeriods = new ArrayList<>();
        ArrayList<PeriodInfo> acceptedPeriods = new ArrayList<>();
        
        for(int i = 1; i <= primeFactors.size(); i++) {
            ArrayList<ArrayList<Integer>> iCombincations = composeICandidatePeriods(primeFactors, i);
            
            for(ArrayList <Integer>combination : iCombincations) {
                int periodLength = getPeriodLength(combination);
                if(rejectedPeriods.contains(new PeriodInfo(periodLength))
                        || acceptedPeriods.contains(new PeriodInfo(periodLength))) {
                    break;
                }
                int signalStrength = isPeriod(signal, combination);
                if(signalStrength > 0) {
                    if (!acceptedPeriods.contains(new PeriodInfo(periodLength))) {
                        PeriodInfo periodInfo = new PeriodInfo(combination, signalStrength);
                        
                        acceptedPeriods.add(periodInfo);
                        
                    }
                } else {
                    rejectedPeriods.add(new PeriodInfo(periodLength));
                }
            }
        }
        
        return acceptedPeriods;
    }
    
    public int findOverallPeriodUsingMatlab() {
        return 210;
    }

    private ArrayList<ArrayList<Integer>> composeICandidatePeriods(ArrayList<Integer> primeFactors, int i) {
        return getCombinations(primeFactors, -1, i);
    }

    // TODO change this - assumes a perfect signal, 
    // take samples may be
    // start backwards
    // returns the singal strength
    private int isPeriod(int[] signal, ArrayList<Integer> combination) {
        int periodLength = 1;
        for(Integer i : combination) {
            periodLength *= i;
        }
        
        int maxOfMins = Integer.MIN_VALUE;
        int start = -1;
        for(int w = 0; w <= periodLength; w++) {
            int min = Integer.MAX_VALUE;
            for(int n = 0; n < signal.length - w; n += periodLength) {
                if (min > signal[w + n]) {
                    min = signal[w + n];
                }
            }
            if(maxOfMins < min) {
                maxOfMins = min;
                start = w;
            }
        }
        if(maxOfMins > 0) {
            for(int n = start; n < signal.length; n += periodLength) {
                signal[n] -= maxOfMins;
            }  
        }
        
        return maxOfMins;
        
    }

    private ArrayList<ArrayList<Integer>> getCombinations(ArrayList<Integer> primeFactors, int lastIndex, int length) {
        if (length == 0) {
            return null;
        }
        if (lastIndex == primeFactors.size()) {
            return null;
        }
        ArrayList<ArrayList<Integer>> allLists = new ArrayList<>();
        for(int k = lastIndex + 1; k < primeFactors.size(); k++) {
            ArrayList<ArrayList<Integer>> lessCombinations = getCombinations(primeFactors, k, length - 1);
            if(lessCombinations != null) {
               for(ArrayList<Integer> list : lessCombinations) {
                   list.add(primeFactors.get(k));
                   allLists.add(list);
               } 
            } else {
                ArrayList<Integer> listOfOneItem = new ArrayList<>();
                listOfOneItem.add(primeFactors.get(k));
                allLists.add(listOfOneItem);
            }
        }
        return allLists;
    }

    private int getPeriodLength(ArrayList<Integer> combination) {
        int periodLength = 1;
        for(Integer i : combination) {
                periodLength *= i;
        }
        return periodLength;
    }

}
