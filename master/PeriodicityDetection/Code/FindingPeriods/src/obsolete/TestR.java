/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obsolete;

import findingperiods.*;
import obsolete.GaussianRandom;
import java.util.Random;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

/**
 *
 * @author asma
 */
public class TestR {

    /*public static void main(String[] args) {
     try {
     RConnection c = new RConnection();

     int[] allPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673};

     int count = 0;
     int countCorrect = 0;

     long totalTime = 0;
     for (int i = 0; i < allPrimes.length; i++) {
     for (int j = i + 1; j < allPrimes.length; j++) {
     int p1 = allPrimes[i];
     int p2 = allPrimes[j];
     System.out.println(allPrimes[i] + "," + allPrimes[j]);
     int[] signal = getSignal(allPrimes[i], allPrimes[j]);

     long startTime = System.currentTimeMillis();
     int[] periods = getPeriods(c, signal);
     long endTime = System.currentTimeMillis();
     totalTime += (endTime - startTime);
     if (contains(periods, p1)) {
     countCorrect++;
     }
     count++;
     if (contains(periods, p2)) {
     countCorrect++;
     }
     count++;
     }
     }
     System.out.println("Count = " + count);
     System.out.println("CountCorrect = " + countCorrect);
     long avTime = totalTime / (count / 2);
     System.out.println("Av time = " + avTime);
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    /*public static void main(String[] args) {
     try {
     RConnection c = new RConnection();

     int[] allPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673};

     int count = 0;
     int countCorrect = 0;

     long totalTime = 0;

     for (int i = 0; i < allPrimes.length; i++) {
     int p1 = allPrimes[i];
     System.out.println(allPrimes[i]);

     int[] signal = getSignal(allPrimes[i]);

     long startTime = System.currentTimeMillis();
     int[] periods = getPeriods(c, signal);
     long endTime = System.currentTimeMillis();
     long diff = endTime - startTime;
     totalTime += diff;

     if (contains(periods, p1)) {
     countCorrect++;
     }
     count++;

     }
     System.out.println("Count = " + count);
     System.out.println("CountCorrect = " + countCorrect);
     long avTime = totalTime / count;
     System.out.println("avTime = " + avTime);
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    /*public static void main(String[] args) {
        try {
            RConnection c = new RConnection();

            int[] allPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673};

            int count = 0;
            int countCorrect = 0;
            Random r = new Random();

            long totalTime = 0;

            for (int l = 0; l < 1000; l++) {
                int i = Math.abs(r.nextInt()) % allPrimes.length;
                int p1 = allPrimes[i];
                int j = Math.abs(r.nextInt()) % allPrimes.length;
                int p2 = allPrimes[j];
                int k = Math.abs(r.nextInt()) % allPrimes.length;
                int p3 = allPrimes[k];
                System.out.println(l + ": " + allPrimes[i] + "," + allPrimes[j] + "," + allPrimes[k]);
                int[] signal = getSignal(allPrimes[i], allPrimes[j], allPrimes[k]);
                long startTime = System.currentTimeMillis();
                int[] periods = getPeriods(c, signal);
                long endTime = System.currentTimeMillis();
                long diff = endTime - startTime;
                totalTime += diff;
                if (contains(periods, p1)) {
                    countCorrect++;
                }
                count++;
                if (contains(periods, p2)) {
                    countCorrect++;
                }
                count++;
                if (contains(periods, p3)) {
                    countCorrect++;
                }
                count++;
            }
            System.out.println("Count = " + count);
            System.out.println("CountCorrect = " + countCorrect);
            long avTime = totalTime / (count / 3);
            System.out.println("Av time = " + avTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    
    public static void main(String[] args) {
        try {
            RConnection c = new RConnection();

            int[] allPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673};

            int count = 0;
            int countCorrect = 0;
            Random r = new Random();

            long totalTime = 0;

            for (int l = 0; l < 1000; l++) {
                int i = Math.abs(r.nextInt()) % allPrimes.length;
                int p1 = allPrimes[i];
                int j = Math.abs(r.nextInt()) % allPrimes.length;
                int p2 = allPrimes[j];
                
                System.out.println(l + ": " + allPrimes[i] + "," + allPrimes[j]);
                int[] signal = getSignal(allPrimes[i], allPrimes[j]);
                long startTime = System.currentTimeMillis();
                int[] periods = getPeriods(c, signal);
                long endTime = System.currentTimeMillis();
                long diff = endTime - startTime;
                totalTime += diff;
                if (contains(periods, p1)) {
                    countCorrect++;
                }
                count++;
                if (contains(periods, p2)) {
                    countCorrect++;
                }
                count++;
            }
            System.out.println("Count = " + count);
            System.out.println("CountCorrect = " + countCorrect);
            long avTime = totalTime / (count / 2);
            System.out.println("Av time = " + avTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*public static void main(String[] args) {
     try {
     RConnection c = new RConnection();

     int[] allPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673};

     int count = 0;
     int countCorrect = 0;
            
     for(int i = 0; i < allPrimes.length; i++) {
     for(int j = i + 1; j < allPrimes.length; j++) {
     for(int k = j + 1; k < allPrimes.length; k++) {
     int p1 = allPrimes[i];
     int p2 = allPrimes[j];
     int p3 = allPrimes[k];
     System.out.println(allPrimes[i] + "," +  allPrimes[j] + "," + allPrimes[k]);
     int[] signal = getSignal(allPrimes[i], allPrimes[j], allPrimes[k]);
     int[] periods = getPeriods(c, signal);
     if(contains(periods, p1)) {
     countCorrect++;
     }
     count ++;
     if(contains(periods, p2)) {
     countCorrect++;
     }
     count ++;
     if(contains(periods, p3)) {
     countCorrect++;
     }
     count ++;
     }
     }
     } 
     System.out.println("Count = " + count);
     System.out.println("CountCorrect = " + countCorrect);
     } catch(Exception e) {
     e.printStackTrace();
     }
     }*/
    /*public static void main(String[] args) {
     try {
     RConnection c = new RConnection();

     int[] allPrimes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641, 643, 647, 653, 659, 661, 673};

     int count = 0;
     int countCorrect = 0;

     long totalTime = 0;
     for (int i = 0; i < allPrimes.length; i++) {
     int p1 = allPrimes[i];
     System.out.println(allPrimes[i]);
     int[] signal = getSignal(allPrimes[i]);

     long startTime = System.currentTimeMillis();
     int[] periods = getPeriods(c, signal);
     long endTime = System.currentTimeMillis();

     long diff = endTime - startTime;
     System.out.println(diff);
     totalTime += diff;

     if (contains(periods, p1)) {
     countCorrect++;
     }
     count++;

     }
     long avTime = totalTime / count;
     System.out.println("Count = " + count);
     System.out.println("CountCorrect = " + countCorrect);
     System.out.println("Time = " + avTime);
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/

    public static int[] getSignal(int v1) {
        int signalLength = 2 * 8064;
        int[] signal = new int[signalLength];
        int signalStrength = 1;
        for (int k = v1; k < signalLength; k += v1) {
            signal[k] += signalStrength;
        }
        //addGaussianNoise(signal);
        return signal;
    }

    private static void addGaussianNoise(int[] signal) {
        int signalLength = 8064;
        float noisePercentage = 0.5f;
        int noiseLength = 1;
        double noiseVariance = 10000;
        GaussianRandom r;
        r = new GaussianRandom(noiseLength, noiseVariance);
        int numNoiseSamples = (int) (noisePercentage * signalLength);
        for (int i = 0; i < numNoiseSamples; i++) {
            int x = (int) (Math.abs(r.getGaussian()) % signalLength);
            signal[x] += noiseLength;
        }
    }

    public static int[] getSignal(int v1, int v2, int v3) {
        int signalLength = 2 * 8064;
        int[] signal = new int[signalLength];
        int signalStrength = 50;
        for (int k = v1; k < signalLength; k += v1) {
            signal[k] += signalStrength;
        }
        for (int k = v2; k < signalLength; k += v2) {
            signal[k] += signalStrength;
        }
        for (int k = v3; k < signalLength; k += v3) {
            signal[k] += signalStrength;
        }

        return signal;
    }

    public static int[] getSignal(int v1, int v2) {
        int signalLength = 2 * 8064;
        int[] signal = new int[signalLength];
        int signalStrength = 50;
        for (int k = v1; k < signalLength; k += v1) {
            signal[k] += signalStrength;
        }
        for (int k = v2; k < signalLength; k += v2) {
            signal[k] += signalStrength;
        }
        return signal;
    }

    public static int[] getPeriods(RConnection c, int[] myvalues) throws RserveException,
            REXPMismatchException,
            REngineException {

        c.assign("myvalues", myvalues);

        // source the Palindrom function
        c.eval("source(\"/home/asma/Desktop/findPeriods.R\")");

        // call the function. Return true
        REXP v = c.eval("chisq.pd(myvalues, 2, 998, .05)");

        int[] periods = v.asIntegers();
        return periods;

    }

    private static boolean contains(int[] periods, int p1) {
        for (int i = 0; i < periods.length; i++) {
            if (periods[i] == p1) {
                return true;
            }
        }
        return false;
    }

}
