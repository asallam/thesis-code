package obsolete;

import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author asma
 */
public class GaussianRandom {
    double mean; 
    double variance;
    
    private Random fRandom;
    
    public GaussianRandom(double mean, double variance) {
        this.mean = mean;
        this.variance = variance;
        fRandom = new Random();
    }
  
    public double getGaussian() {
        return mean + fRandom.nextGaussian() * variance;
    }
}
