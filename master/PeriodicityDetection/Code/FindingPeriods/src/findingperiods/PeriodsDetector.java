/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package findingperiods;

import java.util.*;

/**
 *
 * @author asma
 */
public class PeriodsDetector {

    final static int MAX_NETWORK_DELAY = 1000;
    final static int MAX_PERIOD_DIFF = 100;

    float threshold = 0.80f;
    // number of milliseconds
    int resolution;// = 2;
    ArrayList<Integer> initialPeriods;// = new ArrayList<>(Arrays.asList(4, 6, 46, 10, 43, 8, 92, 38, 28, 5, 28));

    int delta;

    int[] signal;
    Bucket[] buckets;

    ArrayList<PeriodInfo> confirmedPeriods;

    ArrayList<Long> trainingQueriesTimes;
    ArrayList<Long> detectionQueriesTimes;

    public PeriodsDetector(int resolution, int[] periods) {
        this.resolution = resolution;
        initialPeriods = new ArrayList<>();
        for (int i = 0; i < periods.length; i++) {
            initialPeriods.add(periods[i]);
        }
    }

    public void filterPeriods() {

        confirmedPeriods = new ArrayList<>();

        Collections.sort(initialPeriods);

        for (Integer periodLength : initialPeriods) {

            if (periodLength == 0) {
                continue;
            }
            if (isInconfirmedPeriods(periodLength)) {
                continue;
            }
            long finalBefore = 0;
            long finalAfter = 0;
            long finalStart = 0;
            int maxNumMatches = 0;

            for (int j = 0; j < periodLength; j++) {
                long startTime1 = 0;
                int numMatches = 0;

                delta = (int) ((float) MAX_NETWORK_DELAY / (2 * resolution));

                if (signal[j] > 0) {
                    long minAfter = Long.MAX_VALUE;
                    long minBefore = Long.MAX_VALUE;

                    Bucket bucket0 = buckets[j];

                    for (long q0 : bucket0.getQueriesTimes()) {
                        long maxAfter1 = Long.MIN_VALUE;
                        long maxBefore1 = Long.MIN_VALUE;

                        for (int k = j + periodLength; k < signal.length; k += periodLength) {

                            boolean matched = false;

                            long minTimeDiff = Long.MAX_VALUE;
                            long intervalContr = 0;

                            if (buckets[k] != null) {
                                ArrayList<Long> thisBuckets = buckets[k].getQueriesTimes();
                                for (long b : thisBuckets) {
                                    long requiredDist = (long) (k - j) * resolution;
                                    long diff = requiredDist - (b - q0);
                                    if (diff < 0) {
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = Math.abs(diff);
                                            intervalContr = -minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    } else {
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = diff;
                                            intervalContr = minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    }
                                }
                                matched = true;
                            }
                            for (int d = 1; d <= delta; d++) {
                                if (k - d >= 0 && buckets[k - d] != null) {
                                    ArrayList<Long> beforeBuckets = buckets[k - d].getQueriesTimes();
                                    for (long b : beforeBuckets) {
                                        long requiredDist = (long) (k - j) * resolution;
                                        long diff = requiredDist - (b - q0);
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = diff;
                                            intervalContr = -minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    }
                                    matched = true;
                                }
                                if (k + d <= buckets.length - 1 && buckets[k + d] != null) {
                                    ArrayList<Long> afterBuckets = buckets[k + d].getQueriesTimes();
                                    for (long b : afterBuckets) {
                                        long requiredDist = (long) (k - j) * resolution;
                                        long diff = (b - q0) - requiredDist;
                                        if (Math.abs(minTimeDiff) > Math.abs(diff)) {
                                            minTimeDiff = diff;
                                            intervalContr = minTimeDiff;
                                            startTime1 = q0;
                                        }
                                    }
                                    matched = true;
                                }
                            }

                            if (matched) {
                                if (intervalContr < 0) {
                                    if (maxBefore1 < -intervalContr) {
                                        maxBefore1 = -intervalContr;
                                    }

                                } else {
                                    if (maxAfter1 < intervalContr) {
                                        maxAfter1 = intervalContr;
                                    }

                                }
                                numMatches++;
                            }
                        }

                        if (maxBefore1 < minBefore) {
                            minBefore = maxBefore1;
                        }
                        if (maxAfter1 < minAfter) {
                            minAfter = maxAfter1;
                        }
                    }

                    if (numMatches > maxNumMatches) {
                        maxNumMatches = numMatches;
                        finalStart = startTime1;
                        finalBefore = minBefore;
                        finalAfter = minAfter;
                    }
                }
            }
            int numExpectedAppearances = signal.length / periodLength;
            if (maxNumMatches > threshold * numExpectedAppearances) {
                PeriodInfo periodInfo = new PeriodInfo();
                periodInfo.setPeriodLength(periodLength);
                if (finalBefore < 0) {
                    finalBefore = 0;
                }
                periodInfo.setIntStart(finalBefore);
                if (finalAfter < 0) {
                    finalAfter = 0;
                }
                periodInfo.setInEnd(finalAfter);
                periodInfo.setStartTime(finalStart);
                periodInfo.setResolution(resolution);
                float percentageMissed = 1 - (float) maxNumMatches / numExpectedAppearances;
                periodInfo.setPercentageMissed(percentageMissed);
                confirmedPeriods.add(periodInfo);
            }
        }
    }

    public void composeSeries(int resolution) {
        Collections.sort(trainingQueriesTimes);

        int numEntries = (int) ((trainingQueriesTimes.get(trainingQueriesTimes.size() - 1)) / resolution) + 1;
        signal = new int[numEntries];

        buckets = new Bucket[numEntries];

        for (long queryTime : trainingQueriesTimes) {
            try {
                int index = (int) (queryTime / resolution);
                signal[index]++;
                if (buckets[index] == null) {
                    buckets[index] = new Bucket();
                }
                buckets[index].addQueryTime(queryTime);
                //System.out.print(index + ",");
            } catch (Exception e) {
                System.err.println("ERROR");
            }
        }
    }

    public void parseTrainingFile(String trainingFileName, String queryToDetect, float trainingPortion) {
        LogFilesParser lp = new LogFilesParser();

        lp.parseFile(trainingFileName, queryToDetect);

        trainingQueriesTimes = lp.getTrainingQueries(trainingPortion);
        detectionQueriesTimes = lp.getDetectionQueries(trainingPortion);
    }

    public void doTraining() {
        composeSeries(resolution);
        filterPeriods();
    }

    public void doDetection() {

        PeriodMatching pm = new PeriodMatching(confirmedPeriods.get(0));

        int numAnomalies = 0;

        for (Long detectionQueryTime : detectionQueriesTimes) {
            boolean isAnomaly = pm.checkQuery(detectionQueryTime);
            if (isAnomaly) {
                numAnomalies++;
            }
        }

        System.out.println("NumAnomalies = " + numAnomalies + "/" + detectionQueriesTimes.size());
    }

    public static void main(String[] args) {

        int[] resolution = {2, 5, 10, 20, 50, 100, 200, 500};

        int[][] initialPeriods = {{0, 11000, 3000, 11000, 1000, 1000, 3000, 15000, 3500, 500, 500, 3500, 7500, 7500, 15000, 4000, 1501, 17000, 8000, 2000},
            {0, 400, 400, 1400, 1400, 1200, 1200, 200, 600, 200, 600, 2000, 1800, 2400, 1800, 2000, 2400, 1000, 1000, 2600},
            {0, 200, 200, 400, 100, 100, 400, 300, 300, 700, 500, 500, 700, 900, 800, 600, 600, 800, 900, 1100},
            {0, 100, 100, 50, 50, 200, 200, 150, 150, 250, 250, 300, 400, 350, 300, 350, 400, 450, 450, 650},
            {0, 20, 20, 60, 40, 40, 60, 80, 80, 100, 100, 160, 120, 120, 160, 140, 140, 180, 180, 200},
            {0, 10, 10, 30, 40, 30, 40, 20, 20, 50, 50, 60, 80, 60, 80, 70, 70, 90, 90, 100},
            {0, 5, 5, 10, 20, 20, 15, 10, 15, 25, 25, 40, 40, 30, 30, 35, 35, 45, 45, 50},
            {0, 2, 2, 6, 6, 8, 4, 8, 4, 10, 10, 16, 12, 16, 12, 14, 14, 18, 18, 20}};
        
        for (int i = 0; i < resolution.length; i++) {
            PeriodsDetector pd = new PeriodsDetector(resolution[i], initialPeriods[i]);
            pd.parseTrainingFile("/home/asma/Desktop/PG_logs/frequency/epinions/1.csv", "SELECT avg(rating) FROM review r WHERE r.i_id=$1", 0.8f);
            pd.doTraining();
            //pd.printPeriods();
            System.out.println("-- resolution = " + resolution[i] + "--");
            if (pd.containsManyPeriods()) {
                System.err.println("ERROR");
            } else {
                pd.doDetection();
            }
        }

    }

    private boolean isInconfirmedPeriods(int periodLength) {
        for (PeriodInfo p : confirmedPeriods) {
            int confirmedPeriodLength = p.getPeriodLength();
            if (periodLength >= confirmedPeriodLength
                    && ((periodLength % confirmedPeriodLength < MAX_PERIOD_DIFF) || (confirmedPeriodLength - periodLength % confirmedPeriodLength < MAX_PERIOD_DIFF))) {
                return true;
            }
        }
        return false;
    }

    private void printSignal() {
        for (int i = 0; i < signal.length; i++) {
            System.out.print(signal[i] + ", ");
        }
        System.out.println();
    }

    private void printPeriods() {
        for (PeriodInfo p : confirmedPeriods) {
            System.out.println(p.periodLength + ", " + p.percentageMissed + ", [" + p.intStart + "," + p.intEnd + "]");
        }
    }

    private boolean containsManyPeriods() {
        return confirmedPeriods.size() > 1;
    }
}

//    public void composeHourlySeries() {
//        allQueriesTimes = SignalModifier.generateSignal_addOneHour(allQueriesTimes);
//        composeSeries(resolution);
//    }
//    private ArrayList<Long> sampleSeries() {
//        ArrayList<Long> samplesSeries = new ArrayList<>();
//        for (int i = 0; i < allQueriesTimes.size(); i += 10) {
//            samplesSeries.add(allQueriesTimes.get(i));
//        }
//        return samplesSeries;
//    }
//    @SuppressWarnings("empty-statement")
//    private void findInitialPeriods() {
//        try {
//            RConnection c = new RConnection();
//
//            c.assign("myvalues", signal);
//
//            c.eval("source(\"/home/asma/Desktop/PG_logs/findPeriods.R\")");
//
//            
//            int minPeriodLength = signal.length / allQueriesTimes.size();
//            int maxPeriodLength = signal.length / 4;
//            
//            REXP v = c.eval("chisq.pd(myvalues, " + minPeriodLength + "," + maxPeriodLength + " , .01)");
//
//            initialPeriods = v.asIntegers();
//
//        } catch (RserveException ex) {
//            Logger.getLogger(PeriodsDetector.class.getName()).log(Level.SEVERE, null, ex);
//        } catch (REXPMismatchException | REngineException ex) {
//            Logger.getLogger(PeriodsDetector.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }
