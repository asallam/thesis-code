/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package findingperiods;


/**
 *
 * @author asma
 */
public class PeriodMatching {

    PeriodInfo periodInfo;
    
    long nextAnticipatedTime;

    public PeriodMatching(PeriodInfo periodInfo) {
        this.periodInfo = periodInfo;   
        this.nextAnticipatedTime = periodInfo.startTime + periodInfo.periodLength * periodInfo.resolution;
    }

    public boolean checkQuery(long queryTime) {
        
        while(queryTime > nextAnticipatedTime + periodInfo.intEnd) {
            nextAnticipatedTime += (periodInfo.periodLength * periodInfo.resolution);
        }
        
        if(queryTime >= nextAnticipatedTime - periodInfo.intStart && queryTime <= nextAnticipatedTime + periodInfo.intStart) {
            nextAnticipatedTime += (periodInfo.periodLength * periodInfo.resolution);
            return false;
        }
        return true;
    }

}
