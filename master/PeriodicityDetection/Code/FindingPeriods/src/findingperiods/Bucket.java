/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package findingperiods;

import java.util.ArrayList;

/**
 *
 * @author asma
 */
public class Bucket {
    ArrayList<Long> queriesTimes;
    
    public Bucket() {
        queriesTimes = new ArrayList<>();
    }
    
    public void addQueryTime(long queryTime) {
        queriesTimes.add(queryTime);
    }

    long getLastQueryTimeInBucket() {
        return queriesTimes.get(queriesTimes.size() - 1);
    }
    
    public ArrayList<Long> getQueriesTimes() {
        return queriesTimes;
    }
    
}
