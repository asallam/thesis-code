package findingperiods;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author asma
 */
public class PeriodInfo {
    int periodLength;
       
    long intStart;
    long intEnd; 
    
    long firstAccessInTraining;
    
    float percentageMissed;
    
    long startTime;
    
    int resolution;
    

    public PeriodInfo() {
    
    }
    
    public PeriodInfo(int periodLength) {
        this.periodLength = periodLength;
    }

    public int getPeriodLength() {
        return periodLength;
    }
    
    public float getPercentageMissed() {
        return percentageMissed;
    }

    @Override
    public boolean equals(Object obj) {
        return this.periodLength == ((PeriodInfo)obj).getPeriodLength();
    }

    public void setPeriodLength(int periodLength) {
        this.periodLength = periodLength;
    }

    public void setIntStart(long intStart) {
        this.intStart = intStart;
    }

    public void setInEnd(long intEnd) {
        this.intEnd = intEnd;
    }
    
    public void setFirstAccess(long firstAccess) {
        this.firstAccessInTraining = firstAccess;
    }

    public void setPercentageMissed(float percentageMissed) {
        this.percentageMissed = percentageMissed;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }
    
    public long getStartTime() {
        return startTime;
    }

    public void setResolution(int resolution) {
        this.resolution = resolution;
    }

    public long getIntStart() {
        return intStart;
    }
    
    public long getIntEnd() {
        return intEnd;
    }

    public int getResolution() {
        return resolution;
    }

}
