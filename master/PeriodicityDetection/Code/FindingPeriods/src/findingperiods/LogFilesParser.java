/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package findingperiods;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class LogFilesParser {

    int[] signal;
    Bucket[] buckets;
    
    ArrayList<Date> allDates = new ArrayList<>();
    ArrayList<Long> allQueriesTimes = new ArrayList<>();

    public Bucket[] getBuckets() {
        return buckets;
    }
    
    public ArrayList<Long> getAllQueriesTimes() {
        return allQueriesTimes;
    }

    public ArrayList<Long> getTrainingQueries(float portion) {
        
        ArrayList<Long> trainingQueries = new ArrayList<>();
        int numTrainingQueries = (int)Math.ceil(portion * allQueriesTimes.size());
        for(int i = 0; i < numTrainingQueries; i++) {
            trainingQueries.add(allQueriesTimes.get(i));
        }
        
        return trainingQueries;
    }
    
    public ArrayList<Long> getDetectionQueries(float portion) {
        
        ArrayList<Long> detectionQueries = new ArrayList<>();
        int numTrainingQueries = (int)Math.ceil(portion * allQueriesTimes.size());
        for(int i = numTrainingQueries; i < allQueriesTimes.size(); i++) {
            detectionQueries.add(allQueriesTimes.get(i));
        }
        
        return detectionQueries;
    }

    public void parseFile(String fileName, String queryToDetect) {
        
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line = br.readLine();

            Date firstQueryTime = null;
            while (line != null) {
                if (line.contains("\",\"parameters:")) {
                    String[] lineParts = line.split(",");

                    String queryTime = lineParts[0];
                    
                    String query = composeQuery(line);
                    if (query != null && query.equals(queryToDetect)) {
                        Date queryFormattedTime = formatTime(queryTime);
                        allDates.add(queryFormattedTime);
                        
                        if (firstQueryTime == null) {
                            firstQueryTime = queryFormattedTime;
                        }
                    }
                }
                line = br.readLine();
            }
            
            Collections.sort(allDates);
            
            for(Date d : allDates) {
                long diff = getDiff(d, firstQueryTime);
                allQueriesTimes.add(diff);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String composeQuery(String line) {
        String query = line.substring(line.indexOf(":", line.indexOf("execute ") + 8) + 2, line.indexOf("\",\"parameters:"));
        query = query.replaceAll("\"", "");

        return query;
    }

    public Date formatTime(String queryTime) {
        //System.out.println(queryTime);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss.SSS");
        Date queryFormattedTime = null;
        try {
            queryFormattedTime = sdf.parse(queryTime);

            //System.out.println(queryFormattedTime);
            return queryFormattedTime;
        } catch (ParseException ex) {
            Logger.getLogger(LogFilesParser.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    private long getDiff(Date queryFormattedTime, Date firstQueryTime) {
        long diff = queryFormattedTime.getTime() - firstQueryTime.getTime();
        //System.out.println(diff);
        return diff;
    }

}
