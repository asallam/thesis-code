/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package findingperiods;

import java.util.ArrayList;

/**
 *
 * @author asma
 */
public class SignalModifier {
    
    public static ArrayList<Long> generateSignal_addOneHour(ArrayList<Long> allDiffs) {
        ArrayList<Long> newDiffs = new ArrayList<>();
        
        long maxDiff = 0;
        long maxDiffNewDiff = 0;
        
        for(int i = 0; i < allDiffs.size(); i++) {
            long oldDiff = allDiffs.get(i);
            long numMilliSecondsInOneHour = 24 * 60 * 60 * 1000;
            long numMilliSecondsInOneSecond = 1000;
            
            long index1 = ((long)oldDiff) / 1000;
            long index2 = ((long)oldDiff - i * numMilliSecondsInOneSecond + i * numMilliSecondsInOneHour) / (24 * 60 * 60 * 1000);
            //System.out.println(index1 + " : " + index2);
            
            System.out.println((long)oldDiff - i * numMilliSecondsInOneSecond);
            
            long newDiff = (long)oldDiff - i * numMilliSecondsInOneSecond + i * numMilliSecondsInOneHour;
            newDiffs.add(newDiff);
            
            long thisDiff = oldDiff % 1000;
            if(thisDiff > maxDiff) {
                maxDiff = thisDiff;
                maxDiffNewDiff = newDiff;
                if(maxDiff == 999) {
//                    System.out.println();
                }
            }
        }
        System.out.println("MAX Diff = " + maxDiff + "," + maxDiffNewDiff);
        return newDiffs;
    }
    
}
