/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import Common.DB_connector;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asma
 */
public class TableDeleteSels_Training {
    String tableName;
    int maxDeletes;
    HashMap<String, Integer> usersDeletes;
    DB_connector dB_connector;

    public TableDeleteSels_Training(DB_connector dB_connector, String tablesName) {
        this.tableName = tablesName;
        this.dB_connector = dB_connector;
        usersDeletes = new HashMap<>();
    }

    public TableDeleteSels_Training(String tableName) {
        this.tableName = tableName;
    }

    public void setConnector(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    // TODO employ the grubb's test - http://www.itl.nist.gov/div898/handbook/eda/section3/eda35h1.r
    public void computeMax() {
        for (Map.Entry pair : usersDeletes.entrySet()) {
            int numDeletes = (int) pair.getValue();
            if(maxDeletes < numDeletes) {
                maxDeletes = numDeletes;
            }
        }
    }

    void addSel(String UID, String query) {
        int count = dB_connector.executeUpdate(query);
        Integer userDeletes = usersDeletes.get(UID);
        if (userDeletes == null) {
            usersDeletes.put(UID, count);
        } else {
            usersDeletes.replace(UID, userDeletes + count);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TableDeleteSels_Training) {
            TableDeleteSels_Training other = (TableDeleteSels_Training) obj;
            String table1 = this.tableName;
            String table2 = other.getTableName();
           
            return table1.equals(table2);
            
        } else {
            return false;
        }
    }

    public String getTableName() {
        return tableName;
    }

    public int getMaxDeletes() {
        return maxDeletes;
    }

    DB_connector getConnector() {
        return dB_connector;
    }
    
}
