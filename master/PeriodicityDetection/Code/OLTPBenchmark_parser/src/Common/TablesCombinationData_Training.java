/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;

import Utils.MathUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author asma
 */
public class TablesCombinationData_Training {

    ArrayList<String> tablesNames;
    float selectivityThreshold;
    HashMap<String, ArrayList<String>> usersPredicates;
    DB_connector dB_connector;

    public TablesCombinationData_Training(DB_connector dB_connector, ArrayList<String> tablesNames) {
        this.tablesNames = tablesNames;
        this.dB_connector = dB_connector;
        usersPredicates = new HashMap<>();
    }

    public TablesCombinationData_Training(ArrayList<String> tablesNames) {
        this.tablesNames = tablesNames;
    }

    public void setConnector(DB_connector dB_connector) {
        this.dB_connector = dB_connector;
    }

    public void addPredicate(String UID, String predicate) {
        ArrayList<String> userPredicates = usersPredicates.get(UID);
        if (userPredicates == null) {
            userPredicates = new ArrayList<>();
            usersPredicates.put(UID, userPredicates);
        }
        if (!userPredicates.contains(predicate)) {
            userPredicates.add(predicate);
        }
    }

    // TODO employ the grubb's test - http://www.itl.nist.gov/div898/handbook/eda/section3/eda35h1.r
    public void computeThreshold() {
        String queryTablesPortion = "";
        long numRowsCartesian = 1;
       
        for (String tableName : tablesNames) {
            if (!queryTablesPortion.equals("")) {
                queryTablesPortion = queryTablesPortion + ", ";
            }
            queryTablesPortion = queryTablesPortion + tableName;
            
            String query = "select count(*) from " + tableName;
            query = query.replaceAll("\"\"", "\"");
            numRowsCartesian *= dB_connector.executeQuery_getCount(query);
        }

        ArrayList<Integer> allResults = new ArrayList<>();
       
        for (Map.Entry pair : usersPredicates.entrySet()) {
            ArrayList<String> userPredicates = (ArrayList<String>) pair.getValue();
            String combinedPredicates = "";
            for (String predicate : userPredicates) {
                if (!combinedPredicates.equals("")) {
                    combinedPredicates = combinedPredicates + " OR ";
                }
                combinedPredicates = combinedPredicates + "(" + predicate + ")";
            }

            
            String query = "Select count(*) from " + queryTablesPortion + " where " + combinedPredicates;

            System.out.println(query);
            query = query.replaceAll("\"\"", "\"");
            int numRows = dB_connector.executeQuery_getCount(query);
            allResults.add(numRows);
            
        }
        
        MathUtil m = new MathUtil();
        Integer outlier;
        int i = 0;
        int numResults = allResults.size();
        do {
            outlier = m.getOutlier(allResults, 0.95);
            if(outlier != null) 
                allResults.remove(outlier);
            i++;
        } while (outlier != null && i < 0.1 * numResults);
        
        int maxRows = 0;
        for(int n : allResults) {
            if(n > maxRows) {
                maxRows = n;
            }
        }
        
        this.selectivityThreshold = (float) maxRows / numRowsCartesian;
        
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TablesCombinationData_Training) {
            TablesCombinationData_Training other = (TablesCombinationData_Training) obj;
            ArrayList<String> tables1 = this.tablesNames;
            ArrayList<String> tables2 = other.getTablesNames();
            if (tables1.size() != tables2.size()) {
                return false;
            }
            for (String table1 : tables1) {
                if (!tables2.contains(table1)) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public ArrayList<String> getTablesNames() {
        return tablesNames;
    }

    public DB_connector getConnector() {
        return dB_connector;
    }

    public float getSelThreshold() {
        return selectivityThreshold;
    }
}
