/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Common;



/**
 *
 * @author asma
 */
public class TableInsertSels_Detection {
    String tableName;
    int maxInserts;
    int numInsertsSoFar;
    DB_connector dB_connector;
    
    float remainingSel;
    
    int tableRowCount;

    public int getTableRowCount() {
        return tableRowCount;
    }
    
    public TableInsertSels_Detection(DB_connector dB_connector, String tableName, int maxInserts) {
        this.tableName = tableName;
        this.dB_connector = dB_connector;
        this.maxInserts = maxInserts;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof TableInsertSels_Detection) {
            TableInsertSels_Detection other = (TableInsertSels_Detection) obj;
            String table1 = this.tableName;
            String table2 = other.getTableName();
            return table1.equals(table2);
        } else {
            return false;
        }
    }

    public String getTableName() {
        return tableName;
    }
    
    public boolean checkAnomaly(String query) {
        return numInsertsSoFar >= maxInserts;
    }
    
    public int executeQuery(String query) {
        int count = dB_connector.executeUpdate(query);
        numInsertsSoFar += count;
        return count;
    }
    
    public void recordSel() {
        tableRowCount = dB_connector.getNumTableRows(tableName);
        remainingSel = (float)(maxInserts - numInsertsSoFar) / tableRowCount;        
    }

    public float getRemainingSel() {
        return remainingSel;
    }
}
