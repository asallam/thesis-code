/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculator;

import Common.RoleProfile;
import Common.TableChanges;
import Common.TrainingPhase;
import Common.DB_connector;
import Common.LogFilesParser;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author asma
 */
public class Main_FP {

    public static void runForDataset(String datasetName, int numDetectionFiles, String []tablesNames) {
        String dirName = "/home/asma/Desktop/PG_logs/" + datasetName + "/";
        
        String trainingFileName1 = "training_10.csv";
        String detectionFileName1 = "detection_10.csv";
      

        LogFilesParser logFilesParserTraining = new LogFilesParser();
        logFilesParserTraining.parseFile(dirName + trainingFileName1);
        //logFilesParserTraining.parseFile(dirName + trainingFileName2);

        ArrayList<String> UIDs_training = logFilesParserTraining.getUIDs();
        ArrayList<String> queries_training = logFilesParserTraining.getAllQueries();
        HashMap<String, ArrayList<String>> usersQueries = logFilesParserTraining.getUsersQueries();

        LogFilesParser logFilesParserDetection = new LogFilesParser();
        logFilesParserDetection.parseFile(dirName + detectionFileName1);

        LogFilesParser logFilesParserDetection2 = new LogFilesParser();
        if(numDetectionFiles > 1) {
            String detectionFileName21 = "detection_100_1.csv";
            String detectionFileName22 = "detection_100_2.csv";
            logFilesParserDetection2.parseFile(dirName + detectionFileName21);
            logFilesParserDetection2.parseFile(dirName + detectionFileName22);
        } else {
            String detectionFileName21 = "detection_100.csv";
            logFilesParserDetection2.parseFile(dirName + detectionFileName21);
        }

        DB_connector dB_connector = new DB_connector(datasetName);

        TrainingPhase trainingPhase = new TrainingPhase(dB_connector);
        trainingPhase.doTraining(UIDs_training, queries_training, usersQueries);
        int numTrainingQueries = queries_training.size();

        dB_connector.executeAnalyze();

        RoleProfile roleProfile = trainingPhase.getProfile();

        HashMap<String, TableChanges> tablesChanges = new HashMap<>();
        for (String tableName : tablesNames) {
            tablesChanges.put(tableName, new TableChanges(tableName, dB_connector));
        }

        ArrayList<String> UIDs_detection = logFilesParserDetection.getUIDs();
        ArrayList<String> queries_detection = logFilesParserDetection.getAllQueries();

        ArrayList<String> allUIDs = new ArrayList<>();
        for (String UID : UIDs_detection) {
            if (!allUIDs.contains(UID)) {
                allUIDs.add(UID);
            }
        }

        DetectionPhase_new detectionPhase = new DetectionPhase_new(dB_connector, allUIDs, roleProfile, tablesChanges);
        
        
        detectionPhase.checkQueries_FP(UIDs_detection, queries_detection);

        detectionPhase.recordCurrentSels();
       

        ArrayList<String> UIDs_detection2 = logFilesParserDetection2.getUIDs();
        ArrayList<String> queries_detection2 = logFilesParserDetection2.getAllQueries();

        ArrayList<String> allUIDs2 = new ArrayList<>();
        for (String UID : UIDs_detection2) {
            if (!allUIDs2.contains(UID)) {
                allUIDs2.add(UID);
            }
        }

        detectionPhase.checkQueries_FN(UIDs_detection2, queries_detection2, allUIDs, allUIDs2, queries_detection.size());

        System.out.println("==============================================");

        System.out.println("Num Queries: " + detectionPhase.getNumQueries());
        System.out.println("False positives: " + detectionPhase.getFalsePositives());
        System.out.println("False positives Select: " + detectionPhase.getFalsePositivesSelect() + "/" + detectionPhase.getNumQueriesSelect());
//        System.out.println("False positives Insert: " + detectionPhase.getFalsePositivesInsert() + "/" + detectionPhase.getNumQueriesInsert());
//        System.out.println("False positives Delete: " + detectionPhase.getFalsePositivesDelete() + "/" + detectionPhase.getNumQueriesDelete());
//        System.out.println("False positives Update: " + detectionPhase.getFalsePositivesUpdate() + "/" + detectionPhase.getNumQueriesUpdate());

//        System.out.println("Num Queries 2: " + detectionPhase.getNumQueries2());
//        System.out.println("False negatives: " + detectionPhase.getNumFalseNegatives());
//        System.out.println("False Negatives Select: " + detectionPhase.getFalseNegativesSelect() + "/" + detectionPhase.getNumQueriesSelect2());
//        System.out.println("False Negatives Insert: " + detectionPhase.getFalseNegativesInsert() + "/" + detectionPhase.getNumQueriesInsert2());
//        System.out.println("False Negatives Delete: " + detectionPhase.getFalseNegativesDelete() + "/" + detectionPhase.getNumQueriesDelete2());
//        System.out.println("False Negatives Update: " + detectionPhase.getFalseNegativesUpdate() + "/" + detectionPhase.getNumQueriesUpdate2());

//        System.out.println("==============================================");

        detectionPhase.printDetectionData(dirName);

        for (String tableName : tablesNames) {
            TableChanges tc = tablesChanges.get(tableName);
            System.out.println(tableName + ": FPs = " + tc.getFPs() + ", FNs = " + tc.getFNs());
        }
//        detectionPhase.printFNs(dirName);
        detectionPhase.printAnomalyDegrees(dirName);
        detectionPhase.printFPs(dirName);
    }

    public static void main(String[] args) {
    
        String[] tablesNames;
        String datasetName;
        
//        tablesNames = new String[]{"access_info", "call_forwarding", "special_facility", "subscriber"};
//        datasetName = "tatp";
//        runForDataset(datasetName, 1, tablesNames);
        
        tablesNames = new String[]{"airline", "airport", "airport_distance", "config_histograms", "config_profile", "country", "customer", "flight", "frequent_flyer", "reservation"};
        datasetName = "seats";
        runForDataset(datasetName, 1, tablesNames);
        
        /*tablesNames = new String[]{"item", "review", "review_rating", "trust", "user"};
        datasetName = "epinions";
        runForDataset(datasetName, 1, tablesNames);*/
        
        /*tablesNames = new String[]{"category", "config_profile", "global_attribute_group", "global_attribute_value", "item", "item_attribute", 
        "item_bid", "item_comment", "item_image", "item_max_bid", "item_purchase", "region", "useracct", "useracct_attributes", "useracct_feedback",
        "useracct_item", "useracct_watch"};
        datasetName = "auctionmark";
        runForDataset(datasetName, 1, tablesNames);*/
        
//        tablesNames = new String[]{"customer", "district", "history", "item", "new_order", "oorder", "order_line", "stock", "warehouse"};
//        datasetName = "tpcc";
//        runForDataset(datasetName, 2, tablesNames);
    
    }
    
}
