/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author asma
 */
public class RemainingSel {
    public static void main(String args[]) {
        String fileName = "/home/asma/Desktop/PG_logs/tatp/detectionResults_select.txt";
        
        ArrayList<String> tablesNames = new ArrayList<>();
        ArrayList<Float> sumRemainingSel = new ArrayList<>();
        ArrayList<Integer> numRows = new ArrayList<>();
        ArrayList<Integer> numQueriesTillAnomaly = new ArrayList<>();
        ArrayList<Float> sumAnomalousQueriesSel = new ArrayList<>();
        ArrayList<Integer> counts = new ArrayList<>();
        
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String tableName = br.readLine();
            while(tableName != null) {
                               
                int i;
                for(i = 0; i < tablesNames.size(); i++) {
                    if(tablesNames.get(i).equals(tableName)) {
                        break;
                    }
                }
                if(i >= tablesNames.size()) {
                    tablesNames.add(tableName);
                    sumRemainingSel.add(0.0f);
                    numRows.add(0);
                    numQueriesTillAnomaly.add(0);
                    sumAnomalousQueriesSel.add(0.0f);
                    counts.add(0);
                }
                
                String line = br.readLine();
                float remainingSel = Float.parseFloat(line); // RemainingSel
                line = br.readLine();
                int numRows1 = Integer.parseInt(line); // RowsCartesian
                line = br.readLine();
                int numQueriesTillAnomaly1 = Integer.parseInt(line); // NumQueriesTillAnomaly
                line = br.readLine();
                float anomalousQueriesSel = Float.parseFloat(line); // AnomalousQueriesSel
                
                sumRemainingSel.set(i, sumRemainingSel.get(i) + remainingSel);
                numRows.set(i, numRows1);
                numQueriesTillAnomaly.set(i, numQueriesTillAnomaly.get(i) + numQueriesTillAnomaly1);
                sumAnomalousQueriesSel.set(i, sumAnomalousQueriesSel.get(i) + anomalousQueriesSel);
                counts.set(i, counts.get(i) + 1);
                
                tableName = br.readLine(); // tableName
            }
            
            for(int k = 0; k < tablesNames.size(); k++) {
                tableName = tablesNames.get(k);
                float remainingSel = sumRemainingSel.get(k) / counts.get(k); // RemainingSel
                int numRows1 = numRows.get(k); // RowsCartesian
                float numQueriesTillAnomaly1 = (float)(numQueriesTillAnomaly.get(k)) / counts.get(k); // NumQueriesTillAnomaly
                float anomalousQueriesSel = sumAnomalousQueriesSel.get(k) / counts.get(k); // AnomalousQueriesSel
                float avgAnomalousQueriesSel = anomalousQueriesSel;
                if(numQueriesTillAnomaly1 > 0) {
                    avgAnomalousQueriesSel = anomalousQueriesSel /numQueriesTillAnomaly1;
                }
                
                System.out.println(tableName + " " + remainingSel + " " + numRows1 + " " + avgAnomalousQueriesSel);
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RemainingSel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RemainingSel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
